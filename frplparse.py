#!/bin/env python
#==========================
# TODO:
# - fispy: undo declarations when parsing fails.  I currently can't define
#   a subroutine at the command line because the function header adds the
#   entry to the environment.  As a result, the second line sees the entry
#   already there and bombs with a redeclaration error.
# - Figure out recursion in sayit2.fr
#
# - Run user code in fispy inside an environment: fty.Lang->stdlib->usrcode
# - Move member-function call parameters into an environment
# - Implement user-defined functions - *** working with parameters in
#   - Implement the return value as a predefined var named 'ans'
#==========================
# Loops:
# TODO test downto - so far only up has been tested.
# TODO? make the loop check for up/down at runtime so stride can be a variable.

# *** Python.exe: os.chdir(r"c:\..."); import imp; imp.reload(frplparse) ***

# TODO: Make sure input charset is handled correctly.  Add a charset directive.

########################################################################
# TODO:
#
# - Regularize where blank lines are skipped.  Currently it happens
#   in _FStatement, _FProgram, and _FImports.
########################################################################

from sys import stdout, setrecursionlimit, getrecursionlimit
import pdb
import re
from pyp import *
from inspect import currentframe, getframeinfo
    #http://stackoverflow.com/questions/3056048
#from pprint import pprint
#    # http://bytes.com/topic/python/answers/746803-help-pyparsing

from collections import Iterable
import shortuuid    # for auto-generating kind names
    # https://github.com/stochastic-technologies/shortuuid

# frpl-specific modules
import frpltypes as fty
import frplast as ast
import frplstdlib
from frplutil import FB as _FB, CK as _CK, SCK as _SCK
from frplutil import CKOneOf as _CKOneOf

ParserElement.enablePackrat()
    # DO NOT FORGET TO DO THIS! or else your parse will be _very_ slow.

#=========================================================================
# Language keywords.  Define them here so that we can exclude them from
# the space of valid identifiers below.

# First, special method names for operators that can be overloaded.
# These cannot be identifiers but can participate as operands in expressions.
# For example, you can express a+2 as a[plus,2], and "plus" is an operand.
#
# Bitwise operators are not overloadable so that they will always
# be just the bits, ma'am.
#
# pl, mi, ti, di, mo are 100% synonymous with plus, minus, times, div,
# mod.  A defined operator plus will be called for pl and plus.
_OverloadableOperators='''
and but di div eq ge get getbits gt le lt mi minus mo mod ne not or
pl plus put putbits ti times tothe
'''

_ME = "me"
# Note: "me" is a useful identifier, not overloadable, not usable in
# parts.  TODO figure out how to handle it appropriately.  For now, it's
# just a plain old identifier.

_ANS = "ans"
# The return value of a function.

_RESTRICTED_IDENTS = [_ME, _ANS]
    # Identifiers that can't be declared except by internal code
    # TODO?  Add rez and friends here?  Preferably not.

# Now, keywords.  Not all of these are defined; some are reserved for
# future use.

FInterpreterExitCommands = ['bye','exit','end','leave','quit', 'goodbye']
        # these are reserved for use by the interpreter.
        # lots of choices for exit command in hopes of avoiding
        # the frustration of using a standard ftp client :)

# _FNonIdentWord: Words that can't occur as standalone parts of identifiers.
# E.g., foo.xor is forbidden but foo.xorthing is OK.  The overloadable
# operators are not part of _FNonIdentWord because they can be referred to
# as identifiers.
# TODO? Make some of these overloadable?
_FNonIdentWord = _CKOneOf("""
    all any array as begin bit bitand bitnot bitor bits bitxor block by
    catalog compl def downto else elseif false flag flex for fr from function
    gets if import in into is job kind lamb lambda list loop make
    mixin mod names nand next nm none nop nor of
    package proc procedure shl shr ta task test to true
    unless unwrap uses using when with without wrap xnor xor
    """+
    ' '.join(FInterpreterExitCommands)
)
    # array and list are as you expect.
    # catalog is a hash map (dict, map<>)
    # fr, from, using, uses, and without are for
    # future expansions of the import system.  Likewise mixin.
    # lamb and lambda are for anonymous functions.
    # function, proc, and procedure are for subroutines.

#=========================================================================
# Identifier/declaration parsers.  Way up here so test() can use it.

_ParseIdentifier = lambda s,l,t: ast.FAIdent(t[0].lower(), s,l)
    # .lower() so that identifiers will be case-insensitive.

def _ResolveTypeRef(tyref):
    """ Helper for _ParseVarDeclaration() and the Kind parsing routines. """

    retv = None
    # Find out what type we want
    if isinstance(tyref,fty.FTBase):
        retv = tyref

    elif isinstance(tyref, ast.FAIdent):     # Look up a type in the environment
        tyname = tyref.pvName
        env = _Env.G.find(tyname)

        if env is not None:
            ent=env[tyname]
            if ent.decltype is not None:
                retv=ent.decltype
    #endif

    return retv
# _ResolveTypeRef

def _ParseVarDeclaration(s,l,t):
    return _ParseVarDeclarationInternal(s,l,t,False)

def _ParseVarDeclarationInternal(s,l,t, allow_overrides):
    """ Parse a declaration.  t[0] is the string or ast.FAIdent of the name.
        t[1] is either an FAIdent or an fty.FTBase of the type.
        allow_overrides, if True, permits declaring reserved items such
        as _ANS and _ME.
        This function is also called directly by other parsing functions,
        e.g., _ParseLoopClauseFor.
        Requres _Env.G.
    """
    assert _Env.G is not None
    #print('parsing declaration (env='+repr(_Env.G)+')')
    #atype = None
    tyname=str(t[1])

    atype = _ResolveTypeRef(t[1])

    if atype is None:
        raise ParseFatalException(s, l,
            "I'm afraid I don't know what kind of thing '%s' is."%tyname)
    #endif

    # Get the name
    if isinstance(t[0], str):
        aname = t[0]
    elif isinstance(t[0], ast.FAIdent):
        aname = t[0].pvName
    else:
        raise ParseFatalException(s, l,
            "Can't understand %s as name to be declared"%str(t[0]))
    #endif

    # Make sure it's allowed
    if (not allow_overrides) and (aname.lower() in _RESTRICTED_IDENTS):
        raise ParseFatalException(s, l,
            ("I'm sorry to say that '%s' is reserved and can't be used" +
             "in a 'nm' statement.")%aname)

    # Add to the symbol table.
    if _Env.G.find(aname) is None:
        _Env.G[aname] = ast.Entry(atype, s=s, l=l)
            # Save the location for dbg  ^^^^^^^^ msgs on redecls.
        _Env.decls_this_run.append( (_Env.G, aname) )
            # Save the declaration in case the parse fails

    else:   # attempt to redefine
        #pdb.set_trace()
        prior_decl = _Env.G.find(aname)[aname]
        orig_s = prior_decl.source_string
        orig_l = prior_decl.source_loc
        raise ParseFatalException(s, l,
            "I'm afraid I have already heard of '%s' %s line %d, col %d."%(
            aname, "and don't want to risk confusion.  It was on",
            lineno(orig_l,orig_s),col(orig_l,orig_s)))
    #endif definition new else

    return ast.FADecl(atype, t[0], s,l)   # type, then name
# end _ParseVarDeclaration

#=========================================================================
# Test harness

# Test suppressor - set DOTEST.DOIT=True to run tests.  Default is False.
class _DoTestClass(object):
    def __init__(self):
        self.DOIT = False       # If True, test() runs tests.
        self.DEBUG = False      # If True, the parser prints debug info
        self.SILENT = False     # overrides test() arg printparse
DOTEST = _DoTestClass()
#DOTEST.DEBUG=True

def BP():
    """Absolute breakpoint triggered on a global switch"""
    if DOTEST.DEBUG:
        pdb.set_trace()

def test(elem, teststr, expected, printparse=False, vars=None):
    """Main test function.
        PyParsing _elem_ either does or does not match _teststr_.  If that
        comparison is opposite from bool _expected_, print a message.
        Run all tests starting from an empty environment."""
    if (not DOTEST.DEBUG) and (not DOTEST.DOIT):
        return

    # Load the environment
    SetGlobalEnv(ast.Env())     # adds Lang as the outer of the new env
    if vars is not None:
        for decl in vars:   #elem = (name:str,ty:str or fty.FTBase)
            (name,ty)=decl
            _ParseVarDeclaration('',0,[_ParseIdentifier('',0,[name]),
                                    _ParseIdentifier('',0,[ty])])

    lineno = getframeinfo(currentframe().f_back).lineno
    theline=str(lineno)+':'

    # In general, we can only run the string once, so run it here and
    # save any exception.
    matched = True
    theparse = None
    theexception = None
    try:
        theparse = elem.parseString(teststr)
    except ParseBaseException as err:
        matched = False
        theexception = err
    #end try

    if matched!=expected:
        # print caller's line number and a debug message
        print(theline)
        stdout.flush()
        print( (elem.name if hasattr(elem,"name") else "unnamed element"),
                "failed on --" + teststr + "--: should have",
                ("matched" if expected else "missed"), "but didn't.")

        if not matched: # if we wanted it to but it didn't,
                        # show the exception.
            print('  -> exception was %s.'%theexception)

    if printparse and not DOTEST.SILENT:  # whether or not it matched.
        if matched:
            print('%s -- %s -- parsed to == %s =='%(theline, teststr,
                            str(theparse)))
        else:
            print('%s -- %s -- parse failed'%(theline, teststr))
        stdout.flush()

    SetGlobalEnv(ast.Env())  # for safety, discard all declarations
    # end test

# Environments ##############################################################

class _EnvHolder(object):
    def __init__(self):
        self.G = None    # global namespace
#        self.A = None    # anonymous-message namespace
        self.is_first_arg = False
            # True if we are parsing the first argument of an apply,
            # for which an unknown identifier will be rewritten to a
            # string literal.
        self.decls_this_run = []
            # Declarations made this run.  List of (Env, name:str)
            # pairs.
#end class _EnvHolder

_Env=_EnvHolder()
    # use fields since otherwise the None values will be captured
    # in, e.g., _ParseVarDeclaration(), which will then never see later changes

def pushG(s,l,t):
    """Pushes a new, empty environment on the stack.
        Helper for nesting scopes in parse actions."""
    _Env.G = ast.Env(outer=_Env.G)
    #print('$$$ [push] envG is now '+_Env.G.uu) #DEBUG
    return _Env.G

def pushGNow():
    """ Non-parse-action invoker for pushG """
    return pushG('', 0, None)

def popG(s,l,t):
    """Helper for un-nesting scopes in parse actions"""
    oldenvg = _Env.G
    if _Env.G is not None:
        _Env.G = _Env.G.outer
    #print('$$$ [pop] envG is now '+_Env.G.uu) #DEBUG
    return oldenvg

def popGNow():
    """ Non-parse-action invoker for popG """
    return popG('', 0, None)

def SetGlobalEnv(theenv):
    """Public setter for the global environment.
        Overrides _outer_ so that the predefined frpl language types
        are always outermost. """
    assert theenv is not None
    _Env.G = theenv
    _Env.G.outer = fty.Lang # mutates _theenv_
#end SetGlobalEnv

#def SetAnonMsgEnv(theenv):
#    """Public setter for the anon-msg environment"""
#    _Env.A = theenv

def GetGlobalEnv():
    """Public getter for the global environment"""
    return _Env.G

#def GetAnonMsgEnv():
#    """Public getter for the anon-msg environment"""
#    return _Env.A
# Helper routines ###########################################################

# handy constants and syntactic sugar

# Helper for _FExpr and other parse functions
def _unwrapt0(fn):
    """Decorator for convenience in processing parse results in FExpr
    and other parse functions.
    Passes t[0] as the tokens argument to fn.  This undoes the effect
    of the Group() added by infixNotation()."""
    return lambda s,l,t: fn(s,l,t[0])

# Typechecking helper
#def _RealTy(astitem:ast.FANode, start_G = True)->fty.FTBase:
def _RealTy(astitem:ast.FANode)->fty.FTBase:
    """Returns the type for _astitem_, looking up identifiers if necessary."""
    retv = astitem.ty
    # The type is only different if it's an identifier.
    if astitem.ty is fty.FTIdentifier:
        startenv = _Env.G
        if startenv is not None:
            theenv = startenv.find(astitem.pvName)
            if theenv is not None:
                retv = theenv[astitem.pvName].ty
                    # if no environment or the name is not found in the
                    # environment, the return value will be unchanged
                    # as fty.FTIdentifier.
    return retv
# end function _RealTy

## as of 2015/06/16, not used.
#def _IsUndefined(fvID:ast.FAIdent, start_G = True)->bool:
#    """ Check whether an identifier is undefined. """
#    assert fvID.ty == fty.FTIdentifier  # sanity check
#    rty = _RealTy(fvID, start_G)
#    return rty is not fty.FTIdentifier
#        # If we got back FTIdentifier, it was undefined.
## end _IsUndefined

def _Promote(astitem, totype):
    """Promote types"""
    # At the moment this is all we handle.
    if astitem.ty is fty.FTInteger and totype is fty.FTReal:
        return ast.FATypecast(totype, astitem, lambda i: float(i))
    else:
        return astitem
# end function _Promote

def _LookupInG(name:str, s,l, ctx:str=''):
    """Get the ast.Entry for _name_ """
    #pdb.set_trace()
    destenv = _Env.G.find(name)
    if destenv is None:
        raise ParseFatalException(s, l,
            "I haven't seen what variable %s names%s"%(name,
            (' (needed for parsing a '+ctx) if ctx != '' else ''))
    return destenv[name]
#end _LookupInG

# frpl language definition ##################################################
DOTEST.SILENT = True
DOTEST.DOIT=True

ParserElement.setDefaultWhitespaceChars(" \t")  # don't ignore end of line
_FEOL = LineEnd().setName("EOL")  # end of statement
_SEOL = Suppress(_FEOL).setName("sEOL")     # for convenience

_FComment = Suppress(Regex("[;#]") + restOfLine)
    # Try this as the comment for now.
    # No more than one statement per line, for simplicity.
    # Don't include the _FEOL.
    # Allow ; (non-shifted) and # (for shebang lines)
# For now, don't worry about line continuation ("...")
#   - TODO decide whether line continuation should be provided.

# Things that can appear in expressions #####################################
# These are always packaged as tuples of (type, value).

# Constants

_FBasedNumber = Regex(r"(?P<val>\d[0-9a-zA-Z]*)(?P<base>[BHObho]|[Nn](?P<basenum>\d+))").setName('based-number literal')
    # number with a base (base-10 handled below): <digits><base specifier>
    # <base specifier>: b for bin | h for hex | o for octal |
    #                   n<digits> for base <digits>, up to base 36.
    # handle unary - as an operator

def _ParseBasedNumber(s,l,t):
    val=t['val'].lower()
    base=t['base'].lower()
    basenum=-1     # error flag
    if base=='b':
        basenum=2
    elif base=='h':
        basenum=16
    elif base=='o':
        basenum=8
    else:
        basenum=int(t['basenum'])
    # parse the based number to an integer.
    # No based floating point by design.
    try:
        newval = int(val, basenum)
        return ast.FAConst(fty.FTInteger, newval, s, l)
            #ParseResults([t[0],val,basenum])
    except ValueError as e:
        raise ParseFatalException(s,l,
                             'Could not convert "'+val+'" to a base-'+
                             str(basenum)+' integer',_FBasedNumber) from e
# end _ParseBasedNumber

_FBasedNumber.setParseAction(_ParseBasedNumber)

    # because it ends in "o", it matches syntactically.
    # The parse action checks for whether the digits are in range.

#-------------------

_FNumber = Regex(r"\d+(\.\d*)?([eE][+-]?\d+)?").setName('numeric literal')
    # thanks to Paul McGuire; edited from examples/fourFn.py
    # Again, unary - is an operator, so is not in this Regex.
    # A number always has to start with [0-9].
    # All floats are base-10 by design.  Base-2 floats are uncommon
    # and beyond the scope of this language.

def _ParseNumber(s,l,t):
    #customized from http://stackoverflow.com/a/379966
    instr=t[0]
    try:
        return ast.FAConst(fty.FTInteger, int(instr), s,l)
    except:
        try:
            return ast.FAConst(fty.FTReal,float(instr), s,l)
        except:
            raise ParseFatalException(s,l,
                "Couldn't convert '%s' to a number"%instr)
#end _ParseNumber
_FNumber.setParseAction(_ParseNumber)


# Superseded
# A based or real number.  This is because either _FNumber
# or _FBasedNumber could swallow digits the other needed.
# This regexp will allow some things that are not actually
# semantically-valid numbers, so it needs further checking.
#FComboNumber = Regex(
#    r"\d[0-9a-zA-Z]*(\.[0-9A-Za-z]*)?([eE][+-]?\d+)?([BHObho]|[Nn]\d+)"
#).setName("ComboNumber")

#-------------------

_FString = (QuotedString("\"", "\\") | QuotedString("'", "\\")).setName('string literal')
    # strings can be single or double-quoted.
_FString.setParseAction(lambda s,l,t: ast.FAConst(fty.FTString,t[0], s,l))
    # t[0] is the string.  Should only ever have one.

#-------------------

# Predefined values and other identifier-like words that have
# special meaning, including overloadable operators.
_FBoolValue = _CKOneOf("true false").setName('bool literal')
_FBoolValue.setParseAction(
    lambda s,l,t: ast.FAConst(fty.FTBoolean, t[0].lower()=="true", s,l))


_FNoneValue = _CK('none').setParseAction(
    lambda s,l,t: ast.FAConst(fty.FTNone, None, s,l))
_FPredefinedValue = (
    _FBoolValue|_FNoneValue
)

#-------------------
# Special identifiers
_FSpecialIdentifier = _CKOneOf(_OverloadableOperators)
_FSpecialIdentifier.setParseAction(
    lambda s,l,t: ast.FAIdent(t[0].lower(), s,l))
#DOTEST.DOIT = True
#DOTEST.DOIT = False

#-------------------

# Identifiers
# Identifiers can include dots for package and class access.
# frpl does not distinguish between C++'s . and :: .
# Class.Member is like :: and instance.Member is like .

# Identifier parts: individual elements.  None can be a bare keyword.

@_unwrapt0
def _ParseIdentPart(s,l,t):
    if _FNonIdentWord==t:
        raise ParseException(s,l,   # not Fatal, so that it can be processed
                                    # as a keyword
            ("Can't use %s as part of an identifier - it means something else"
                %t))
    return t
# end _ParseIdentPart

_FIdentifierPart = Word(alphas, alphanums).setName('IdentPart')
    # No underscores since they're shifted.  (But so is camelCase... oh well)
_FIdentifierPart.setParseAction(_ParseIdentPart)

# Complete identifers.  Prohibit comments and whitespace so '.' doesn't
# look like an operator.
_FIdentifier=delimitedList(_FIdentifierPart, '.', True).setName('Ident')
_FIdentifier.setParseAction(_ParseIdentifier)


#DOTEST.DOIT=True
#DOTEST.DOIT=False

# TODO add record and array elements

# Flags: /foo or /foo/arg

_FFlag = Regex(r'/\s*(?P<nm>[a-zA-Z0-9]+)')
    # TODO allow Unicode here and throughout.
_FFlag.setParseAction(lambda s,l,t: ast.FAFlag(t['nm'], s,l))

_FFlagArgOp=Literal('/')

#-------------------

#Sum it all up: Operands.
# At present, these are only used in rvalues, so an Identifier is always
# a ReadVar.  This is because the assignment (remembering) operator '=' is
# not an operator in an expression.  It is instead part of the
# assignment statement.
def _ParseOperandIdentifier(s,l,t):
    # pdb.set_trace()
    # Get the declared type of the identifier
    ty = _RealTy(ast.FAIdent(t[0].lower(), s,l))
    # Can't do this because rez and other method names are parsed here
    # but are not in any environment.
    #if ty is fty.FTIdentifier:
    #    # The identifier was not found
    #    raise ParseFatalException(s,l,
    #        ("I don't recognize %s.  "
    #        "Did you declare it with 'names'?")%t[0])
    ##endif
    # Build the node
    retv = ast.FAReadVar(ty, t[0].lower(),
        ast.FAReadVar.NO_ENV if _Env.is_first_arg else _Env.G , s,l)
        # t[0] is still a string since this replaced the old parser function.
    return retv
# _ParseOperandIdentifier()

_FIdentInOperand = _FIdentifier.copy().setName('IDInOp')

_FOperand = (
            _FString |
            _FBasedNumber |      # based number has to come before number
            _FNumber |           # since otherwise Number swallows the
                                # leading [0-9]+ and succeeds.
            _FPredefinedValue |  # Before _FIdentifier so it grabs reserved
                                # words before _FIdentifier sees them.
            _FSpecialIdentifier| # also before _FIdentifier
            _FFlag |
            _FIdentInOperand.setParseAction(_ParseOperandIdentifier)
           ).setName("_FOperand")

_FOperand.setDebug(DOTEST.DEBUG)
#DOTEST.DOIT=True
#DOTEST.DOIT=False

def _ParseOperand(s,l,t):
    _Env.is_first_arg = False   # now that we've seen an operand,
                                # we're no longer doing the first.
# _ParseOperand

_FOperand.setParseAction(_ParseOperand)

# Types ###########################################################
# FT* patterns
_FType = _FIdentifier.copy()  #TODO add type definitions?
_FType.setName('typename')
_FNamesKeyword = _CKOneOf("names nm")   # for declarations

# Expressions ###########################################################

# Arithmetic operators
_FUnaryArithOp = Literal('-') | _CK('neg')
                    # No unary + ; we don't need it.

_FExpOp = _CK("tothe")   # save ^ for xor; don't use ** since
                                    # accidentally doubling a character should
                                    # not succeed silently.
_FMulOp = _CKOneOf("times ti div di mod mo")
    # the text forms can be used without the
    # shift key.
_FAddOp = _CKOneOf("plus minus pl mi")

# Range operators
#_FRangeBinaryOp = _CKOneOf("to downto")
_FRangeBinaryOp = (_CK("to").setDebug(DOTEST.DEBUG).setName('rangeto')  |
                 _CK("downto").setDebug(DOTEST.DEBUG).setName('rangedownto'))
#_FRangeTernaryOps = (_CK("by").setDebug(True).setName('terrangeby'),
#                     _CK("to").setDebug(True).setName('terrangeto'))
_FRangeStrideOp = _CK("by").setDebug(DOTEST.DEBUG).setName('terstride')
    # Range constructors.  1 to 5 is [1,2,3,4,5].  1 to 5 by 2 is [1,3,5]
    # The "by" can be positive or negative.  If "downto" is used,
    # the "by" must be negative.

_FElementReassignmentOp = _CK('gets')
    # arr = arr[but,5 gets 3]  ; arr[5]=3

# Bitwise operators.  These are advanced, so they all use the shift key.
# However, I'll provide non-shift versions as well, per the requirement.
# Examples of bitwise operators from which the precedence is drawn (C style):
#   set bit in mask: x | (1<<k)
#   clear bit in mask: x & (~(1<<k))
#   test bit: (x & (1<<k)) == (1<<k)
#   test bit of set: (x & mask) != 0
# The precedence is similar to Python.
# What about combinations of arith and bitwise ops?  I don't recall seeing
# many in code; here's what I can think of at the moment:
#   Extract multibit field: if( ((x & mask) >> ofs) > 3) { print("spl"); }
#       This requires override if shifts are higher-precedence than
#       masks, but I don't think that's too bad for all I've seen.
# Ooh - what about an operator for this?
#   x field mask = an lvalue or rvalue of just the bits in _mask_, which
#       must be contiguous.  Then the above would be
#           if(x field mask > 3) { ... }
#   or perhaps
#           if(mask of x > 3) { ... }  Yeah!  I like that.
#   if (1<<5) of x > 3  --- so "of" binds weaker than shl/shr
#   if mask of x & 101b == 101b  --- so "of" binds tighter than and/or.
#   Also allow bitof: 5 bitof x  <=>  1<<5 of x
# OK, so the original question: arith/bit combos?  For now I will just leave
# all bit below all arith, and we'll see what happens.

# Types:
#   - Numeric has int and float.
#   - Bitnumber (name TODO) has specific number of bits and signedness.
#   - Bitmask is used with "of" to pull out specific bits.
#   - Field is a reference to some bits in a bitnumber.  It can be
#       an lvalue or an rvalue.

# Conversion ops
_FUnaryBitmaskOp = _CK("bit")
    # "bit 2" makes (Bitmask)(1<<2).
    # Range constructors have higher precedence than bit so you can say
    # bit 3 to 5 of foo and get what you expect.

_FUnaryBitnumberOp = _CK("bits")
    # with Numeric n, "bits n" yields Bitnumber(n).

# Real bit ops - only on bitnumbers
_FUnaryBitOp = Literal('~') | _CK("compl")
_FShiftBitOp = oneOf('<< >>') | _CKOneOf("shl shr")
    # << could be a typo for <, and likewise >> for >.
    # It's not a problem because int doesn't implicitly convert to bool,
    # so if x<<5 will fail with "if expected a bool but got a number".
_FOfBitOp = _CK("of")
    # given Bitmask m and Bitnumber n, "m of n" makes Field(n, m)
    # "all of n" makes Field(n), which includes all bits.

_FCombineBitOp = oneOf('& | ^') | _CKOneOf('bitand bitor bitxor')

# Relational operators
_FRelOp = oneOf("< > <= >= <>") | _CKOneOf('lt gt eq le ge ne')
    # not =, which is the assignment operator.
    # "==" is STRICTLY FORBIDDEN!  In fact, no operator may be defined
    # as a doubled version of another operator with a different meaning,
    # as noted above with reference to "tothe".

# Logical operators
_FUnaryLogicalOp = _CK("not")
_FLogicalOp = _CKOneOf("and or xor nand nor xnor")  # save &, |, ^ for bitwise

# Ternary operator a la Python - cond?a:b is expressed as a if cond else b
_FTernaryOps = (_CK("if"), _CK("else"))
    # TODO find out if this conflicts with the syntax of statements

# - Parse actions ---------------------------------------------------------

@_unwrapt0
def _ParseApply(s,l,t):
    """t[0] is the function and t[1] the arglist.  If there are multiple
        applies, build a node tree."""
    #pdb.set_trace()

    # TODO Build an apply chain for however many applies there are.
    if len(t)>2:
        raise ParseFatalException(s,l, 'At present, chained applies are'+
                                       ' not implemented.  Sorry! (%s)'%str(t))

    # Process target
    retv = t[0] # an FAReadVar, probably

    # Make sure the target actually exists
    declared_ty = _RealTy(retv)
    if declared_ty is fty.FTIdentifier:
        raise ParseFatalException(s,l,
            ("I can't call a function on '%s' since I don't know"+
             " what kind of thing it is.")%retv.pvName)
    #endif

    for params in t[1:]:  # foreach apply
        # Process args
        if isinstance(params, ast.FAList):
            # It was a parameterized arg list with commas.  Grab the args.
            #pdb.set_trace()
            params = tuple(params.whole_msg)
        if not isinstance(params,tuple):
            params=(params,)
            # single-parameter calls don't get wrapped by _ParseComma,
            # so add the tuple here for regularity.

        #TODO once function declarations are added, look up the function
        #declaration and get the type of the return value.

        # Process the first arg specially.
        # It can be an unquoted string - like a method name.
        firstparam = params[0]
        if (isinstance(firstparam, ast.FAReadVar) or
            isinstance(firstparam, ast.FAIdent)):
                # FAIdents are special names; FAReadVars are normal.
            declared_ty = _RealTy(firstparam)
            if declared_ty is fty.FTIdentifier:
                # t[0] is an undeclared variable.  Change
                # it to a string literal.
                params = tuple(
                    [ast.FAConst(fty.FTString, firstparam.pvName, s,l)] +
                    list(params[1:]))

        retv = ast.FAApply(retv, params, _Env.G, s,l)
    # end foreach apply

    return retv
# end function _ParseApply

@_unwrapt0
def _ParseComma(s,l,t):
    """Remove the commas so what is left is a list of operands."""
    # Stash the items in a list.  If these are arguments of an apply,
    # _ParseApply will extract the items from the FAList
    # and build an FAApply.
    #pdb.set_trace()
    return ast.FAList(_Env.G, tuple(t[::2]), s,l)
        # ::2 => every other element is an operand.
        #   The slice returns a list, not a ParseResults instance.

def _ParseAssocBuilder(astclass):
    """Returns a parser function for associative operators that
        returns an instance of _astclass_.  _astclass_ is assumed to
        take the tokens in source order and process them appropriately
        depending on the associativity of the operator."""

    @_unwrapt0
    def innerParseAction(s,l,t):
        """The parse action being build by _ParseLAssocBuilder"""
        #pdb.set_trace()
        # Typecheck.  For now, all elements have to have the same type.
        ty = _RealTy(t[0])

        if ty is fty.FTIdentifier:
            raise ParseFatalException(s,l,
                ("I don't know what kind of value %s remembers.  "
                "Did you declare it with 'names'?")%t[0])

        for idx in range(2,len(t),2):
            e = t[idx]
            ok = False
            e = _Promote(e,ty)
                # Make it match the desired type if possible
            if _RealTy(e) is not ty:
                #BP()
                #pdb.set_trace()
                errmsg = ( 'While looking for %s, I found that item %d (%s) '
                            'was a %s')%(
                        str(ty), idx/2+1, str(e), str(e.ty))
                #print('  -> '+errmsg,file=stderr)
                    # because otherwise the error message is just an
                    # 'expected end of string' or some such
                raise ParseFatalException(s,l, errmsg)
        return astclass(ty, tuple(t), s,l)
            # Freeze the operands in a tuple - don't carry around a ParseResults
            # object once we're out of the parser.
    # end inner function innerParseAction

    return innerParseAction
# end function _ParseLAssocBuilder

#_ParseRangeOp = _ParseAssocBuilder(ast.FARange)

@_unwrapt0
def _ParseRangeOp(s,l,t):
    if t[0].ty != fty.FTInteger:
        raise ParseFatalException(s,l,
            'Before "to" I expected a number, but I saw %s'%str(t[0]))
    if t[2].ty != fty.FTInteger:
        raise ParseFatalException(s,l,
            'After "to" I expected a number, but I saw %s'%str(t[2]))

    if t[1].lower()=='to':
        incr_amount = ast.FAPlusOne
        up = True
    elif t[1].lower()=='downto':
        incr_amount = ast.FAMinusOne
        up = False
    else:
        raise ParseFatalException(s,l,
            'I expected "to" or "downto", but I saw %s'%str(t[1]))
    #endif

    #pdb.set_trace()
    theretval= ast.FARange(t[0].ty, t[0], t[2], up, incr_amount, s, l)
        # TODO fix this - it's not actually t[0].ty
    return theretval

# end _ParseRangeOp

@_unwrapt0
def _ParseStrideOp(s,l,t):
    """ Takes <range> by <integer> and returns a new range with the
        same start and end but with the stride filled in. """
    # Should have FARange, 'by', stride.
    #pdb.set_trace()
    if not isinstance(t[0], ast.FARange):
        raise ParseFatalException(s,l,
            "'by' can only be used on a range (e.g., 1 to 5).  " +
            "I saw %s."%t[0])
    if t[1].lower()!="by":
        raise ParseFatalException(s,l,
            'Saw %s but expected foo by bar'%str(t))

    if t[2].ty != fty.FTInteger:
        raise ParseFatalException(s,l,
            'After "by" I expected a number, but I saw %s'%str(t[2]))

    theretval = ast.FARange(t[0].ty, t[0].first, t[0].last, t[0].up, t[2])
    #pdb.set_trace()
    return theretval
#end _ParseStrideOp()

@_unwrapt0
def _ParseReassignment(s,l,t):
    #pdb.set_trace()

    if len(t)!=3:
        raise ParseFatalException(s, l,
            '"gets" only takes two operands (saw %s)'%str(t))

    # Process destination index
    idx = t[0]
    #pdb.set_trace() # TODO what type is idx?
    if isinstance(idx, ast.FAConst):
        idxty = idx.ty
    else:
        destsym = _LookupInG(idx.pvName, s,l, 'reassignment')
            # throws if idx doesn't exist
        idxty=destsym.ty
    #endif

    if idxty is not fty.FTInteger:  # Type-check index
        raise ParseFatalException(s,l,'Attempt to index an array '+
            'with non-integer variable %s in a reassignment'%idx.name)
        # TODO expand this to permit other index types, e.g., ranges

    # Process new value
    newval = t[2]
    # TODO type check newval

    return ast.FAReassignment(idx, newval, s, l)
        # NOTE: not ParseAssocBuilder since
        #   - can't be chained (1 gets 2 gets 3 is not meaningful)
        #   - operands are different types - `number' gets `real' is fine
        #       if it's a vector.
# end function _ParseReassignment

_UMinusInnerParser = _ParseAssocBuilder(ast.FAArithExp)
@_unwrapt0
def _ParseUMinus(s,l,t):
    """ Parse a unary minus operator.  At present, this is somewhat
        hacked.  It checks the type of the value provided and makes a
        zero of that type, then parses '0-x' (of that type).
    """
    ty = t[-1].ty
    zero = ast.FAConst(ty, 0, s, l)
    return _UMinusInnerParser(s,l,[[zero,'-',t[-1]]])
# end _ParseUMinus()

@_unwrapt0
def _ParseUnaryLogicalOp(s,l,t):
    """ Parse a unary _not_.  t[0]=='not', t[1] is an expr. """
    return ast.FALogicalExp(fty.FTBoolean,
        [ast.FATrue, 'not', t[1]],  # the FATrue is a placeholder
        s, l)
# _ParseUnaryLogicalOp

@_unwrapt0
def _ParseFlagArg(s,l,t):
    if len(t)!=3:
        raise ParseFatalException(s, l,
            'Flags can only take at most one operand (saw %s)'%str(t))

    if t[0].ty is not fty.FTFlag:
        raise ParseFatalException(s, l,
            'The thing before a slash has to be a flag (e.g., /foo/arg)' +
            ' (saw %s)'%str(t))

    if t[2].ty is fty.FTFlag:
        raise ParseFatalException(s, l,
            "The thing after a slash can't be a flag (e.g., /foo/arg)" +
            ' (saw %s)'%str(t))

    #pdb.set_trace()
    # Assemble
    t[0].bind(t[2]) # No need to duplicate the flag since it can't
                    # be used anywhere else (as far as I know).
                    # TODO confirm this.
    return t[0]
#end _ParseFlagArg

# Setting up to parse an Apply
def _ParseApplyStarter(s,l,t):
    _Env.is_first_arg = True
        # We're starting an apply, so the next _FOperand we sill will
        # be the first.
# _ParseApplyStarter

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# _FExpr

setrecursionlimit(getrecursionlimit()*2)
    # Empirical.  On my test system, gem[say,gem[ask]] overflows the
    # recursion limit if enough clauses in _FExpr are enabled.
    # TODO replace this with the improved parser in pyp.

### NOTE!!!
# infixNotation wraps all its results in a Group().  As a result,
# every parse action for an inner expression (i.e., every parse action
# listed in the opList) receives t==a single-element ParseResults
# of the Group.
_FExpr = infixNotation(  baseExpr=_FOperand,
                        lpar=Suppress('['),
                        rpar=Suppress(']'),
                        name='FExpr',
       opList=[# high to low precendence
        #(expr,numterms, rl, parseaction),

        # Message sending
        (FollowedBy('[').setName('send').setParseAction(_ParseApplyStarter),
            2,opAssoc.LEFT, _ParseApply),

        # Arithmetic
        (_FUnaryArithOp, 1, opAssoc.RIGHT, _ParseUMinus),      #Prefix
            # At present, - is the only unary op ^^^^^^
        (_FExpOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FAArithExp)),
            #Left seems to make more sense in phrases like 2 tothe 3 tothe 5,
            # i.e., 2 to the 3, all to the 5.
        (_FMulOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FAArithExp)),
        (_FAddOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FAArithExp)),

# TODO add these operators back in gradually

        # Range
            # TODO add parse functions for these
        (_FRangeBinaryOp, 2, opAssoc.LEFT, _ParseRangeOp),  # to, downto
        (_FRangeStrideOp, 2, opAssoc.LEFT, _ParseStrideOp),

#        # Conversion ops - convert numbers to bitnumbers.
#        (_FUnaryBitmaskOp, 1, opAssoc.RIGHT),
#        (_FUnaryBitnumberOp, 1, opAssoc.RIGHT),
#
#        # Bitwise - operate on bitnumbers
#        (_FUnaryBitOp, 1, opAssoc.RIGHT),
#        (_FShiftBitOp, 2, opAssoc.LEFT),
#            # so 1<<2<<4 shifts by 6, i.e., first by two bits and then
#            # by four bits.  Otherwise, 1<<(2<<4) (right-assoc) would shift
#            # left by 32 bits, which seems unexpected.
#            # I wish PyParsing had opAssoc.NONE.
#        (_FOfBitOp, 2, opAssoc.LEFT),
#            # Here mask of val1 of val2 doesn't make any sense right-assoc.
#            # Mask of val1 must produce a mask for val2.
#        (_FCombineBitOp, 2, opAssoc.LEFT),
#
        # Relational
        (_FRelOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FARelExp)),

        # Logical
        (_FUnaryLogicalOp, 1, opAssoc.RIGHT, _ParseUnaryLogicalOp),
        (_FLogicalOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FALogicalExp)),

#        # Ternary
#        (_FTernaryOps, 3, opAssoc.RIGHT)

         # Element assignments ('gets'):
         # higher precedence than comma so can make comma-separated lists
         # such as    1 gets 2, 3 gets 4
         (_FElementReassignmentOp, 2, opAssoc.LEFT, _ParseReassignment),
            #_ParseAssocBuilder(ast.FAReassignment)),

         # Flag options, which can be almost any full expression.
         # Above comma so that a flag takes up a single arg slot.
         # E.g., [/foo/1,/bar/2] is two parms, but /foo/[1,2] is one with
         # a list as an arg.
         (_FFlagArgOp, 2, opAssoc.LEFT, _ParseFlagArg),
         #Building messages: comma.  Lowest precedence.
         (',',2,opAssoc.LEFT, _ParseComma),  # Argument lists or anonymous function calls

       ],
       debug=False
) #_FExpr
_FExpr.setName('expr')
_FExpr.setDebug(DOTEST.DEBUG)

#DOTEST.DOIT=True
#test(_FExpr, "1 to 5 by 2",True,True)

#_FExpr.setParseAction(
#    lambda s,l,t: ast.FAExpr(None, frplutil.flatten(t), s,l))
    # Since everything is postfix, flatten it to a single list.
    # We need to put the flattened list in a list of its own so that
    # postfix sublists will be kept whole when combined with infix
    # parse results as input to In2Post.
    # TODO typecheck and give the expression the right type.

#DOTEST.DOIT=True
#DOTEST.DOIT=False

#test(_FExpr,'/foo/1',True,True)
#DOTEST.DOIT=True
#pdb.set_trace()
    # We don't test whether the condition holds until runtime.
#DOTEST.DOIT=False
#DOTEST.DOIT=True
#DOTEST.DOIT=False
#test(_FExpr,'1<2<3',True,True)         # TODO turn these back on when
#test(_FExpr,'1 lt 2 eq 3',True,True)   # I update FARelExp to permit chained
#DOTEST.DOIT=False
# Tests: operands and unaries
#test(_FExpr, "-42", True)      # TODO add when I turn UnaryArithOp back on
#test(_FExpr, "--x", True)      # ditto
    # no more unary +

    # This case shows why the _FExpr parse action is [flatten()] rather than
    # just plain flatten() (without wrapping in a list).


# Exponentiation - left-associative

# Multiplicative operators

# Additive operators

#test(_FExpr, "~42", True)   # TODO add these tests back in when
                            # _FExpr is expanded.
#test(_FExpr, "1&0ffh", True)
#test(_FExpr, "~10h", True)
#test(_FExpr, "~10h+1", True)
#test(_FExpr, "(~10h)+1&0ffh", True) # to do - test this
#test(_FExpr, "~(10h+1)&0ffh", True) # to do - test this
#test(_FExpr, "~10h+1&0ffh", True) # to do - test this
#test(_FExpr, "3*4+-7.09", True)  # TODO put back in when unary minus added
#test(_FExpr, "0c0h of 7fffh", True)
#test(_FExpr, "x | 1<<5", True)
#test(_FExpr, "x & ~(1<<5)", True)
#test(_FExpr, "3 bitof x if cond else false", True) #Fails - TODO figure out why

# Statements ###########################################################
# FEOL is a statement terminator, so each statement clause includes the FEOL.
# Also, each statement ignores comments.
# FS* patterns
#DOTEST.DOIT=True

_FStatement = Forward()

_FSNop=(Optional(_CK('nop'))+_FEOL).setName('nop')    # do-nothing
_FSNop.ignore(_FComment)
_FSNop.ignore(Regex('^[[:space:]]*$').leaveWhitespace())
_FSNop.setParseAction(lambda s,l,t: ast.FANop(s,l))

#----------------------------------------------------------------
# - Existence Assertions ("is") -

_FSIsClause = (
    (_CK('kind') + Optional(_CK('of') - _FIdentifier)) |    # kind [of <ty>]
    (_CK('like') - _FIdentifier) |  # make a kind from a freestanding function
    (_FIdentifier - _FExpr).setName('const')    # <type> <value>
)
_FSIs = (_FIdentifier + _CK('is') - _FSIsClause + _SEOL)

def _ParseIs(s,l,t):
    # TODO RESUME HERE - fill in "like" first.
    pass
# _ParseIs()

#----------------------------------------------------------------
# - Variable Declarations ("names") -

#_FSVarDeclaration=(_FIdentifier+Suppress(_CK("names")|_CK("nm"))+
#                    mustHave(_FType)+_SEOL)
_FSVarDeclarationNoEOL = Group(_FIdentifier+Suppress(_FNamesKeyword) - _FType)
_FSVarDeclaration=(_FIdentifier+Suppress(_FNamesKeyword) - _FType+_SEOL)
    # "-" begins things that have to match
    # "names" because the var is actually a reference, i.e., name of an obj
_FSVarDeclaration.ignore(_FComment)

_FSVarDeclaration.setName('declaration').setParseAction(_ParseVarDeclaration)
    # _ParseVarDeclaration is defined up at the top of the file, above the
    # test harness.
#_FSVarDeclaration.ignore(_FComment)


#----------------------------------------------------------------
# - Assignments -

_FSAssignment=_FIdentifier+Suppress("=") - _FExpr+_SEOL
    # minus sign is because once you have the equals sign, you're committed to
    # seeing an expression next, and no backtracking will succeed.
    # Only identifiers can be lvalues because instances are immutable.
    # Reassignments are handled with "but".
_FSAssignment.ignore(_FComment)

_FSAssignment.setName("_FSAssignment")
#_FSAssignment.ignore(_FComment)


def _ParseAssignment(s,l,t):
    #lambda s,l,t: ast.FAAssignment(t[1].ty, t[0], t[1], s,l))
    # 0=>dest; 1=>expr
    #print('parsing assignment (env='+repr(_Env.G)+')')  #debug
    destid=t[0] #FAIdent
    rvalue=t[1] #FAExpr
    if _Env.G is None:      # true during testing
        return ast.FAAssignment(t[1].ty, t[0].pvName, t[1], s,l)

    #runtime: _Env.G is not None
    # Lookup dest in env
    #pdb.set_trace()
    destsym = _LookupInG(destid.pvName, s,l)   # an ast.Entry

    # TODO
    # Type-check rvalue against dest
    #DEBUG: type-checking commented out
    #if rvalue.ty != destsym.ty:
    #    raise ParseFatalException(s,l,
    #        "I don't know how to make a %s variable remember a %s"%(
    #            str(destsym.ty), str(rvalue.ty)))

    #pdb.set_trace()
    # We have a good assignment.  Remember it.
    return ast.FAAssignment(destsym.ty, destid.pvName, rvalue, s,l)
        # TODO figure out whether the type of an assignment statement
        # should always be None or something else.
# end _ParseAssignment

_FSAssignment.setParseAction(_ParseAssignment)

#_FSAssignment.setBreak()
# TODO put these back in when lvalues are FExprs again
#test(_FSAssignment, 'x[5]=42',True)
#test(_FSAssignment, 'x[item,5,10] =  [ask,how,are,you,today,query]',True)
#test(_FSAssignment, '  arr[4,foo]=39', True)

# Compound statements: insides.  The delimeters are part of the statements
# since they are different for different statements.
def _ParseStatementChunk(s,l,t):
    #pdb.set_trace()
    return ast.FACompoundStmt(t[:], s,l)
        # Wrap up all the statements in one AST node.
        # Deep copy to drop the ParseResults.

_FStatementChunk = OneOrMore(_FStatement).setName('StmtChunk')
    # TODO determine whether I can allow empty statement chunks -
    # currently, like Python, they have to be occupied by a nop.
_FStatementChunk.setParseAction(_ParseStatementChunk)
_FStatementChunk.setDebug(DOTEST.DEBUG)

#----------------------------------------------------------------
# - Blocks -
# Blocks are just to define nested scopes.
_FSBlock = (Suppress(_CK('block').setParseAction(pushG)) - _SEOL +
            _FStatementChunk+
            _SCK('ta')+_SEOL)
_FSBlock.setName('block')
def _ParseBlock(s,l,t):
    #pdb.set_trace()
    return ast.FABlock(popG(s,l,t), t[0])

_FSBlock.setParseAction(_ParseBlock)

#----------------------------------------------------------------
# - Conditionals -

_FSElseIfSuppressed=_SCK("elif")
    # TODO determine experimentally whether this
    # makes sense to the target audience.
_FSConditional = (_SCK("if") - _FExpr + _SEOL +
                     _FStatementChunk +     # true
                     Optional(
                        OneOrMore(
                            _FSElseIfSuppressed - _FExpr + _SEOL +
                            _FStatementChunk
                        ).setName('condElifInner')
                     ).setName('condElif') +
                     Optional(
                        _SCK("else") - _SEOL +
                        _FStatementChunk    # false
                    ).setName('condElse') +
                 _SCK("ta")+_SEOL)
    # TODO add elseif, else
_FSConditional.setName('_FSConditional')
_FSConditional.ignore(_FComment)  # for comments on the if or ta lines

def _ParseConditional(s,l, t):

    # Sort out the clauses

    nclauses = len(t)
        # nclauses=2: if/then.
        # nclauses=3: if/then/else
        # nclauses>3 and even: if/elif chain
        # nclauses>3 and odd: if/elif chain with else

    conds=[]
    acts=[]
    for idx in [2*i for i in range(0,int(nclauses/2))]:
        # Idx steps through the cond/act pairs, omitting the _else_.
        conds += [t[idx]]
        acts += [t[idx+1]]
    # next idx

    # Add an else clause, if one was provided
    if nclauses>2 and (nclauses%2==1):
        conds += [ast.FATrue]
        acts += [t[nclauses-1]]
    #end if

    # Add a True=>nop at the end if required.  FAConditional requires
    # that there always be a condition that succeeds.

    if nclauses==2 or (nclauses>3 and nclauses%2==0):
        conds += [ast.FATrue]
        acts += [ast.FANop()]
    #end if

    # Build the node

    try:    # TODO figure out if this way of error-checking makes sense.
            # Should frplparse pre-check everything before creating the
            # ast nodes?
        #return ast.FAConditional(t[0], t[1], t[2], s,l)
        return ast.FAConditional(conds, acts, s,l)
    except Exception as e:
        raise ParseFatalException(s,l, 'Could not parse if-statement') from e
# end _ParseConditional

_FSConditional.setParseAction(_ParseConditional)

#----------------------------------------------------------------
# - Loops -
# Every loop defines a block.
_FSLoopClauseFor=_FIdentifier - Suppress(_CK('from')) + _FExpr
    # TODO add other options here ('forever', 'with test', 'while', 'until'
    # Note: the _FExpr should be a range.
_FSLoopClauseFor.setName('for-clause')
    # TODO add "loop x in <iterable>"

def _ParseLoopClauseFor(s,l,t):
    """t[0] is an identifier and t[1] an expr, which must be a range."""
    #pdb.set_trace()
    if not isinstance(t[1], ast.FARange):
        raise ParseFatalException(s,l,'Need a binary range in a for loop; got '+str(t[1]))

    ident = t[0]
    loop_range = t[1]

    # Declare identifier if not already declared
    decl = None
    if _RealTy(ident) is fty.FTIdentifier:  # undefined, so define it
        decl = _ParseVarDeclaration(s,l,[ident,loop_range.ty])

    # Get the Entry

    # Build clause
    destsym = _LookupInG(ident.pvName, s,l)
    init = ast.FAAssignment(destsym.ty, ident.pvName, loop_range.first, s,l)

    # Build the increment
    curridx = ast.FAReadVar(destsym.ty, ident.pvName.lower(), s,l)
    newidx = ast.FAArithExp(curridx.ty,
        [curridx, 'pl', loop_range.stride_amount],s,l)
        # Add the range's increment, which will be an fty.FTInteger
    incr = ast.FAAssignment(destsym.ty, ident.pvName, newidx, s,l)

    # Build the test
    pretest = ast.FARelExp(curridx.ty,
        [curridx, '<=' if loop_range.up else '>=' , loop_range.last], s,l)
        # Grab the right relop from the range, which knows whether it's
        # an up range (test <=) or a down range (test >=)

    # Put the clause together
    #pdb.set_trace()
    clause = ast.FALoopClause(
                init, incr, pretest=pretest, decl=decl #decl may be None
            )   # no posttest in a for loop

    return clause

# end _ParseLoopClauseFor

_FSLoopClauseFor.setParseAction(_ParseLoopClauseFor)

#DOTEST.DOIT=True
#DOTEST.DOIT=False

_FSLoop=(Suppress(_SCK('loop').setParseAction(pushG)) - _FSLoopClauseFor + _SEOL +
            #^ this Suppress is to drop the return value of pushG()
            _FStatementChunk +
            _SCK('next')+_SEOL)
_FSLoop.ignore(_FComment)
_FSLoop.setName('loop')

def _ParseLoop(s,l,t):  #t has the clause, then the statement chunk.
    #pdb.set_trace()
    clause=t[0]
    body=t[1]
    return ast.FABlock(popG(s,l,t), ast.FALoop(clause, body, s,l))
        # Wrap the loop in a scope of its own.

_FSLoop.setParseAction(_ParseLoop)
_FSLoop.ignore(_FComment)

##----------------------------------------------------------------
## - Parameter Lists -
#
## PList AST (_PLA*) --------------------------------------------------
#
#class _PLANode(object):
#    def __init__(self, s, l):
#        self.source_string = s  # for debugging
#        self.source_loc = l
#    # __init__
#
#    def __repr__(self): return str(self)    # for convenience
#
#    def _loc(self):
#        return "line %d, char %d"%(lineno(self.source_loc, self.source_string),
#                                   col(self.source_loc, self.source_string))
#    # _loc()
#
#    def ipattern(self) -> str:
#        """ Returns a string pattern that can be matched by FWorker.invoke
#            and related functions. """
#        return str(self)
#    #end ipattern()
#
## class _PLANode
#
#class _PLAEntry(_PLANode):
#    """ An individual entry."""
#    # TODO figure out how to handle flags
#    def __init__(self, isflag, name, ty, s, l):
#        super().__init__(s,l)
#        self.isflag,self.name,self.ty=isflag,name,ty
#    def __str__(self):
#        return "(%s%s nm %s)"%(
#            'flag ' if self.isflag else '',
#            self.name, 'any' if self.ty is None else self.ty)
#    def ipattern(self):
#        if self.isflag:
#            return FAFlag.ipat(self.name, self.ty)
#        else:
#            return str(self.ty) if self.ty is not None else 'any'
##end class _PLAEntry
#
## PList parser (_PL*) ------------------------------------------------
#_PLFlagEntry = (
#    _CK('flag').setParseAction(lambda s,l,t: True) -
#        # True: it's a flag
#    _FIdentifier +
#    Optional(_SCK('with') + _FType)
#).setName('p-flag-ent')
#
#_PLVarEntry = (
#    Empty().setParseAction(lambda s,l,t: False) +
#    _FIdentifier -
#    Optional(Suppress(_FNamesKeyword) - _FType)
#).setName('p-var-ent')
#
#_PLBaseEntry = _PLFlagEntry | _PLVarEntry
#_PLBaseEntry.setName('p-base-ent')
#
#def _ParsePLBaseEntry(s,l,t):
#    isflag = (t[0] is True)
#    name = t[1]
#    ty = t[2] if len(t)>2 else None
#    return _PLAEntry(isflag,name,ty,s,l)
##_ParsePLBaseEntry()
#
#_PLBaseEntry.setParseAction(_ParsePLBaseEntry)
#
#_PLNumber = Word(nums)  # no based numbers for simplicity, but not negative
#_PLCount = (
#    _CK("any").setParseAction(lambda s,l,t: None) |
#    _PLNumber.setParseAction(lambda s,l,t: int(t[0]))
#).setName('p-count')
#
#_PLRange = Group(_PLCount + Optional(_SCK("to") - _PLCount))
#_PLRange.setName('p-range')
## TODO add parsing function to make sure range is increasing
#
#_PLEntry = Group(
#    Optional( _CK('optional').setName('p-optional') | _PLRange ) +
#    _PLBaseEntry
#).setName('p-entry')
#
#@_unwrapt0
#def _ParsePEntry(s,l,t):
#    #pdb.set_trace()
#    if t[0]=='optional':
#        rng=[0,1]
#    elif len(t)>1:  # t[0] is a range
#        rng=t[0][:2]
#        if rng[0] is None: rng[0]=0
#        if rng[1] is None: rng[1]=sys.maxsize
#    else:
#        rng=[1,1]
#    #endif
#    retv = dict(r=rng,e=t[-1])
#        # TODO wrap this in a class?
#    #pdb.set_trace()
#    return retv
## _ParsePEntry
#_PLEntry.setParseAction(_ParsePEntry)
#
#_PList = delimitedList(_PLEntry, delim=',')
#_PList.setName('p-list')
#
#def _ParsePList(s,l,t):
#    print(str(len(t)) + ": " + str(t))
#    pass
## _ParsePList
#
#_PList.setParseAction(_ParsePList)
#
#DOTEST.DOIT = False#True
#DOTEST.SILENT = False
#test(_PLEntry,'foo nm text',True,True)
#test(_PLEntry,'1 to 10 foo nm text',True,True)
#test(_PLEntry,'1 to any foo nm text',True,True)
#test(_PLEntry,'any to 10 foo nm text',True,True)
#test(_PList,'foo nm text, 1 to 10 bar, quux, any to any quuux nm number',
#    True,True)
## TODO RESUME HERE - why does the next line fail?  Check FIdent == 'bar'.
#test(_PLEntry,'any bar',True,True)
#test(_PLEntry,'any bar nm blag',True,True)
#test(_PList,'any bar',True,True)
#test(_PList,'any bar nm blag',True,True)
#test(_PList,'optional foo',True,True)
#test(_PList,'optional foo nm x',True,True)
#test(_PList,'any bar,any bar nm blag, optional foo, optional foo nm x',
#    True,True)  # TODO RESUME HERE - _ParsePEntry is failing
#test(_PList,'optional 1 to 10 foo',False)   # can't combine optional and range
#DOTEST.DOIT = False
#
##_FSFnParamList = infixNotation(  baseExpr=_FIdentifier,
##                        lpar=NoMatch(), # no nesting in param lists
##                        rpar=NoMatch(),
##                        name='ParmList',
##       opList=[# high to low precendence
##        #(expr,numterms, rl, parseaction)
##         (_FNamesKeyword, 2, opAssoc.LEFT, _PLParseDecl),
##         ('some', 1, opAssoc.PREFIX, _PLParseSome), # >= 1
##         (',',2,opAssoc.LEFT, _PLParseComma),
##        ]
##) # _FSFnParamList
##

# Function headers

#_FSFnHdr =  ( Suppress(_CK('when')) - _FIdentifierPart +
#                    # TODO update if "me" is special-cased.
#                    # Note: name can't have dots (IDPart, not ID)
#                Suppress('[')+Regex(r'[^]]+').setName('arg-pat')+
#                                                                Suppress(']') +
#                Optional(Suppress(_FNamesKeyword)+_FType) +
#                Optional(_CK("flex"))    # flex return types
#            )

#Basic version for initial tests
_FSFnHdrBasic =  ( Suppress(_CK('when').setParseAction(pushG)) -
                    _FIdentifierPart +
                    # TODO update if "me" is special-cased.
                    # Note: name can't have dots (IDPart, not ID)
                        # TODO make name able to have dots
                    Suppress('[')+
                    Group(delimitedList(_FSVarDeclarationNoEOL)
                         ).setName('argl')+
                    Suppress(']') +
                    Optional(Suppress(_CK('ans'))+_FType)
                )
_FSFnHdrBasic.setName('fn-hdr-basic')


def _ParseFnHdrBasic(s,l,t):
    """ Parse a function header.  _Env.G when this runs is the environment
        of the function body.  _Env.G.outer is the environment
        in which the function is to be visible. """

    flags = ast.FKMethFlags.DefaultFlags
    fname = t[0].lower()
    argpattern = t[1]   # TODO replace this with an ast of args?

    #arglist = _PList.parseString(argpattern)

    if len(t)<3:
        retty = fty.FTNone
        have_retty = False
    else:
        retty = _ResolveTypeRef(t[2])
        have_retty = True
    #endif

    if retty is None:
        # Since a retty was specified but we don't recognize it,
        # throw an error.
        raise ParseFatalException(s, l,
            ("I'm afraid I don't know what kind of return type "
             "'%s' is."%str(t[2])))
    #endif
    flags |= ast.FKMethFlags.HasDefaultRetty
        # ignored if not flex, so we can set it here.

    # Add a Kind to the enclosing environment.  Do this here because
    # AST nodes don't have access to the environment until eval().

    inside_kind = (fname == _ME)
    if inside_kind:
        raise ParseFatalException(s, l, 'when me[...] not yet implemented')

#        # Do we have an instance in the current environment?
#        theenv = _Env.G.find(ast.PVN_CURRENT_ME)
#        if theenv is None:
#            raise ParseFatalException(s, l,
#                "I can't declare a 'me' method outside a Kind.")
#        if theenv != _Env.G:
#            raise ParseFatalException(s, l,
#                "I can't declare a 'me' method except at the top"
#                " level of a Kind.")
#
#        kind = theenv[ast.PVN_CURRENT_ME]
#        agency=None # TODO do we need this?

    else:   # not inside a kind
        kindname = '%s@%s'%(fname, shortuuid.uuid())
            # A unique kind name (uuid) that can't be declared ("@") in frpl
        (kind, agency) = ast.MakeKindInEnv(kindname, _Env.G.outer)
            # _Env.G.outer since we're already       ^^^^^^^^^^^^
            # inside the scope of the function body.
    #endif "me" else

    # Declare a single instance called fname to the enclosing environment
    if not inside_kind:     # inside a kind, the instance already exists.
        inner_env = _Env.G
        try:
            _Env.G = _Env.G.outer   # need to pop up to the caller for a sec
            _ParseVarDeclaration(s, l, [fname, kind.worker_ty])
        finally:
            _Env.G = inner_env
        #end try
    #endif not inside_kind

    # Declare the method.  We have to do this here to support recursion.

    # Build the invocation pattern.  For now we are only doing nonames.
    param_names, param_ty_ids = zip(*argpattern)
        # unzip - https://docs.python.org/3/library/functions.html#zip

    param_names = [x.pvName for x in param_names]
        # unwrap the FAIdents to strings

    # Lookup the types
    param_tys = []
    for tyid in param_ty_ids:
        ty = _ResolveTypeRef(tyid)
        if ty is None:
            raise ParseFatalException(s, l,
                ("I'm afraid I don't know what kind of parameter type "
                 "'%s' is."%str(tyid)))
        #endif
        param_tys.append(ty)
    # next tyid

    # Declare the method on the kind based on the argpattern, retty, and
    # flags
    hdl = kind.declare_noname_method(param_tys, ast.FKind.ForwardFunction,
                                retty, flags)

    # Add declarations of the formal parameters to the current environment
    for arg in argpattern:  # arg = (nm,ty), which matches _ParseVarDecl's input
        _ParseVarDeclaration(s,l,arg)
    # next parm

    # If there is a retval, declare it
    if have_retty:
        _ParseVarDeclarationInternal(s,l,[_ANS,retty],True)

    if inside_kind:
        retv = ast.FAFnHdr(kind, hdl, param_names, s=s, l=l)
    else:
        retv = ast.FAFnHdr(kind, hdl, param_names, fname=fname,
                            s=s, l=l)

    return retv

        # TODO? Move FAFnHdr into here?  Maybe just carry this info in a
        # tuple from _ParseFnHdrBasic into _ParseMemberFn?
# end _ParseFnHdrBasic

_FSFnHdrBasic.setParseAction(_ParseFnHdrBasic)

#DOTEST.DOIT = True
#DOTEST.SILENT = False
#test(_FSFnHdrBasic,'when quux[x nm number] ans text',True,True)
#test(_FSFnHdrBasic,'when quuux[x nm number] ',True,True)
#DOTEST.DOIT = False

# Definitions of member functions
_FSMemberFn = ( # Header ::= "when" "[" <string> "]" ["nm" <type>]
                _FSFnHdrBasic - _SEOL +
                    # Body
                    _FStatementChunk+
                # Trailer
                _SCK('ta')+_SEOL)
_FSMemberFn.setName('member-fn')

def _ParseMemberFn(s,l,t):
    #pdb.set_trace()

    # Collect the info
    hdr = t[0]
    body = t[1]     # an FACompoundStmt

    # TODO check _body_ to make sure it assigns _ANS if there is a retval.

    fn_env = popG(s,l,t)

    # Make the invoker.  TODO make a helper function so this and
    # ast.FABlock.eval() can share code.
    def new_fn(me, invocation, env, retty):
        #pdb.set_trace()
        frame = fn_env.newframe(env) # Make the new stack frame

        # Evaluate the parameters in the caller's environment,
        # and stuff the results into the frame as incoming parms.
        # Typechecking has already been done, so we don't need to repeat it.
        for item, name in zip(invocation, hdr.param_names):
            pv = item.eval(env)
            frame[name].pyval = pv
        # next item

        if _ANS in frame:
            frame[_ANS].pyval = None    # No default - you have to assign it.

        # Run the body
        body.eval(frame)    # an FACompoundStmt doesn't return anything

        # Extract the retval, if any
        if _ANS in frame:
            retv = frame[_ANS].pyval
        else:
            retv = None

        return retv
    # new_fn()

    # Assign the invoker
    hdr.kind.implement_method(hdr.handle, new_fn)

    return ast.FAMemberFn(_Env.G, hdr, body, s, l)
# _ParseMemberFn()

_FSMemberFn.setParseAction(_ParseMemberFn)

#----------------------------------------------------------------
# - Kinds -

# TODO

#----------------------------------------------------------------
# Statement.  TODO figure out if we need ^ instead of |
_FStatement<< ( _FSConditional |
                _FSBlock |
                _FSLoop |
                _FSVarDeclaration |
                # TODO add KindDecl
                _FSAssignment |
                _FSMemberFn |
                _FSNop  |   # covers blank lines and comment lines.
#                StringEnd().setName('StringEnd in statement').setParseAction(
#                    lambda s,l,t: ast.FANop(s,l)) |
                #mustHave(whenfb=Optional(_FIdentifier)+'[',
                #        need=_FExpr+_SEOL).setName("whole-line-expr")
                (FollowedBy(Optional(_FIdentifier).setName('optident')+'[') -
                        _FExpr+_SEOL).setName("whole-line-expr")
                    # handles messages - TODO add an action function
                    # to validate it's an expression allowed in a
                    # statement context.
                    # If it's not an assignment, it should be a message send.
                    # That can have a target, and has an open bracket.
            )
_FStatement.setName('_FStatement')
def _ParseStatement(s,l,t):
    #BP()
    return t[0] # pull the statement out of the ParseAction.
_FStatement.setParseAction(_ParseStatement)

_FStatement.ignore(_FComment)

DOTEST.DOIT=False
DOTEST.SILENT= False
test(_FStatement,
"""if n eq 1
  n=2
else
    n=3
ta""",True,True,[('n','number')])

test(_FStatement,
"""if n eq 4
  n=5
elif n eq 6
  n=7
ta""",True,True,[('n','number')])

test(_FStatement,
"""if n eq 8
  n=9
elif n eq 10
  n=11
else
 n=12
ta""",True,True,[('n','number')])

# Programs #############################################################
_FPackageDecl=Group(_CK('package')+_FIdentifier+_SEOL)


_FImport=Group(_CK('import')+delimitedList(_FIdentifier)+_FEOL)
    # at present, can only import whole packages since 'using' (or whatever
    # the syntax turns out to be) is not defined.

_FImports = OneOrMore(_FImport|_FEOL)


# TODO: Update _FProgram so that any number of preamble statements can occur
# before the normal statements start.
#_FProgram = Suppress(ZeroOrMore(_FEOL)) + (
_FProgram = (
    #Optional(_FPackageDecl) +
        # DEBUG: Don't comment out the above line.  _FProgram has to be
        # a different ParserElement than the below OneOrMore, and this
        # clause handles that.
    #Optional(_FImports) +
    _FStatementChunk
    #OneOrMore(_FStatement).setName('stmts').setParseAction(_ParseCompoundStatement)
        # Handles trailing nops and blanks
) + StringEnd() # + Suppress(ZeroOrMore(_FEOL)) #_FProgram
    # StringEnd so there aren't trailing chars.

_FProgram.setName('Program')
_FProgram.setDebug(DOTEST.DEBUG)    # DEBUG
_FProgram.ignore(_FComment)

def _ParseActionProgram(s,l,t):
    """Parse action for _FProgram"""
#    pdb.set_trace()
    return t[0] # DEBUG: extract the FACompoundStmt from the ParseResults.
#        # TODO make this more sophisticated.  Also, figure out how
#        # to not return a ParseResults() anyway - see pyp.py:987
_FProgram.setParseAction(_ParseActionProgram)
#    # Unwrap the ParseResults around the FACompoundStmt.
#    # TODO improve this when things other than statements are allowed.

def ParseProgram(pgmtext,initialenv=None):
    """Public function to parse a program starting from initialenv,
    or an empty environment if initialenv is omitted or None.
    If guardenv, parse the program into a temporary environment under
    initialenv, then merge that environment into initialenv only if
    the parse is successful. """

    if initialenv is None:
        initialenv = ast.Env()
    SetGlobalEnv(initialenv)    # sets initialenv.outer = fty.Lang

    #pdb.set_trace()
    if DOTEST.DEBUG:
        print('\n--- Parsing %s%s ---\n'%(
                pgmtext[:60], '...' if len(pgmtext)>60 else ''))

    _Env.decls_this_run = []
    try:
        tree = _FProgram.parseString(pgmtext)[0]     # propagate all exceptions
    except:
        # Undo declarations made during the failed parse
        for (env,name) in _Env.decls_this_run:
            del env[name]
        raise   # Let the caller take care of the exception

    retv = ast.Program(tree,initialenv)
    # If we get here, the parse was successful.

    return retv
# end ParseProgram()

# Helper for the interactive parser - an expression with nothing after it
_FExprOnlyLine = (_FExpr + _SEOL).setName('expr and nothing else on the line')
_FExprOnlyLine.ignore(_FComment)

# Exports
ParseExpr = _FExpr.parseString
ParseExprOnlyLine = _FExprOnlyLine.parseString
#frpl_parseProgram = _FProgram.parseString
CheckProgram = lambda s: _FProgram==s
CheckStatement = lambda s: _FStatement==s

#############################################################################

# Most recent two changes:
#   2015/11/14  cxw     Added _ParseApplyStarter; updated per frplast.
#                       FFlag: permitted whitespace between slash and name
#   2015/11/24  cxw     Added _Env.decls_this_run so that you can define
#                       subroutines from an interactive session.
#   2015/12/05  cxw     Renamed FSDecl to FSVarDecl

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

