# README #

Thanks very much for contributing to the effort!

Private repo for development - please do not distribute any of this code yet.  Thank you!

### What is this repository for? ###

* This is the command-line interpreter (and reference implementation) for the frpl language.
* Versioning will be by [Semantic Versioning](http://semver.org)

### How do I get set up? ###

* Summary of set up:
    * Windows: Install cygwin and the Python 3 package
    * Other systems: Install xterm and Python 3
    * Install the dependencies per [fispy-deps](https://bitbucket.org/frpl/fispy-deps)
    * Run "python3 fispy.py"

* Configuration: None yet
* Dependencies: [prompt_toolkit](https://github.com/jonathanslenders/python-prompt-toolkit), dill-0.2.2, Pygments-2.0.2, pytz, shortuuid
* Custom dependences: a customized pyparsing, included in this repo

* Database configuration: none yet.
* How to run tests: no tests yet.
* Deployment instructions: Coming soon!

### Contribution guidelines ###

* Writing tests: please!
* Code review: by all means
* Other guidelines: Please use expanded tabs, four spaces per indent, and 
end blocks with a `#endif` comment at the same level as the statement 
that opened the block.  E.g., 

```
#!python

    if True:
        contents
    #endif

```

Yes, this is Python, but I find closing blocks with a blank line insufficiently clear.

### Who do I talk to? ###

* Repo owner or admin: cxw42 (cxwembedded@gmail.com)

### License ###
Except for external packages or files carrying specific license statements, all code is Copyright (c) 2013-2015 Chris White (Bitbucket user cxw42).  All rights reserved.  Licensed CC-BY-SA 3.0.

### Random ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

