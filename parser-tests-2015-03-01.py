#!/bin/env python
# This is a copy of frplparse from 2015-03-01, with the old tests in it.
# ** This code is not used by fispy.py. **

# Open language design questions and issues:
#   - Figure out how to distinguish int and float div.
#   - Decide whether assignments should be expressions or statements.
#       - If so, decide whether to support the comma operator.
#   - Settle the precedence of bitwise ops w.r.t. arith ops.
#   - Decide whether array constructors have to be enclosed in [] and, if so,
#       what the most elegant way to get pyparsing to do that is.
#       Maybe use two separate infixNotation calls for different BNF parts?


import sys
import pdb
import re
from pyp import *
from inspect import currentframe, getframeinfo  
    #http://stackoverflow.com/questions/3056048
#from pprint import pprint
#    # http://bytes.com/topic/python/answers/746803-help-pyparsing

from collections import Iterable

# frpl-specific modules
import frpltypes as fty
import frplast as ast
import frplstdlib
import frplutil     
    # keeping in the util namespace just to avoid pollution-
    # no deep reason

from frplast import *           # abstract syntax tree

ParserElement.enablePackrat()   
    # DO NOT FORGET TO DO THIS! or else your parse will be _very_ slow.

# Helper used below
def _CKOneOf(x):
    return oneOf(x, True, asKeyword=True)   # caseless keywords

#=========================================================================
# Language keywords.  Define them here so that we can exclude them from
# the space of valid identifiers below.

# First, special method names for operators that can be overloaded.
# These cannot be identifiers but can participate as operands in expressions.
# For example, you can express a+2 as a[plus,2], and "plus" is an operand.
#
# Bitwise operators are not overloadable so that they will always
# be just the bits, ma'am.
#
# pl, mi, ti, di, mo are 100% synonymous with plus, minus, times, div,
# mod.  A defined operator plus will be called for pl and plus.
_OverloadableOperators='''
and but di div eq ge get getbits gt le lt mi minus mo mod ne not or
pl plus put putbits ti times tothe
'''

# Now, keywords.  Not all of these are defined; some are reserved for
# future use.
FInterpreterExitCommands = ['bye','exit','end','leave','quit', 'goodbye']
        # these are reserved for use by the interpreter.
        # lots of choices for exit command in hopes of avoiding
        # the frustration of using a standard ftp client :)

_FKeyword = _CKOneOf(_OverloadableOperators + ' ' + """
    all array as begin bit bitand bitnot bitor bits bitxor block by
    catalog compl def downto else elseif false for fr from function
    gets if import in into is job kind lamb lambda list loop make
    mixin mod names next nm none nop of
    package proc procedure shl shr ta task test to true
    unless unwrap uses using without wrap xor
    """+
    ' '.join(FInterpreterExitCommands)
)
    # array and list are as you expect.
    # catalog is a hash map (dict, map<>)
    # fr, from, using, uses, and without are for 
    # future expansions of the import system.  Likewise mixin.
    # lamb and lambda are for anonymous functions.
    # function, proc, and procedure are for subroutines.
    
#=========================================================================
# Identifier/declaration parsers.  Way up here so test() can use it.

_ParseIdentifier = lambda s,l,t: ast.FAIdent(t[0].lower(), s,l)
    # .lower() so that identifiers will be case-insensitive.

def _ParseDeclaration(s,l,t):
    """ Parse a declaration.  t[0] is the ast.FAIdent of the name.
        t[0] is either an FAIdent or an fty.FTBase of the type. 
        This function is also called directly by other parsing functions,
        e.g., _ParseForLoopClause.  
        Requres _Env.G.
    """
    assert _Env.G is not None
    #print('parsing declaration (env='+repr(_Env.G)+')')
    atype = None
    tyname=str(t[1])

    # Find out what type we want
    if isinstance(t[1],fty.FTBase):     
        atype = t[1]

    elif isinstance(t[1], FAIdent):     # Look up a type in the environment
        tyname=t[1].pvName
        env = _Env.G.find(tyname)

        if env is not None:
            ent=env[tyname]
            if ent.decltype is not None:
                atype=ent.decltype
    #endif

    if atype is None:
        raise ParseFatalException(s, l, 
            "I'm afraid I don't know what kind of thing '%s' is."%tyname)
    #endif

    # Add to the symbol table.
    aname = t[0].pvName

    if _Env.G.find(aname) is None:
        _Env.G[aname] = ast.Entry(atype, s=s, l=l)
            # Save the location for dbg  ^^^^^^^^ msgs on redecls.

    else:   # attempt to redefine
        #pdb.set_trace()
        prior_decl = _Env.G.find(aname)[aname]
        orig_s = prior_decl.source_string
        orig_l = prior_decl.source_loc
        raise ParseFatalException(s, l, 
            "I'm afraid I have already heard of '%s' %s line %d, col %d."%(
            aname, "and don't want to risk confusion.  It was on",
            lineno(orig_l,orig_s),col(orig_l,orig_s)))
    #endif definition new else

    return ast.FADecl(atype, t[0], s,l)   # type, then name
# end _ParseDeclaration

#=========================================================================
# Test harness

# Test suppressor - set DOTEST.DOIT=True to run tests.  Default is False.
class _DoTestClass(object):
    def __init__(self):
        self.DOIT = False       # If True, test() runs tests.
        self.DEBUG = False      # If True, the parser prints debug info
        self.SILENT = False     # overrides test() arg printparse
DOTEST = _DoTestClass()
#DOTEST.DEBUG=True

def BP():
    """Absolute breakpoint triggered on a global switch"""
    if DOTEST.DEBUG:
        pdb.set_trace()

def test(elem, teststr, expected, printparse=False, vars=None):
    """Main test function.
        PyParsing _elem_ either does or does not match _teststr_.  If that
        comparison is opposite from bool _expected_, print a message.
        Run all tests starting from an empty environment."""
    if (not DOTEST.DEBUG) and (not DOTEST.DOIT):
        return

    # Load the environment
    SetGlobalEnv(ast.Env())
    if vars is not None:
        for decl in vars:   #elem = (name:str,ty:str or fty.FTBase)
            (name,ty)=decl
            _ParseDeclaration('',0,[_ParseIdentifier('',0,[name]),
                                    _ParseIdentifier('',0,[ty])])

    lineno = getframeinfo(currentframe().f_back).lineno
    theline=str(lineno)+':'
    # TODO replace this with a parseString call and save the exception if
    # necessary.  We can only run the string once.
    matched = (elem==teststr)
    
    if matched!=expected:
        # print caller's line number and a debug message
        print(theline)
        sys.stdout.flush()
        print( (elem.name if hasattr(elem,"name") else "unnamed element"),
                "failed on --" + teststr + "--: should have", 
                ("matched" if expected else "missed"), "but didn't.")

        if not matched: # if we wanted it to but it didn't, 
                        # show the exception.
            SetGlobalEnv(ast.Env()) # HACK - TODO remove when the parseString
            try:                    # call is moved above
                elem.parseString(teststr, parseAll=True)   
                    # this should always fail
            except ParseBaseException as err:
                print('  -> exception was %s.'%err)

    if printparse and not DOTEST.SILENT:  # whether or not it matched.
        if matched:
            print('%s -- %s -- parsed to == %s =='%(theline, teststr, 
                            str(elem.parseString(teststr))))
        else:
            print('%s -- %s -- parse failed'%(theline, teststr))
        sys.stdout.flush()

    SetGlobalEnv(None)  # for safety
    # end test

# Environments ##############################################################

class _EnvHolder(object):
    def __init__(self):
        self.G = None    # global namespace
        self.A = None    # anonymous-message namespace
 
#end class _EnvHolder 
_Env=_EnvHolder()  
    # use fields since otherwise the None values will be captured
    # in, e.g., _ParseDeclaration(), which will then never see later changes

def pushG(s,l,t):
    """Helper for nesting scopes in parse actions"""
    _Env.G = ast.Env(outer=_Env.G)
    return _Env.G

def popG(s,l,t):
    """Helper for un-nesting scopes in parse actions"""
    oldenvg = _Env.G
    if _Env.G is not None:
        _Env.G = _Env.G.outer
    return oldenvg

def SetGlobalEnv(theenv):
    """Public setter for the global environment.
        Overrides _outer_ so that the predefined frpl language types
        are always outermost. """
    if theenv is None:
        _Env.G = fty.Lang
    else:
        _Env.G = theenv
        _Env.G.outer = fty.Lang
#end SetGlobalEnv

def SetAnonMsgEnv(theenv):
    """Public setter for the anon-msg environment"""
    _Env.A = theenv

def GetGlobalEnv():
    """Public getter for the global environment"""
    return _Env.G

def GetAnonMsgEnv():
    """Public getter for the anon-msg environment"""
    return _Env.A
# Helper routines ###########################################################

# handy constants and syntactic sugar
_FB = FollowedBy

_CK = CaselessKeyword
def _SCK(x):
    return Suppress(CaselessKeyword(x))

_ONEOF_CASELESS = True
_ONEOF_KEYWORD = True
def _CaselessOneOf(x):
    return oneOf(x, _ONEOF_CASELESS)

# Helper for _FExpr
def _unwrapt0(fn):
    """Decorator for convenience in processing parse results in FExpr.
    Passes t[0] as the tokens argument to fn.  This undoes the effect
    of the Group() added by infixNotation."""
    return lambda s,l,t: fn(s,l,t[0])

# Typechecking helper
def _RealTy(astitem:ast.FANode, start_G = True)->fty.FTBase:
    """Returns the type for _astitem_, looking up identifiers if necessary.
      If _start_G_, look in _Env.G first.  Otherwise, look in _Env.A first."""
    retval = astitem.ty
    # The type is only different if it's an identifier.
    if astitem.ty is fty.FTIdentifier:
        startenv = _Env.G if start_G else _Env.A
        if startenv is not None:
            theenv = startenv.find(astitem.pvName)
            if theenv is not None:
                retval = theenv[astitem.pvName].ty
                    # if no environment or the name is not found in the
                    # environment, the return value will be unchanged
                    # as fty.FTIdentifier.
    return retval
# end function _RealTy

def _IsUndefined(fvID:ast.FAIdent, start_G = True)->bool:
    """ Check whether an identifier is undefined. """
    assert fvID.ty == fty.FTIdentifier  # sanity check
    rty = _RealTy(fvID, start_G)
    return rty is not fty.FTIdentifier
        # If we got back FTIdentifier, it was undefined.
# end _IsUndefined

def _Promote(astitem, totype):
    """Promote types"""
    # At the moment this is all we handle.
    if astitem.ty is fty.FTInteger and totype is fty.FTReal:
        return ast.FATypecast(totype, astitem, lambda i: float(i))
    else:
        return astitem
# end function _Promote

def _LookupInG(name:str, s,l, ctx:str=''):
    """Get the ast.Entry for _name_ """
    #pdb.set_trace()
    destenv = _Env.G.find(name)
    if destenv is None:
        raise ParseFatalException(s, l, 
            "I haven't seen what variable %s names%s"%(name,
            (' (needed for parsing a '+ctx) if ctx != '' else ''))
    return destenv[name]
#end _LookupInG

# frpl language definition ##################################################
DOTEST.SILENT = True
DOTEST.DOIT=True

ParserElement.setDefaultWhitespaceChars(" \t")  # don't ignore end of line
_FEOL = LineEnd().setName("EOL")  # end of statement
_SEOL = Suppress(_FEOL).setName("sEOL")     # for convenience
test(_FEOL, "foo", False)
test(_FEOL, "\n", True)
test(_FEOL, "\r", False)
test(_FEOL, "\r\n", False)

#_FComment = Suppress(";" + restOfLine + _FEOL)
_FComment = Suppress(";" + restOfLine)
    # Try this as the comment for now.
    # No more than one statement per line, for simplicity.
    # Don't include the _FEOL.
test(_FComment, "foo", False)
test(_FComment, "; blah", True)
test(_FComment, ";- blah\n", False)
# For now, don't worry about line continuation ("...")
#   - TODO decide whether line continuation should be provided.

# Things that can appear in expressions #####################################
# These are always packaged as tuples of (type, value).

# Constants

_FBasedNumber = Regex(r"(?P<val>\d[0-9a-zA-Z]*)(?P<base>[BHObho]|[Nn](?P<basenum>\d+))").setName('based-number literal')
    # number with a base (base-10 handled below): <digits><base specifier>
    # <base specifier>: b for bin | h for hex | o for octal |
    #                   n<digits> for base <digits>, up to base 36.
    # handle unary - as an operator

def _ParseBasedNumber(s,l,t):
    val=t['val'].lower()
    base=t['base'].lower()
    basenum=-1     # error flag
    if base=='b':
        basenum=2
    elif base=='h':
        basenum=16
    elif base=='o':
        basenum=8
    else:
        basenum=int(t['basenum'])
    # parse the based number to an integer.
    # No based floating point by design.
    try:
        newval = int(val, basenum)
        return ast.FAConst(fty.FTInteger, newval, s, l)
            #ParseResults([t[0],val,basenum])
    except ValueError as e:
        raise ParseFatalException(s,l,
                             'Could not convert "'+val+'" to a base-'+
                             str(basenum)+' integer',_FBasedNumber) from e
# end _ParseBasedNumber

_FBasedNumber.setParseAction(_ParseBasedNumber)

test(_FBasedNumber, "42", False)
test(_FBasedNumber, "42h", True)#,True)
test(_FBasedNumber, "42o", True)#,True)
test(_FBasedNumber, "011b", True)#,True)
test(_FBasedNumber, "42asdknasN36", True)#,True)
test(_FBasedNumber, "hello", False)
test(_FBasedNumber, "42-38", False)
test(_FBasedNumber, "hello42", False)
test(_FBasedNumber, "42yo", False)
    # because it ends in "o", it matches syntactically.
    # The parse action checks for whether the digits are in range.

#-------------------
    
_FNumber = Regex(r"\d+(\.\d*)?([eE][+-]?\d+)?").setName('numeric literal')
    # thanks to Paul McGuire; edited from examples/fourFn.py
    # Again, unary - is an operator, so is not in this Regex.
    # A number always has to start with [0-9].
    # All floats are base-10 by design.  Base-2 floats are uncommon
    # and beyond the scope of this language.

def _ParseNumber(s,l,t):
    #customized from http://stackoverflow.com/a/379966
    instr=t[0]
    try:
        return ast.FAConst(fty.FTInteger, int(instr), s,l)
    except ValueError:
        try:
            return ast.FAConst(fty.FTReal,float(instr), s,l)
        except BaseException as e:
            raise ParseFatalException(s,l,"Couldn't convert number")
#end _ParseNumber
_FNumber.setParseAction(_ParseNumber)

test(_FNumber, "42", True)#,True)
test(_FNumber, "42.", True)#,True)
test(_FNumber, "42.0", True)#,True)
test(_FNumber, "42.99e-23", True)#,True)
test(_FNumber, "0.1e101", True)#,True)
test(_FNumber, ".1", False)      # start with [0-9]

# Superseded
# A based or real number.  This is because either _FNumber
# or _FBasedNumber could swallow digits the other needed.
# This regexp will allow some things that are not actually
# semantically-valid numbers, so it needs further checking.
#FComboNumber = Regex(
#    r"\d[0-9a-zA-Z]*(\.[0-9A-Za-z]*)?([eE][+-]?\d+)?([BHObho]|[Nn]\d+)"
#).setName("ComboNumber")

#-------------------

_FString = (QuotedString("\"", "\\") | QuotedString("'", "\\")).setName('string literal')
    # strings can be single or double-quoted.
_FString.setParseAction(lambda s,l,t: ast.FAConst(fty.FTString,t[0], s,l))
    # t[0] is the string.  Should only ever have one.
test(_FString, "\'This is a test\'", True)
test(_FString, "\"This is a test\"", True)
test(_FString, "\"This is a test\'", False)
test(_FString, "\'This is a test\"", False)
test(_FString, "\'This \\\" is a \\\" test\'", True)
test(_FString, "\"This \\\" is a \\\" test\"", True)
test(_FString, r'"This \" is a \" test"', True)#,True)
test(_FString, '"Example string"',True)#,True)
test(_FString, '"Example\nstring"',False)

#-------------------

# Predefined values and other identifier-like words that have
# special meaning, including overloadable operators.
_FBoolValue = _CKOneOf("true false").setName('bool literal')
_FBoolValue.setParseAction(
    lambda s,l,t: ast.FAConst(fty.FTBoolean, t[0].lower()=="true", s,l))

test(_FBoolValue,'yes',False)
test(_FBoolValue,'true',True)#,True)
test(_FBoolValue,'false',True)#,True)
test(_FBoolValue,'trUe',True)#,True)
test(_FBoolValue,'fAlsE',True)#,True)

_FNoneValue = _CK('none').setParseAction(
    lambda s,l,t: ast.FAConst(fty.FTNone, None, s,l))
_FPredefinedValue = (
    _FBoolValue|_FNoneValue
)

#-------------------
# Special identifiers
_FSpecialIdentifier = _CKOneOf(_OverloadableOperators)
_FSpecialIdentifier.setParseAction(
    lambda s,l,t: ast.FAIdent(t[0].lower(), s,l))
DOTEST.DOIT = True
test(_FSpecialIdentifier,'foo',False)
test(_FSpecialIdentifier,'but',True)
#DOTEST.DOIT = False

#-------------------

# Identifiers
# Identifiers can include dots for package and class access.
# frpl does not distinguish between C++'s . and :: .
# Class.Member is like :: and instance.Member is like .

# Identifier parts: individual elements
# First, make sure we don't have a bare keyword.  Then, grab the word
# that is there.
# Specifically, identifiers can't be keywords.  Since there isn't a 
# oneOf() function for Keywords rather than Literals, manually check 
# to see if there is more after the keyword part.  Combine() means 
# no intervening whitespace.

_FIdentifierPart = (
        (~Combine(_FKeyword+Regex("$|[^A-Za-z0-9_]"))).setName('NotAnIdent')  + 
                    # This is what we don't want: just a keyword either
                    # at eol or not followed immediately by more in-ident 
                    # valid chars.
                    # As a result, package names and the like also can't
                    # be keywords.  E.g., 'foo.xor' is invalid.
                    # This is because \. isn't in the char class in the regex.
        Word(alphas+"_", alphanums+"_").setName('AnIdent')
                    # This is what we do want.
    )

# Complete identifers.  Prohibit comments and whitespace so '.' doesn't
# look like an operator.
_FIdentifier=delimitedList(_FIdentifierPart, '.', True).setName('Ident')
_FIdentifier.setParseAction(_ParseIdentifier)
    # TODO have _FIdentifier expressly check for keywords in the 
    # parse action, then throw a helpful error message if one was found.  

    
test(_FIdentifier, "foo", True)
test(_FIdentifier, "42foo", False)
test(_FIdentifier, "_42foo", True)
test(_FIdentifier, "foo_bar_baz", True)
test(_FIdentifier, "xor", False)     # identifiers can't be keywords
test(_FIdentifier, "xorme", True)    # but can start with or contain them.
test(_FIdentifier, "nop", False)     
test(_FIdentifier, "mybitand", True)
test(_FIdentifier, "isthistrueornot", True)
test(_FIdentifier, "TOTHE", False)
test(_FIdentifier, "true42", True)
test(_FIdentifier, "false4", True)
test(_FIdentifier, "42true", False)
test(_FIdentifier, '.wow',False)
test(_FIdentifier, 'foo.wow',True)
test(_FIdentifier, '.wow',False)
test(_FIdentifier, '.wow.smash',False)
test(_FIdentifier, 'foo.xor',False)
test(_FIdentifier, 'foo.xorme',True)
test(_FIdentifier, 'yowza42.wow.smash',True)#,True)
test(_FIdentifier, 'yowza42.names.smash',False)
DOTEST.DOIT=True
test(_FKeyword, 'dice',False,True)
test(_FIdentifierPart, 'dice',True,True)
test(_FIdentifier, 'dice',True,True)
#DOTEST.DOIT=False

# TODO add record and array elements

# Options

_FOption = Regex(r'/(?P<nm>[a-zA-Z]+)')
_FOption.setParseAction(lambda s,l,t: ast.FAConst(fty.FTOption,t['nm'], s,l))
test(_FOption, '/red',True,True)
test(_FOption, '/a',True)
test(_FOption, '/aaskdhaskdjhaksdhaksjdhaksdjh',True)
test(_FOption, '/aiu23',False)
test(_FOption, '/aiu.whatever',False)

#-------------------

#Sum it all up: Operands.
# At present, these are only used in rvalues, so an Identifier is always
# a ReadVar.  This is because the assignment (remembering) operator '=' is
# not an operator in an expression.  It is instead part of the 
# assignment statement.
def _ParseOperandIdentifier(s,l,t):
    # pdb.set_trace()
    # Get the declared type of the identifier
    ty = _RealTy(ast.FAIdent(t[0].lower(), s,l))
    # Build the node
    return ast.FAReadVar(ty, t[0].lower(), s,l)
        # t[0] is still a string since this replaced the old parser function.

_FIdentInOperand = _FIdentifier.copy().setName('IDInOp')

_FOperand = (
            _FString | 
            _FBasedNumber |      # based number has to come before number
            _FNumber |           # since otherwise Number swallows the
                                # leading [0-9]+ and succeeds.
            _FPredefinedValue |  # Before _FIdentifier so it grabs reserved
                                # words before _FIdentifier sees them.
            _FSpecialIdentifier| # also before _FIdentifier
            _FOption |
            _FIdentInOperand.setParseAction(_ParseOperandIdentifier)
           ).setName("_FOperand")   
                                    
#DOTEST.DOIT=True
test(_FOperand, "hello", True, True)
test(_FOperand, "42", True, True)
#DOTEST.DOIT=False
test(_FOperand, "42.0", True)
test(_FOperand, ".1", False)     # numbers always start with [0-9]
test(_FOperand, "0.1", True)
test(_FOperand, "-1", False)
test(_FOperand, "(x)", False)
test(_FOperand, "\"str\'", False)
test(_FOperand, "\"str\"", True)
test(_FOperand, "True", True)
test(_FOperand, "true", True)
test(_FOperand, "false", True)
test(_FOperand, "false4", True) # false4 is a valid identifier
test(_FOperand, "10h", True)
test(_FOperand, "0ffh", True)
test(_FOperand,'/red',True)

# Expressions ###########################################################

# Arithmetic operators
_FUnaryArithOp = Literal('-') | _CK('neg')
                    # No unary + ; we don't need it.
                    
_FExpOp = _CK("tothe")   # save ^ for xor; don't use ** since
                                    # accidentally doubling a character should
                                    # not succeed silently.
_FMulOp = oneOf('* /') | _CKOneOf("times ti div di mod mo")
    # allow both forms since the text forms can be used without the
    # shift key.  
_FAddOp = oneOf('+ -') | _CKOneOf("plus minus pl mi")

# Array operators
_FArrayBinaryOp = _CKOneOf("to downto")
_FArrayTernaryOps = (_CK("by"), _CK("to"))
    # Array constructors.  1 to 5 is [1,2,3,4,5].  1 by 2 to 5 is [1,3,5]

_FElementReassignmentOp = _CK('gets')
    # arr = arr but 5 gets 3   ; arr[5]=3

# Bitwise operators.  These are advanced, so they all use the shift key.
# However, I'll provide non-shift versions as well, per the requirement.
# Examples of bitwise operators from which the precedence is drawn (C style):
#   set bit in mask: x | (1<<k)
#   clear bit in mask: x & (~(1<<k))
#   test bit: (x & (1<<k)) == (1<<k)
#   test bit of set: (x & mask) != 0
# The precedence is similar to Python.
# What about combinations of arith and bitwise ops?  I don't recall seeing
# many in code; here's what I can think of at the moment:
#   Extract multibit field: if( ((x & mask) >> ofs) > 3) { print("spl"); }
#       This requires override if shifts are higher-precedence than
#       masks, but I don't think that's too bad for all I've seen.
# Ooh - what about an operator for this?
#   x field mask = an lvalue or rvalue of just the bits in _mask_, which
#       must be contiguous.  Then the above would be
#           if(x field mask > 3) { ... }
#   or perhaps
#           if(mask of x > 3) { ... }  Yeah!  I like that.
#   if (1<<5) of x > 3  --- so "of" binds weaker than shl/shr
#   if mask of x & 101b == 101b  --- so "of" binds tighter than and/or.
#   Also allow bitof: 5 bitof x  <=>  1<<5 of x
# OK, so the original question: arith/bit combos?  For now I will just leave
# all bit below all arith, and we'll see what happens.

# Types:
#   - Numeric has int and float.
#   - Bitnumber (name TODO) has specific number of bits and signedness.
#   - Bitmask is used with "of" to pull out specific bits.
#   - Field is a reference to some bits in a bitnumber.  It can be
#       an lvalue or an rvalue.

# Conversion ops
_FUnaryBitmaskOp = _CK("bit")
    # "bit 2" makes (Bitmask)(1<<2).
    # Array constructors have higher precedence than bit so you can say
    # bit 3..5 of foo and get what you expect.

_FUnaryBitnumberOp = _CK("bits")
    # with Numeric n, "bits n" yields Bitnumber(n).

# Real bit ops - only on bitnumbers
_FUnaryBitOp = Literal('~') | _CK("compl")
_FShiftBitOp = oneOf('<< >>') | _CKOneOf("shl shr")
    # << could be a typo for <, and likewise >> for >.
    # It's not a problem because int doesn't implicitly convert to bool,
    # so if x<<5 will fail with "if expected a bool but got a number".
_FOfBitOp = _CK("of")
    # given Bitmask m and Bitnumber n, "m of n" makes Field(n, m)
    # "all of n" makes Field(n), which includes all bits.

_FCombineBitOp = oneOf('& | ^') | _CKOneOf('bitand bitor bitxor')

# Relational operators
_FRelOp = oneOf("< > <= >= <>") | _CKOneOf('lt gt eq le ge ne')
    # not =, which is the assignment operator.
    # "==" is STRICTLY FORBIDDEN!  In fact, no operator may be defined
    # as a doubled version of another operator with a different meaning,
    # as noted above with reference to "tothe".

# Logical operators
_FUnaryLogicalOp = _CK("not")
_FLogicalOp = _CKOneOf("and or xor")    # save &, |, ^ for bitwise

# Ternary operator a la Python - cond?a:b is expressed as a if cond else b
_FTernaryOps = (_CK("if"), _CK("else"))


# - Parse actions ---------------------------------------------------------

# TODO - write parse actions for the operators in _FExpr
@_unwrapt0
def _ParseApply(s,l,t):
    """t[0] is the function and t[1] the arglist.  If there are multiple
        applies, build a node tree."""
    #pdb.set_trace()

    # TODO Build an apply chain for however many applies there are.
    if len(t)>2:
        raise ParseFatalException(s,l, 'At present, chained applies are'+
                                       ' not implemented.  Sorry! (%s)'%str(t))

    # Process target
    retval = t[0] # an FAReadVar, probably

    for params in t[1:]:  # foreach apply
        # Process args
        if isinstance(params, ast.FACallAnon):
            # It was a parameterized arg list with commas.  Grab the args.
            #pdb.set_trace()
            params = tuple(params.whole_msg)
        if not isinstance(params,tuple): 
            params=(params,)
            # single-parameter calls don't get wrapped by _ParseComma,
            # so add the tuple here for regularity.

        #TODO once function declarations are added, look up the function 
        #declaration and get the type of the return value.

        # Process the first arg specially.
        # It can be an unquoted string - like a method name.
        firstparam = params[0]
        if (isinstance(firstparam, ast.FAReadVar) or
            isinstance(firstparam, ast.FAIdent)):
                # FAIdents are special names; FAReadVars are normal.
            declared_ty = _RealTy(firstparam)
            if declared_ty is fty.FTIdentifier:
                # t[0] is an undeclared variable.  Change
                # it to a string literal.
                params = tuple(
                    [ast.FAConst(fty.FTString, firstparam.pvName, s,l)] + 
                    list(params[1:]))

        retval = ast.FAApply(retval, params, s,l)
    # end foreach apply

    return retval
# end function _ParseApply

@_unwrapt0
def _ParseComma(s,l,t):
    """Remove the commas so what is left is a list of operands."""
    # Assume that it's an anonymous call.  If it is actually an apply, 
    # _ParseApply will transmogrify it to an FAApply.
    #pdb.set_trace()
    return ast.FACallAnon(_Env.G, tuple(t[::2]), s,l)   
        # ::2 => every other element is an operand.
        #   The slice returns a list, not a ParseResults instance.

def _ParseAssocBuilder(astclass):
    """Returns a parser function for associative operators that
        returns an instance of _astclass_.  _astclass_ is assumed to
        take the tokens in source order and process them appropriately
        depending on the associativity of the operator."""

    @_unwrapt0
    def innerParseAction(s,l,t):
        """The parse action being build by _ParseLAssocBuilder"""
        #pdb.set_trace()
        # Typecheck.  For now, all elements have to have the same type.
        ty = _RealTy(t[0])

        if ty is fty.FTIdentifier:
            raise ParseFatalException(s,l,
                ("I don't know what kind of value %s remembers.  "
                "Did you declare it with 'names'?")%t[0])

        for idx in range(2,len(t),2):
            e = t[idx]
            ok = False
            e = _Promote(e,ty)
                # Make it match the desired type if possible
            if _RealTy(e) is not ty:
                #BP()
                #pdb.set_trace()
                errmsg = ( 'While looking for %s, I found that item %d (%s) '
                            'was a %s')%(
                        str(ty), idx/2+1, str(e), str(e.ty))
                #print('  -> '+errmsg,file=sys.stderr)
                    # because otherwise the error message is just an
                    # 'expected end of string' or some such
                raise ParseFatalException(s,l, errmsg)
        return astclass(ty, tuple(t), s,l)
            # Freeze the operands in a tuple - don't carry around a ParseResults
            # object once we're out of the parser.
    # end inner function innerParseAction

    return innerParseAction
# end function _ParseLAssocBuilder

@_unwrapt0
def _ParseBut(s,l,t):
    """Parse element-reassignment ("but") expressions.
    E.g., a but 5 gets 3, 1 gets 2"""
            # TODO to think about:
            #;[say,[coins but trialindx gets coin]]
            #; have to wrap the "but" in [] since , has higher precedence.
            #; TODO fix this.  Maybe just use "but" chains - 
            #; but 1 gets 2 but 3 gets 4 ...

    #pdb.set_trace()
    # TODO FIXME - handle a but 1 gets 2 but 3 gets 4 but 5 gets 6
    if len(t)>3:
        raise ParseFatalException(s, l, 'Cannot chain "but" (saw'+str(t)+')')

    dest = t[0]
    # TODO destsym = _LookupInG(dest.pvName, s,l)
        # throws if destsym doesn't exist
    vals = t[2]
        # TODO 

    if isinstance(vals, ast.FACallAnon):    # unwrap comma group
        vals = (vals.fname,)+vals.params
    elif isinstance(vals, ast.FAReassignment):
        # A single reassignment - wrap it in a tuple for uniform handling below
        vals=(vals,)
    elif not isinstance(vals, Iterable):
        vals=(vals,)    # so the loop below can run and fail.  It's not valid.

    # Check types of arguments
    for validx, val in enumerate(vals):
        if not isinstance(val, ast.FAReassignment):
            raise ParseFatalException(s, l, 
                'Cannot use %s (index %d) in a "but" expression'%(
                    str(val), validx))
        # TODO check types of val.dest and val.newval against the array type
    #next validx

    return ast.FABut(dest, vals, s, l)
    
#end function _ParseBut

_ArrBinOp = _ParseAssocBuilder(ast.FABinaryRange)
#def _ArrBinOp(s,l,t):
#    #pdb.set_trace()
#    return _ABO(s,l,t)

@_unwrapt0
def _ParseReassignment(s,l,t):
    #pdb.set_trace()

    if len(t)!=3:
        raise ParseFatalException(s, l, 
            '"gets" only takes two operands (saw %s)'%str(t))

    # Process destination index
    idx = t[0]
    #pdb.set_trace() # TODO what type is idx?
    if isinstance(idx, ast.FAConst):
        idxty = idx.ty
    else:
        destsym = _LookupInG(idx.pvName, s,l, 'reassignment')
            # throws if idx doesn't exist
        idxty=destsym.ty
    #endif

    if idxty is not fty.FTInteger:  # Type-check index
        raise ParseFatalException(s,l,'Attempt to index an array '+
            'with non-integer variable %s in a reassignment'%idx.name)

    # Process new value
    newval = t[2]
    # TODO type check newval

    return ast.FAReassignment(idx, newval, s, l)
        # NOTE: not ParseAssocBuilder since 
        #   - can't be chained (1 gets 2 gets 3 is not meaningful)
        #   - operands are different types - `number' gets `real' is fine
        #       if it's a vector.
# end function _ParseReassignment

_UMinusInnerParser = _ParseAssocBuilder(ast.FAArithExp)
@_unwrapt0
def _ParseUMinus(s,l,t):
    """ Parse a unary minus operator.  At present, this is somewhat
        hacked.  It checks the type of the value provided and makes a
        zero of that type, then parses '0-x' (of that type).
    """
    ty = t[-1].ty
    zero = ast.FAConst(ty, 0, s, l)
    return _UMinusInnerParser(s,l,[[zero,'-',t[-1]]])
# end _ParseUMinus()

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# _FExpr

### NOTE!!!
# infixNotation wraps all its results in a Group().  As a result,
# every parse action for an inner expression (i.e., every parse action
# listed in the opList) receives t==a single-element ParseResults
# of the Group.
_FExpr = infixNotation(  baseExpr=_FOperand, 
                        lpar=Suppress('['),
                        rpar=Suppress(']'),
       opList=[# high to low precendence
        #(expr,numterms, rl, parseaction),

        # Message sending
        (FollowedBy('[').setName('send'),2,opAssoc.LEFT, _ParseApply),

        # Arithmetic
        (_FUnaryArithOp, 1, opAssoc.RIGHT, _ParseUMinus),      #Prefix
            # At present, - is the only unary op ^^^^^^
        (_FExpOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FAArithExp)),
            #Left seems to make more sense in phrases like 2 tothe 3 tothe 5,
            # i.e., 2 to the 3, all to the 5.
        (_FMulOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FAArithExp)),
        (_FAddOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FAArithExp)),

# TODO add these operators back in gradually

#        # Array
#            # TODO add parse functions for these
        (_FArrayBinaryOp, 2, opAssoc.LEFT, _ArrBinOp),
#        (_FArrayTernaryOps, 3, opAssoc.RIGHT),

#
#        # Conversion ops - convert numbers to bitnumbers.
#        (_FUnaryBitmaskOp, 1, opAssoc.RIGHT),
#        (_FUnaryBitnumberOp, 1, opAssoc.RIGHT),
#
#        # Bitwise - operate on bitnumbers
#        (_FUnaryBitOp, 1, opAssoc.RIGHT),
#        (_FShiftBitOp, 2, opAssoc.LEFT),
#            # so 1<<2<<4 shifts by 6, i.e., first by two bits and then
#            # by four bits.  Otherwise, 1<<(2<<4) (right-assoc) would shift
#            # left by 32 bits, which seems unexpected.
#            # I wish PyParsing had opAssoc.NONE.
#        (_FOfBitOp, 2, opAssoc.LEFT),
#            # Here mask of val1 of val2 doesn't make any sense right-assoc.
#            # Mask of val1 must produce a mask for val2.
#        (_FCombineBitOp, 2, opAssoc.LEFT),
#
        # Relational
        ## TODO add these in next
        (_FRelOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FARelExp)),
           
        # Logical
        ##(_FUnaryLogicalOp, 1, opAssoc.RIGHT),
        (_FLogicalOp, 2, opAssoc.LEFT, _ParseAssocBuilder(ast.FALogicalExp)),

#        # Ternary
#        (_FTernaryOps, 3, opAssoc.RIGHT)

         # Element assignments ('gets'): 
         # higher precedence than comma so can make comma-separated lists
         # such as    1 gets 2, 3 gets 4
         (_FElementReassignmentOp, 2, opAssoc.LEFT, _ParseReassignment),
            #_ParseAssocBuilder(ast.FAReassignment)),

         #Building messages: comma.  Lowest precedence.
         (',',2,opAssoc.LEFT, _ParseComma),  # Argument lists or anonymous function calls

       ]
) #_FExpr
_FExpr.setName('expr')

#_FExpr.setParseAction(
#    lambda s,l,t: ast.FAExpr(None, frplutil.flatten(t), s,l))
    # Since everything is postfix, flatten it to a single list.
    # We need to put the flattened list in a list of its own so that
    # postfix sublists will be kept whole when combined with infix
    # parse results as input to In2Post.
    # TODO typecheck and give the expression the right type.
    
#DOTEST.DOIT=True
test(_FExpr,'a[but,1 gets 2, 3 gets 4]',True,True)
test(_FExpr,'a but 1',False)
test(_FExpr,'a[but,3 gets 5]',True,True)
#DOTEST.DOIT=False

#DOTEST.DOIT=True
#pdb.set_trace()
test(_FExpr,'1 to 2',True,True)
test(_FExpr,'1 to 3.5',False)
test(_FExpr,"1 to 's'",False)
test(_FExpr,'1 .. 2',False)
test(_FExpr,'1 downto 2',True,True)
    # We don't test whether the condition holds until runtime.
test(_FExpr,'1 downto 3.5',False)
test(_FExpr,"1 downto 's'",False)
test(_FExpr,'2 downto 1',True,True)
#DOTEST.DOIT=False
#DOTEST.DOIT=True
test(_FExpr,"[say,'hello']",True,True)
test(_FExpr,"[ask,number]",True,True)
test(_FExpr,'a[say]',True,True)
test(_FExpr,'a[1]',True,True)
test(_FExpr,'a[ask,1][say,2]',True,True)
#DOTEST.DOIT=False
test(_FExpr,'42',True,True)
test(_FExpr,'a[0]',True,True)
test(_FExpr,'a[0,1]',True,True)
test(_FExpr,'1+2',True,True)
test(_FExpr,'1+2-3+4',True,True)
test(_FExpr,'1<2',True,True)
#test(_FExpr,'1<2<3',True,True)         # TODO turn these back on when
#test(_FExpr,'1 lt 2 eq 3',True,True)   # I update FARelExp to permit chained
test(_FExpr,'true and false',True,True) # relops
test(_FExpr,'true or false or 3',False)
#DOTEST.DOIT=False
# Tests: operands and unaries
test(_FExpr, "42", True)
#test(_FExpr, "-42", True)      # TODO add when I turn UnaryArithOp back on
#test(_FExpr, "--x", True)      # ditto
test(_FExpr, "+-x", False)
    # no more unary +

test(_FExpr, '1+2*3', True)
test(_FExpr, '4*5+6', True)
test(_FExpr, '[7+8]*9', True)
    # This case shows why the _FExpr parse action is [flatten()] rather than
    # just plain flatten() (without wrapping in a list).

test(_FExpr, '1+2+3+4', True)#, True)
test(_FExpr, '5+6*7+8', True)#, True)
test(_FExpr, 'a+b*c*d', True, vars=list(zip(['a','b','c','d'],4*['number'])))#, True)

# Exponentiation - left-associative
test(_FExpr, "2 tothe 3", True)
test(_FExpr, "2 tothe 3 tothe 5", True)#, True)
test(_FExpr, "1 xor 2", True)   # False for FEH2?

# Multiplicative operators
test(_FExpr, "42 div 1", True)
test(_FExpr, "42 tothe 3 * 1", True)
test(_FExpr, "42 tothe 3 * 1 div 4 mod 7 div 3", True)

# Additive operators
test(_FExpr, "42 tothe 3 * 1+3+3-5 div 2 div 4 mod 7 div 3", True)

test(_FExpr, "hello+.1", False,vars=[('hello','real')] )  #because .1 isn't a number
test(_FExpr, "hello+0.1", True,vars=[('hello','real')])
#test(_FExpr, "~42", True)   # TODO add these tests back in when
                            # _FExpr is expanded.
test(_FExpr, "10h", True)    
test(_FExpr, "0ffh", True)
#test(_FExpr, "1&0ffh", True)
#test(_FExpr, "~10h", True)
#test(_FExpr, "~10h+1", True)
#test(_FExpr, "(~10h)+1&0ffh", True) # to do - test this
#test(_FExpr, "~(10h+1)&0ffh", True) # to do - test this
#test(_FExpr, "~10h+1&0ffh", True) # to do - test this
#test(_FExpr, "3*4+-7.09", True)  # TODO put back in when unary minus added
#test(_FExpr, "0c0h of 7fffh", True)
test(_FExpr, "\'stringval\'+45", False) # doesn't typecheck
#test(_FExpr, "x | 1<<5", True)
#test(_FExpr, "x & ~(1<<5)", True)   
#test(_FExpr, "3 bitof x if cond else false", True) #Fails - TODO figure out why

# Messages ###########################################################
#_FExpr already knows how to handle whole messages    

test(_FExpr,"[say,hello,42]",True)
test(_FExpr,"[]",False)  # infixNotation requires something other than
                        # an empty group
test(_FExpr,"yes,no",True)
test(_FExpr,'[42]',True)
test(_FOperand,"[say,hello,42]",False)
test(_FExpr,"dest[say,hello,42]",True,True)
test(_FExpr,"dest[]",False)
test(_FExpr,'dest[42]',True)
test(_FExpr,'42[plus,1]',True)
test(_FExpr,'dest [42 ,    20    ,sdoi]',True)



# Types ###########################################################
# FT* patterns
_FType = _FIdentifier  #TODO add type definitions?

# Statements ###########################################################
# FEOL is a statement terminator, so each statement clause includes the FEOL.
# Also, each statement ignores comments.
# FS* patterns
#DOTEST.DOIT=True

_FStatement = Forward()

_FSNop=Optional(_CK('nop'))+_FEOL    # do-nothing
_FSNop.ignore(_FComment)
_FSNop.ignore(Regex('^[[:space:]]*$').leaveWhitespace())
_FSNop.setParseAction(lambda s,l,t: ast.FANop(s,l))

# - Declarations -

#_FSDeclaration=(_FIdentifier+Suppress(_CK("names")|_CK("nm"))+
#                    mustHave(_FType)+_SEOL)
_FSDeclaration=(_FIdentifier+Suppress(_CK("names")|_CK("nm"))
                    - _FType+_SEOL)
    # "-" begins things that have to match
    # "names" because the var is actually a reference, i.e., name of an obj
_FSDeclaration.ignore(_FComment)

_FSDeclaration.setName('declaration').setParseAction(_ParseDeclaration)
    # _ParseDeclaration is defined up at the top of the file, above the
    # test harness.
#_FSDeclaration.ignore(_FComment)

test(_FSDeclaration, "foo names number",True)
test(_FSDeclaration, "hello names text",True)
test(_FSDeclaration, 'foo names 42',False)
test(_FSDeclaration, 'foo names struct    thingy',False)
test(_FSDeclaration, 'names x',False)
test(_FSDeclaration, 'foo nm boolean',True)
test(_FSDeclaration, 'foo nm boolean ;comment',True)

# - Assignments -

#_FSAssignment=_FExpr+Suppress("=")+_FExpr+_SEOL
    # this works because '=' is not an operator in an _FExpr.
    # We need _FExprs for lvalues because of, e.g., assignment to
    # elements of an array.
#_FSAssignment=_FIdentifier+Suppress("=")+mustHave(_FExpr)+_SEOL
_FSAssignment=_FIdentifier+Suppress("=") - _FExpr+_SEOL
    # TODO change this back to _FExpr as an lvalue.
    # mustHave is since once you have the equals sign, you're committed to
    # seeing an expression next, and no backtracking will succeed.
_FSAssignment.ignore(_FComment)

_FSAssignment.setName("_FSAssignment")
#_FSAssignment.ignore(_FComment)


def _ParseAssignment(s,l,t):
    #lambda s,l,t: ast.FAAssignment(t[1].ty, t[0], t[1], s,l))    
    # 0=>dest; 1=>expr
    #print('parsing assignment (env='+repr(_Env.G)+')')  #debug
    destid=t[0] #FAIdent
    rvalue=t[1] #FAExpr
    if _Env.G is None:      # true during testing
        return ast.FAAssignment(t[1].ty, t[0].pvName, ast.Entry(None), t[1], s,l)

    #runtime: _Env.G is not None
    # Lookup dest in env 
    #pdb.set_trace()
    destsym = _LookupInG(destid.pvName, s,l)   # an ast.Entry

    # TODO 
    # Type-check rvalue against dest
    #DEBUG: type-checking commented out
    #if rvalue.ty != destsym.ty:
    #    raise ParseFatalException(s,l,
    #        "I don't know how to make a %s variable remember a %s"%(
    #            str(destsym.ty), str(rvalue.ty)))

    #pdb.set_trace()
    # We have a good assignment.  Remember it.
    return ast.FAAssignment(destsym.ty, destid.pvName, destsym, rvalue, s,l)
        # TODO figure out whether the type of an assignment statement
        # should always be None or something else.
# end _ParseAssignment

_FSAssignment.setParseAction(_ParseAssignment)

test(_FSAssignment, "x=42",True, vars=[('x','number')])
test(_FSAssignment, "x=42 stuff",False, vars=[('x','number')])
#_FSAssignment.setBreak()
# TODO put these back in when lvalues are FExprs again
#test(_FSAssignment, 'x[5]=42',True)
#test(_FSAssignment, 'x[item,5,10] =  [ask,how,are,you,today,query]',True)
test(_FSAssignment, '  hello=[ask,who,are,you]', True, vars=[('hello','text')])
test(_FSAssignment, '  hello=[ask,who,are,you] ;with comment YEAH!', True, vars=[('hello','text')])
#test(_FSAssignment, '  arr[4,foo]=39', True)

# Compound statements: insides.  The delimeters are part of the statements
# since they are different for different statements.
def _ParseStatementChunk(s,l,t):
    #pdb.set_trace()
    return ast.FACompoundStmt(t[:], s,l)
        # Wrap up all the statements in one AST node.
        # Deep copy to drop the ParseResults.

_FStatementChunk = OneOrMore(_FStatement).setName('StmtChunk')
_FStatementChunk.setParseAction(_ParseStatementChunk)
_FStatementChunk.setDebug(DOTEST.DEBUG)

# - Blocks -
# Blocks are just to define nested scopes.
_FSBlock = (Suppress(_CK('block').setParseAction(pushG)) - _SEOL +
            _FStatementChunk+
            _SCK('ta')+_SEOL)
_FSBlock.setName('block')
def _ParseBlock(s,l,t):
    #pdb.set_trace()
    return ast.FABlock(popG(s,l,t), t[0])

_FSBlock.setParseAction(_ParseBlock)

# - Conditionals -

#_FSConditional = (Suppress(_CK("if"))+mustHave(_FExpr)+ _SEOL+
_FSConditional = (_SCK("if") - _FExpr + _SEOL+
                     _FStatementChunk +     # true
                     Optional(
                        _SCK("else")+_SEOL -
                        _FStatementChunk    # false
                    ) +
                 _SCK("ta")+_SEOL)
    # TODO add elseif, else
_FSConditional.setName('_FSConditional')
_FSConditional.ignore(_FComment)  # for comments on the if or ta lines

def _ParseConditional(s,l, t):
    #pdb.set_trace()
    if len(t)==2:   # if no else block, fill it in with a nop.
        t = t[:] + [ast.FANop(s,l)]    
    try:    # TODO figure out if this way of error-checking makes sense.
            # Should frplparse pre-check everything before creating the
            # ast nodes?
        return ast.FAConditional(t[0], t[1], t[2], s,l)
    except Exception as e:
        raise ParseFatalException(s,l, 'Could not parse if-statement') from e
_FSConditional.setParseAction(_ParseConditional)

# Loops.  Every loop defines a block.
_FSLoopClause=Suppress(_CK('for')) - _FIdentifier+Suppress(_CK('in'))+_FExpr
    # TODO add other options here ('forever', 'with test', 'while', 'until'
    # TODO add range exprs
_FSLoopClause.setName('for-clause')

def _ParseForLoopClause(s,l,t):
    """t[0] is an identifier and t[1] an expr, which must be a range."""
    #pdb.set_trace()
    if not isinstance(t[1], ast.FABinaryRange):
        raise ParseFatalException(s,l,'Need a binary range in a for loop; got '+str(t[1]))

    ident = t[0]
    expr = t[1]

    # Declare identifier if not already declared
    decl = None
    if _RealTy(ident) is fty.FTIdentifier:       # undefined...
        decl = _ParseDeclaration(s,l,[ident,expr.ty])   # ...so define it.

    # Get the Entry

    # Build clause
    destsym = _LookupInG(ident.pvName, s,l)
    init = ast.FAAssignment(destsym.ty, ident.pvName, destsym, expr.first, s,l)

    # Build the increment
    curridx = ast.FAReadVar(destsym.ty, ident.pvName.lower(), s,l)
    newidx = ast.FAArithExp(curridx.ty,[curridx, 'pl', expr.incr], s,l)
        # Add the range's increment, which will be +1 or -1
    incr = ast.FAAssignment(destsym.ty, ident.pvName, destsym, newidx, s,l)

    # Build the test
    pretest = ast.FARelExp(curridx.ty,
        [curridx, expr.relop_continue_when, expr.last], s,l)
        # Grab the right relop from the range, which knows whether it's
        # an up range (test <=) or a down range (test >=)

    # Put the clause together
    clause = ast.FALoopClause(
                init, incr, pretest=pretest, decl=decl #decl may be None
            )   # no posttest in a for loop

    return clause

# end _ParseForLoopClause

_FSLoopClause.setParseAction(_ParseForLoopClause)

#DOTEST.DOIT=True
test(_FSLoopClause,'for i in 1 to 2',True,True)
test(_FSLoopClause,'for quux in 42 to 78',True,True)
#DOTEST.DOIT=False

_FSLoop=(Suppress(_SCK('loop').setParseAction(pushG)) - _FSLoopClause + _SEOL +
            #^ this Suppress is to drop the return value of pushG()
            _FStatementChunk +
            _SCK('next')+_SEOL)
_FSLoop.setName("_FSLoop")
_FSLoop.ignore(_FComment)
# TODO write the parsing function for FSLoop, including binding the loop var
# before parsing the statement chunk.

_FSLoop.setName('loop')
def _ParseLoop(s,l,t):  #t has the clause, then the statement chunk.
    #pdb.set_trace()
    clause=t[0]
    body=t[1]
    return ast.FABlock(popG(s,l,t), ast.FALoop(clause, body, s,l))
        # Wrap the loop in a scope of its own.

_FSLoop.setParseAction(_ParseLoop)
_FSLoop.ignore(_FComment)

# Statement.  TODO figure out if we need ^ instead of |
_FStatement<< ( _FSConditional |
                _FSBlock |
                _FSLoop |
                _FSDeclaration |
                _FSAssignment |
                _FSNop  |   # covers blank lines and comment lines.
#                StringEnd().setName('StringEnd in statement').setParseAction(
#                    lambda s,l,t: ast.FANop(s,l)) |
                #mustHave(whenfb=Optional(_FIdentifier)+'[', 
                #        need=_FExpr+_SEOL).setName("whole-line-expr")
                (FollowedBy(Optional(_FIdentifier)+'[') -
                        _FExpr+_SEOL).setName("whole-line-expr")
                    # handles messages - TODO add an action function
                    # to validate it's an expression allowed in a
                    # statement context.
                    # If it's not an assignment, it should be a message send.
                    # That can have a target, and has an open bracket.
            )
_FStatement.setName('_FStatement')
def _ParseStatement(s,l,t):
    #BP()
    return t[0] # pull the statement out of the ParseAction.
_FStatement.setParseAction(_ParseStatement)

_FStatement.ignore(_FComment)

# Test statements.  This has to come after the _FStatement<< .
#DOTEST.DOIT=True
test(_FSLoop,'''loop for i in 1 to 10
[say,i]
next''',True,True)

DOTEST.DOIT=False       # TODO update tests below here

#DOTEST.DOIT=True
test(_FStatement,'''if true
 x
 y
 z
ta''',True,True)
test(_FStatement,'''if true
 x
 y
else
 z
 w
ta''',True,True)
#DOTEST.DOIT=False
test(_FStatement,'nop',True)
test(_FStatement,'a[5]',True, True)
#DOTEST.DOIT=False
test(_FStatementChunk,'''nop
nop
nop''',True,True)
#DOTEST.DOIT=True
_FStatementChunk.setDebug(DOTEST.DEBUG)
_FStatement.setDebug(DOTEST.DEBUG)
_FExpr.setDebug(DOTEST.DEBUG)
test(_FStatementChunk,'''
s names text
if s eq 'phrank'
  [say,'You are awesome!']
ta
if s eq 'cxw'
  [say,'You are an amazing robot']
ta
[say,"Glad you're here!"]
''',True,True)

#DOTEST.DOIT=False

# Test conditionals after assigning _FStatement,
# because otherwise _FStatement in the body of a
# conditional is unassigned so never matches.
#DOTEST.DOIT=True
test(_CK("if")+_FExpr+_FEOL,"if 1",True)
test(_CK("if")+_FExpr+_FEOL,"if 1\n",True)
test(OneOrMore(_FSAssignment),'x=42',True)
test(OneOrMore(_FStatement),'x=42',True)
test(_FSConditional,'if 1\nx=42\nend',False)
test(_FSConditional,'if true\nx=42\nend',False)
test(_FSConditional,'''if true
  x=42
  hello names text
  hello=[ask,who,are,you]
ta''', True)
  #TODO put this back in when lvalues are FExprs => arr[4,foo]=39

test(_FSConditional, '''if true
    x
ta''',True)
test(_FSConditional, '''if true
    x
else
    y
ta''',True)
test(_FSConditional, '''if true ;comment
    x
ta''',True)
test(_FSConditional, '''if true ;comment
    x   ;comment2
ta''',True)
test(_FSConditional, '''if true ;comment
    x ;comm
ta ;comm''',True)
test(_FSConditional, '''if true ;comment
    x ;comm
ta ;comm
;comm''',True)
test(_FSConditional, '''if true ;comment
    x ;comm
ta ;comm

;comm''',False)     # has an extra statement at the end.

test(_FStatement, '''if true ;comment
    x ;comm
ta ;comm

;comm''',False)

test(OneOrMore(_FStatement), '''if true ;comment
    x ;comm
ta ;comm

;comm''',True,True) #  returns ['if', 'true', '\n', 'x', '\n', 'ta', '\n', '\n']

#DOTEST.DOIT=False

test(_FStatement,'#comment', False)
test(_FComment,';comment', True)
test(_FStatement,';comment', True, True)

test(_FStatement,'x=42 ;comment', True,True)
test(_FStatement,'x=;comment', False)
test(_FStatement,'     ;comment', True,True)
test(_FStatement,"[say, 'Hello, world!'    ;comment", False)
test(_FStatement,"[say, 'Hello, world!'    ];comment", True,True)

test(_FStatement,"",True)            # returns []
test(_FStatement,"\n",True)          # returns ['\n']
test(_FStatement,"       \n",True)   # also returns ['\n']

test(_FStatement,'''if true
  x=42   # save the answer
  hello names text  #all your base

  hello=[ask,who,are,you]
  # are belong to us
  arr[4,foo]=39
  
ta''', False)

#pdb.set_trace()
test(_FStatement,'''
x nm number
if true
  x=42   ; save the answer
  hello names text  ; all your base

  hello=[ask,who,are,you]
  ; are belong to us
  
ta''', True)
  #TODO put this back in when lvalues are FExprs => arr[4,foo]=39

#DOTEST.DOIT=True
#pdb.set_trace()
test(_FStatement, 'x nm number', True, True)
test(_FStatement, 'x=42', True,True)
#DOTEST.DOIT=False

# Programs #############################################################
_FPackageDecl=Group(_CK('package')+_FIdentifier+_SEOL)

test(_FPackageDecl,'package foo',True)
test(_FPackageDecl,'\npackage foo',False)    # leading \n handled in _FProgram

_FImport=Group(_CK('import')+delimitedList(_FIdentifier)+_FEOL)
    # at present, can only import whole packages since 'using' (or whatever
    # the syntax turns out to be) is not defined.

_FImports = OneOrMore(_FImport|_FEOL)
test(_FImports,'import wowza',True)
test(_FStatement,'nop',True)


# TODO: Update _FProgram so that any number of preamble statements can occur
# before the normal statements start.
#_FProgram = Suppress(ZeroOrMore(_FEOL)) + (
_FProgram = (
    #Optional(_FPackageDecl) +
        # DEBUG: Don't comment out the above line.  _FProgram has to be
        # a different ParserElement than the below OneOrMore, and this
        # clause handles that.
    #Optional(_FImports) +
    _FStatementChunk
    #OneOrMore(_FStatement).setName('stmts').setParseAction(_ParseCompoundStatement)
        # Handles trailing nops and blanks
) + StringEnd() # + Suppress(ZeroOrMore(_FEOL)) #_FProgram
    # StringEnd so there aren't trailing chars.

_FProgram.setName('Program')
_FProgram.setDebug(DOTEST.DEBUG)    # DEBUG
_FProgram.ignore(_FComment)
def _ParseActionProgram(s,l,t):
    """Parse action for _FProgram"""
#    pdb.set_trace()
    return t[0] # DEBUG: extract the FACompoundStmt from the ParseResults.
#        # TODO make this more sophisticated.  Also, figure out how
#        # to not return a ParseResults() anyway - see pyp.py:987
_FProgram.setParseAction(_ParseActionProgram)
#    # Unwrap the ParseResults around the FACompoundStmt.
#    # TODO improve this when things other than statements are allowed.

def ParseProgram(s,initialenv=None):
    """Public function to parse a program starting from initialenv,
    or an empty environment if initialenv is omitted or None."""
    if initialenv is None:
        initialenv = ast.Env()
    SetGlobalEnv(initialenv)
    #pdb.set_trace()
    tree = _FProgram.parseString(s)[0]     # propagate all exceptions 
    return ast.Program(tree,initialenv)
# end ParseProgram()

# end ParseProgram
test(_FProgram, "[say,'Hello world!']",True)
test(_FProgram, """
package foo
import wowza
nop

[say,'Hello, World!']
name names text
name=[ask,'What is your name?']
[say,'Hello,',name,'!']
""",True)

test(_FProgram, '''
x nm number
if true
  x=42   ; save the answer
  hello names text  ; all your base

  hello=[ask,who,are,you]
  ; are belong to us
  
ta''', True,True)
  #TODO put this back in when lvalues are FExprs => arr[4,foo]=39

#DOTEST.DOIT=True
#test(_FProgram,open('test.fr','rt').read(),True,True)
#DOTEST.DOIT=False

#DOTEST.DOIT=True
DOTEST.SILENT=False
test(_FProgram,'''
x nm numvector
x=numvector[rez]
x=x[but,1 gets 1,2 gets 2]
x[0]''',True,True)
#DOTEST.DOIT=False

# Helper for the interactive parser - an expression with nothing after it
_FExprOnlyLine = (_FExpr + _SEOL).setName('expr and nothing else on the line')
_FExprOnlyLine.ignore(_FComment)

# Exports
ParseExpr = _FExpr.parseString
ParseExprOnlyLine = _FExprOnlyLine.parseString
#frpl_parseProgram = _FProgram.parseString
CheckProgram = lambda s: _FProgram==s
CheckStatement = lambda s: _FStatement==s

#def main():
#    return 0
#    # end main
#
#if __name__ == "__main__":
#    exit(main())
#
## vi: set ts=4 sts=4 sw=4 expandtab ai:
#
