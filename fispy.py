#fispy.py: Frpl Interpreter (command line), Stack-based, PYthon implementation

# TODO next: try out plac for command-line parsing.  Add /k.
# TODO: Figure out how to get deeply-nested syntax errors out.
# TODO refactor error reporting so errors on eval can get MARKERs 
# just like errors at parse time.

FISPY_VER='version of 2015/11/14 (v0.1.0)'
EXT='.fr'   # frpl source extension
SAVE_NAME='gem.frplsession' # only save/load one session at present

WEIRD_EXC_THLD=2    # exit when we see this many consecutive weird excs.
MARKER='@@'         # marker for showing where the error is

PROF_FNAME = 'fispy-profile-stats.txt'  # where to save profiling results

# === Imports =========================================================
import sys, os.path, traceback
import argparse
import pdb
import frpltypes as fty
import frplast as ast
import frplparse as fp
import frplstdlib
import pyp

import dill   #for save/load

from prompt_toolkit.shortcuts import get_input  # for readline
from prompt_toolkit.history import InMemoryHistory

# === Helpers =========================================================
def ReportError(exc):
    """ Helper for reporting syntax errors """
    print("! I'm afraid I don't understand.  %s"%str(exc))
    print("! This may be near the %s in the below line:"%MARKER)
    print("! " + exc.markInputline(MARKER))
# end ReportError()

# === Setup ===========================================================
# Set up initial environment for interactive interpreter
IEnv = ast.Env()
#print('$$$ IEnv.uu = '+IEnv.uu)

pgm = fp.ParseProgram(frplstdlib.DefaultEnvProgram, 
                        initialenv=IEnv)
    # This sets IEnv.outer = fty.Lang to pull in the base language.
    # No need for guardenv since these are required to always succeed.

pgm.run()      # the retval from ParseProgram remembers _IEnv_.
    # create initial vars

# Set up for save/load.  These are the runtime-provided implementations of
# gem[save] and gem[load].
class gem_save():
    def save(self):
        with open(SAVE_NAME, 'wb') as f:
            #pdb.set_trace()
            dill.dump(IEnv, f)
# end gem_save

class gem_load():
    def load(self):
        with open(SAVE_NAME,'rb') as f:
            #pdb.set_trace()
            IEnv = dill.load(f)
# end gem_load

IEnv.add(fty.PVN_SAVE, fty.Entry(fty.FTNativeFunction, gem_save()))
IEnv.add(fty.PVN_LOAD, fty.Entry(fty.FTNativeFunction, gem_load()))

#print('$$$ Lang is '+str(fty.Lang))     #DEBUG
#print('$$$ IEnv is '+str(IEnv))

# === Batch ===========================================================
def run_program_noninteractive(s):
    """Interpret the frpl program given in _s_.  Only used for
        non-interactive execution, so aborts on error."""

    # Parse --------------------
    #pdb.set_trace()
    pgm = None
    try:
        pgm = fp.ParseProgram(s, IEnv)
    except pyp.ParseFatalException as exc:
        # Syntax error in the frpl program.
        # May also be incomplete-statement.
        print("! I'm afraid I do not understand what you mean.  %s"%str(exc))
        print("! This may be near the %s in the below line:"%MARKER)
        print("! " + exc.markInputline(MARKER))

    except pyp.ParseException as exc:
        # syntax error, or incomplete statement.
        errlineno = exc.lineno    # save for use in errmsg
        errstr = str(exc)
        errmark = exc.markInputline(MARKER)

        # syntax error in something that's gone before.
        print(
            ("! I'm afraid I'm not sure what you mean.  %s\n"
             "! This may be near the %s:")%(errstr,MARKER))
        print("! "+errmark)

    except Exception as exc:
        # Other error
        print("! Apologies; something screwy is happening.  %s"%
                str(exc))
        (exty, exval, tb) = sys.exc_info()
        traceback.print_exception(exty,exval,tb)
    #end try..except

    # Run ----------------------
    #pdb.set_trace()
    if pgm is not None:
        pgm.run()

# end run_program_noninteractive()

# === Interactive =====================================================
def run_session_interactive():
    """Run an interactive frpl session."""
    print('''Welcome to frpl (%s) by cxw!
To exit, type     bye    and hit Enter.
My name is gem.  How can I help you?'''%FISPY_VER)

    hist = InMemoryHistory()

    num_weird_exc = 0   # for aborting on repeated exceptions

    #currEnv = IEnv  # keep track of the last successful environment

    doexit = False
    while not doexit:       # Loop whose body reads a complete statement
        promptstr='ready> '

        thisline=''         # The current line
        lineno=1            # Line number, for incomplete-statement checks

        wholeline=''        # The user's entry, which may be multiline
        lastwholeline=''    # The user's entry before adding thisline

        pgm=None            # the ast.Program we are going to ruin
        havestmt = False    # True if we have something ready to eval

        while not havestmt:     # Loop to assemble a complete statement

            # Input the next line
            try:
                #pdb.set_trace()
                thisline=get_input(promptstr, history=hist)
            except EOFError:    # exit cleanly
                doexit=True
                break #from havestmt
            except KeyboardInterrupt:
                if lineno==1:   # ^C at the top level exits fispy
                    doexit=True
                break #from havestmt
            #end try

            if len(thisline.strip())==0:
                if lineno==1:    # Empty line at the start: begin fresh
                    break   # from havestmt
                else:           # In a multiline: ignore it
                    thisline='' # but leave it in for numbering.
                #endif

            elif (thisline.lower().strip() in fp.FInterpreterExitCommands):
                doexit = True   # time to exit fispy
                break
            #endif exit command

            lastwholeline = wholeline       # in case thisline has an error
            wholeline += thisline + '\n'
                # Add the \n on the end so an incomplete input will
                # error on the line number after the one we just got.
                # This permits determing (apparently reliably)
                # whether input is complete.

            # Check for single-line expressions here first
            AsExprException=None
            if lineno==1:
                # TODO fix this so it handles anonymous messages.
                # At the moment it swallows them as exprs (since they are).
                try:
                    exprtree = fp.ParseExprOnlyLine(thisline)[0]
                    # If we get here, the line is a valid expression
                    # with nothing else after it.  Convert it to
                    # the appropriate print statement.
                    wholeline = 'gem[say,[%s]]\n'%thisline
                    pgm = fp.ParseProgram(wholeline, IEnv)
                    #print('Got program %s\n'% str(pgm))
                    havestmt=True
                    break   # out of 'while not havestmt' - we're done
                except Exception as e:
                    AsExprException=e   # remember it for later
                    pass    # fall through - handle error

            # Parse it as a statement if possible
            try:
                #pdb.set_trace()
                pgm = fp.ParseProgram(wholeline, IEnv)
                havestmt = True # since the parse succeeded, run it
                #print('Got stmt %s\n'%str(pgm))
                num_weird_exc=0

            except pyp.ParseFatalException as exc:
                # Syntax error in the frpl program.
                # May also be incomplete-statement.
                num_weird_exc=0
                if exc.lineno<lineno:   # it's an error in the user's text
                                        # on a previous line
                    ReportError(exc)
                    break   # restart
                elif exc.lineno == lineno:  # it's an error in the
                                            # most recent line
                    ReportError(exc)
                    #pdb.set_trace()
                    if lineno>1:
                        print("Here's what I have so far:\n%s" % 
                                lastwholeline)
                    wholeline = lastwholeline   # discard thisline
                    lineno -= 1
                    # keep going
                # else incomplete statement, so keep going

            except pyp.ParseException as exc:
                # syntax error, or incomplete statement.
                num_weird_exc=0
                errlineno = exc.lineno    # save for use in errmsg
                errstr = str(exc)
                errmark = exc.markInputline(MARKER)

                if errlineno<=lineno:
                    # syntax error in something that's gone before.
                    print(
                        ("! I'm afraid I'm not sure what you mean.  %s\n"
                         "! This may be near the %s:")%(errstr,MARKER))
                    print("! "+errmark)
                    if AsExprException is not None:
                        print(
                            '!\n! --- In case it helps, as an expression,'+
                             ' the problem would be:\n'+
                             '! %s (maybe near %s):\n! %s\n'%(
                                str(AsExprException), 
                                MARKER,
                                AsExprException.markInputline(MARKER))
                            )
                        AsExprException=None
                    break   # restart
                # else incomplete statement, so keep going

            except Exception as exc:
                # Other error
                print("! Apologies; something screwy is happening.  %s"%
                        str(exc))
                (exty, exval, tb) = sys.exc_info()
                traceback.print_exception(exty,exval,tb)
                num_weird_exc = num_weird_exc+1
                if num_weird_exc == WEIRD_EXC_THLD:
                    print('! Saw %d exceptions - aborting'%WEIRD_EXC_THLD)
                    sys.exit()  # abort now
                break   #restart
            #end try..except

            lineno += 1
            promptstr='ready> ' if lineno==1 else '     > ' 
                # set up appropriately for the next iteration
        #loop while not havestmt

        # Run it
        if havestmt:
            #pdb.set_trace()
            try:
                pgm.run()
            except Exception as exc:
                #pdb.set_trace()
                # TODO find out why here[now] throws with:
                #   AttributeError: 'module' object has no attribute 'now'
                print("! I'm afraid there's a problem:\n! %s"%str(exc))
        
    # loop while not doexit
    print('Bye for now!  (Press Enter if you need to close this window)')

# end run_session_interactive()

# === Argument parsing ================================================

def parse_args():
    """ Parse the command-line arguments, if any, and return a list of
        callables to be performed in order. """

    retv = []   # a list of actions to perform

    # Parse the arguments
    parser = argparse.ArgumentParser(
        description='frpl command-line interpreter.')
    parser.add_argument('files', metavar='NAME', nargs='*',
                       type=argparse.FileType(mode='r', encoding='utf-8'),
                       help='a frpl file to execute.')
        # TODO support automatically adding EXT if the file doesn't exist.
        # See https://docs.python.org/3/library/argparse.html#argparse.Action
    parser.add_argument('-p','--profile', dest='profile', action='store_true',
                       help='Enable profiling, saving results in %s'%PROF_FNAME)
    parser.add_argument('-k','--keep', dest='keep', action='store_true',
                       help='Enter the interactive loop once the files on'
                            ' the command line have been run.')

    args = parser.parse_args()

    # Create the action list from the arguments

    if args.profile:
        import cProfile,pstats,io   #from https://docs.python.org/3/library/profile.html#profile.Profile
        pr = cProfile.Profile()

        def startprof():
            pr.enable()
        def endprof():
            pr.disable()
            sortby = 'time'  #'cumulative'
            with open(PROF_FNAME,'w') as fd:
                ps = pstats.Stats(pr, stream=fd).sort_stats(sortby)
                ps.print_stats()
                print('Profile saved to',fd.name)
            #end with
        #endprof()

        retv.append(startprof)
    #endif args.profile
            
    def makeprocessor(fd):
        """ Make a callable that will run the frpl program in open file fd."""
        def inner():
            # TODO run each program in its own environment except the last.
            # Probably need to add pushG- and popG-type actions to retv.
            run_program_noninteractive(fd.read())
        return inner
    # makeprocessor()
        
    for fd in args.files:
        retv.append(makeprocessor(fd))

    if args.keep or len(args.files)==0:
        retv.append(run_session_interactive)

    if args.profile:
        retv.append(endprof)

    return retv
# parse_args()

# === MAIN ============================================================

if __name__=='__main__':
    actions = parse_args()
    for act in actions:
        act()
    #next

    # Old code to add extension
    #for scriptfn in sys.argv[2 if prof else 1:]:
    #    if not os.path.exists(scriptfn):
    #        if os.path.exists(scriptfn+EXT):
    #            scriptfn+=EXT
    #        else:
    #            print("! I can't find the file '%s' you asked me to run.  " 
    #                    "Sorry!"%scriptfn, file=sys.stderr)
    #            continue    # next filename
    #    with open(scriptfn, 'rt') as fd:
    #        run_program_noninteractive(fd.read())
    ##next scriptfn

#end main

#############################################################################
# Most recent two changes:
#   2015/11/20  cxw     Updated per prompt_toolkit master of even date.
#   2015/11/30  cxw     Added parse_args(); moved profiling out of __main__.

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

