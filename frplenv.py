# frplenv.py: FRPL and PYP shell helpers

import imp
import os
import sys
import pt
import pyp

def repyp():
    imp.reload(pyp) 
    imp.reload(pt)

# To reload this file, do
#   exec(open("frplenv.py").read())
# I tried to put that command in a function in this file, but it didn't work.

# Most recent two changes:
#   2013/03/30  cwhite  Created from fe.py
#   2013/05/17  cwhite  Updated per change of fe's name to pt.

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

