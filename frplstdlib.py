# frplstdlib.py: Standard library for frpl

import frpltypes as fty
from frplast import *

import sys
import random           # for KRand
import pytz as TZ       # for date/time
import datetime as DT   # ditto
from time import sleep
#import tzlocal # TODO once installed

import myaixterm
from prompt_toolkit.shortcuts import get_input  # for _stdio_ask

##########################################################################
# Autoboxing support
FN_BOXED = 'boxed'

#FTBoolean boolean

#FTInteger number

#FTReal real

#FTString text
_pvnStringBox = 'boxtext'     # TODO?  Make this a frpl-inaccessible name?
KStringBox = None

def _stringbox_ctor(me, invocation, env, retty):
    assert len(invocation)==2
    me.setvar('boxed',fty.Entry(fty.FTString, invocation[1]))
    me.pvBoxed = invocation[1].eval(env)
        # Lock the string down at the time the ctor runs
#_stringbox_ctor

def _stringbox_charat(me, invocation, env, retty):
    """ Noname function - integer index """
    idx = invocation[0].eval(env)
    idx = idx - 1   # since frpl strings are 1-based
    if idx>=0 and idx<len(me.pvBoxed):
        return me.pvBoxed[idx]
    else:
        return ''   # fail gracefully
# _stringbox_charat

def _stringbox_len(me, invocation, env, retty):
    return len(me.pvBoxed)
# _stringbox_len

def _stringbox_unbox(me, invocation, env, retty):
    """ Return an FAConst(FTString) """
    return K(str(me.pvBoxed))
        # TODO: after calling s=sbox[unbox], s returns an AST node rather
        # than a pyval.  Fix this. 
# _stringbox_unbox

if _pvnStringBox not in fty.Lang:     # multiple-include protection
    (KStringBox, _) = MakeKindInEnv(_pvnStringBox,fty.Lang)
    KStringBox.declare_field(FN_BOXED, fty.FTString)
    KStringBox.declare_ctor(fty.FTString, _stringbox_ctor)
    KStringBox.declare_noname_method(fty.FTInteger, _stringbox_charat, 
                                        fty.FTString)
    KStringBox.declare_named_method('len', None, _stringbox_len, fty.FTInteger)
    KStringBox.declare_named_method('unbox', None, _stringbox_unbox, 
                                    fty.FTString)
    fty.FTString.set_box(KStringBox)
#endif StringBox not registered

#FTFlag flag


##########################################################################
# Built-in Kinds

# Vectors ----------------------------------------------------------------

_pvnNumVector='numbervector'
_pvnRealVector='realvector'
KNumVector=None
KRealVector=None

# ctor
def _vec_ctor(me, invocation, env, retty):
    me.wrapped=[]       # the vector of interest
    if len(invocation)==2:
        # Pre-fill with zeros
        me.wrapped = [0]*invocation[1].eval(env)
# end _vec_ctor

# dtor
def _vec_dtor(me, invocation, env, retty):
    del me.wrapped  # help the GC

# Element access
def _vec_idx(me, invocation, env, retty):
    #pdb.set_trace()
    pvIdx = invocation[0].eval(env)
    return me.wrapped[pvIdx-1]  # one-based indexing
    #try:
    #    return me.wrapped[pvIdx-1]  # one-based indexing
    #except:
    #    pdb.set_trace() # DEBUG
# end _vec_idx

# Element modification: "but"
def _vec_but_internal(me, in_place, invocation, env, retty):
    #pdb.set_trace()
    if in_place:
        newarr = me.wrapped
    else:
        newarr = me.wrapped[:]  # shallow copy
    #endif

    for cmd in invocation[1:]:

        # Sanity typecheck, but the typechecker should catch this
        if not isinstance(cmd, FAReassignment):
            raise ParseFatalException(cmd.source_string,
                cmd.source_loc, "Can't use %s in a 'but'"%str(cmd))

        # Check index
        destidx = cmd.destidx.eval(env)
        if destidx is None:
            # TODO make this a runtime exception
            raise ParseFatalException(cmd.source_string, cmd.source_loc,
                'Cannot evaluate index %s - is it initialized?'%
                str(cmd.destidx))
        if destidx<1:
            raise ParseFatalException(cmd.source_string, cmd.source_loc,
                'Attempt to evaluate index %d of array (%s)'%(
                destidx, str(cmd.destidx)))

        # Get new value
        newval = cmd.newval.eval(env)

        # Update array
        if destidx>=len(newarr):    # zero pad so it fits
            newarr += [0]*(destidx-len(newarr)) #add 1 for 0-based
            # NOTE: must use += here so newarr doesn't get clobbered.
            # This is because, for in-place, newarr is actually an alias
            # of me.wrapped.
        newarr[destidx-1] = newval      # -1 because frpl is one-based
    # next cmd

    if in_place:
        return None     # no retval from a mutator
    else:
        # create a new worker wrapping newarr
        newvec = MakeWorkerOfKind(me.fkind, [], env)
        newvec.wrapped = newarr

        return newvec # return the new worker
    #endif
# end _vec_but_internal

def _vec_but(me, invocation, env, retty):
    return _vec_but_internal(me, False, invocation, env, retty)
def _vec_mutator(me, invocation, env, retty):
    #pdb.set_trace()
    return _vec_but_internal(me, True, invocation, env, retty)

# vector length
def _vec_len(me, invocation, env, retty):
    return len(me.wrapped)
#end _vec_len

if _pvnNumVector not in fty.Lang:     # multiple-include protection
    # Create numbervector and realvector and register them in Lang.

    # For now, make separate vector types since we do not have generics.
    (KNumVector, _) = MakeKindInEnv(_pvnNumVector,fty.Lang)
    (KRealVector, _) = MakeKindInEnv(_pvnRealVector,fty.Lang)

    # Vectors have no fields; they are pure (opaque) ADTs to the frpl program.

    KNumVector.declare_ctor(FKind.OptionalArg(fty.FTInteger), _vec_ctor)
    KRealVector.declare_ctor(FKind.OptionalArg(fty.FTInteger), _vec_ctor)

    KNumVector.declare_dtor(None, _vec_dtor)
    KRealVector.declare_dtor(None, _vec_dtor)

    KNumVector.declare_noname_method(fty.FTInteger, _vec_idx, fty.FTInteger)
    KRealVector.declare_noname_method(fty.FTInteger, _vec_idx, fty.FTReal)

    KNumVector.declare_named_method('but', FKind.AtLeastOne(fty.FTGets),
                                    _vec_but, KNumVector.worker_ty)
    KRealVector.declare_named_method('but', FKind.AtLeastOne(fty.FTGets),
                                    _vec_but, KNumVector.worker_ty)

    KNumVector.declare_mutator(FKind.AtLeastOne(fty.FTGets), _vec_mutator)
    KRealVector.declare_mutator(FKind.AtLeastOne(fty.FTGets), _vec_mutator)

    KNumVector.declare_named_method('len', None, _vec_len, fty.FTInteger)
    KRealVector.declare_named_method('len', None, _vec_len, fty.FTInteger)
#endif

# Random numbers ---------------------------------------------------------

_pvnKRand='randomgen'   # Kind name
KRand=None

# ctor
def _rand_ctor(me, invocation, env, retty):
    """ ctor: optional number argument seed """
    if len(invocation)==2:
        seed = invocation[1].eval(env)
    else:
        seed = None
    random.seed(seed)
# end _rand_ctor

# no dtor

# Get random integers within a specified range
def _rand_randint(me, invocation, env, retty):
    """ rand[m]: return a random integer x, 1<=x<=m
        rand[i,j]: return a random integer x, i<=x<=j
    """
    #pdb.set_trace()
    if len(invocation)==2:
        pvMin = invocation[0].eval(env)
        pvMax = invocation[1].eval(env)
    else:
        pvMin = 1
        pvMax = invocation[0].eval(env)
    #endif
    if pvMin > pvMax:
        pvMin,pvMax = pvMax,pvMin
    return random.randint(pvMin,pvMax)
# end _rand_randint

def _rand_uniform(me, invocation, env, retty):
    """ rand[uniform]: return a random real x, 0<=x<1 """
    return random.random()
# end _rand_uniform

if _pvnKRand not in fty.Lang:
    # Register KRand
    (KRand, _) = MakeKindInEnv(_pvnKRand,fty.Lang)

    KRand.declare_ctor(FKind.OptionalArg(fty.FTInteger), _rand_ctor)

    KRand.declare_noname_method(
        FKind.SpecificNumberOf(fty.FTInteger,1,2), _rand_randint, fty.FTInteger)

    KRand.declare_named_method('uniform', None, _rand_uniform, fty.FTReal)
#endif

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Date and time = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

# Similar to pytz, but not exactly.  Classes are timezone, when (a datetime),
# duration, and beat.

# Register all so types will be available

_pvnKTimeZone='timezone'   # Kind name  # TODO try "cal.timezone"
KTimeZone=None
_pvnKWhen='when'
KWhen=None
_pvnKDuration='duration'
KDuration=None
_pvnKBeat='beat'
KBeat=None
_pvnKShapeTools='shapetools'
KShapeTools=None

_timezone_doregister = False
if _pvnKTimeZone not in fty.Lang:   # proxy for registration of all
    _timezone_doregister = True
    (KTimeZone, _) = MakeKindInEnv(_pvnKTimeZone, fty.Lang)
    (KWhen, _) = MakeKindInEnv(_pvnKWhen, fty.Lang)
    (KDuration, _) = MakeKindInEnv(_pvnKDuration, fty.Lang)
    (KBeat, _) = MakeKindInEnv(_pvnKBeat, fty.Lang)
    (KShapeTools, _) = MakeKindInEnv(_pvnKShapeTools, fty.Lang)
#endif need to register

# timezone ---------------------------------------------------------------

# ctor
def _timezone_ctor(me, invocation, env, retty):
    """ ctor: optional string argument timezone, or 'local' for the local
        timezone.  UTC if no argument is provided. """
    if len(invocation)==2:
        thetz = invocation[1].eval(env).strip()
    else:
        thetz = 'UTC'   # default

    if thetz.lower()=='local':
        thetz='UTC'     # TODO get zone from tzlocal

    me.timezone = TZ.timezone(thetz)
# end _timezone_ctor

# no dtor

def _timezone_now(me, invocation, env, retty):
    """ Return current time in this timezone.  No args."""
    # TODO make sure this is actually doing what we want.
    dt = DT.datetime.now(me.timezone)
    return dt.timestamp()
# end _timezone_now

if _timezone_doregister:
    KTimeZone.declare_ctor(FKind.OptionalArg(fty.FTString), _timezone_ctor)

#    KTimeZone.declare_noname_method(
#        FKind.SpecificNumberOf(fty.FTInteger,1,2), _timezone_timezoneint, fty.FTInteger)
#
    KTimeZone.declare_named_method('now', None, _timezone_now, fty.FTReal)
#endif

# when -------------------------------------------------------------------

# ctor
def _when_ctor(me, invocation, env, retty):
    """ ctor: TODO """
# end _when_ctor

# no dtor

if _timezone_doregister:
    KWhen.declare_ctor(FKind.OptionalArg(fty.FTString), _when_ctor)
#endif

# duration ---------------------------------------------------------------

# ctor
def _duration_ctor(me, invocation, env, retty):
    """ ctor: TODO """
# end _duration_ctor

# no dtor

if _timezone_doregister:
    KDuration.declare_ctor(FKind.OptionalArg(fty.FTString), _when_ctor)
#endif

# beat -------------------------------------------------------------------

# ctor
def _beat_ctor(me, invocation, env, retty):
    """ ctor: TODO """
# end _beat_ctor

# no dtor

if _timezone_doregister:
    KBeat.declare_ctor(FKind.OptionalArg(fty.FTString), _duration_ctor)
#endif

# shapetools -------------------------------------------------------------

def _shapetools_incircle(me, invocation, env, retty):
    """ [incircle,x,y,X,Y,R]: determine whether (x,y) is in the circle
        centered at (X,Y) with radius R.
        For now, all parms are integers.
    """
    pt_x = invocation[1].eval(env)
    pt_y = invocation[2].eval(env)
    cx = invocation[3].eval(env)
    cy = invocation[4].eval(env)
    r = invocation[5].eval(env)
    return ( ( (pt_x-cx)**2 + (pt_y-cy)**2 ) < r**2 )
# end _shapetools_incircle

if _timezone_doregister:
    # Uses default ctor; no dtor
    KShapeTools.declare_named_method('incircle',
        FKind.SpecificNumberOf(fty.FTInteger,5,5), _shapetools_incircle,
        fty.FTBoolean)
#endif

# Animation helpers ------------------------------------------------------

_pvnKAnim='animstudio'   # Kind name
KAnim=None

# no ctor or dtor

# Sleep
def _anim_msleep(me, invocation, env, retty):
    """ anim[msleep,x]: sleep for x milliseconds """
    pvmillis = invocation[1].eval(env)
    sleep(pvmillis/1000.0)  # since sleep takes seconds
    return None
# end _anim_msleep

if _pvnKAnim not in fty.Lang:
    # Register KAnim
    (KAnim, _) = MakeKindInEnv(_pvnKAnim,fty.Lang)

    KAnim.declare_named_method('msleep', fty.FTInteger, _anim_msleep, None)
#endif

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# stdio = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# "gem" is the name of the console.  It has "say" and "ask" methods.

_pvnKStdio='console'   # Kind name.
KStdio=None

# ctor
def _stdio_ctor(me, invocation, env, retty):
    myaixterm.aix_init()
# end _stdio_ctor

# no dtor

def _stdio_say(me, invocation, env, retty):
    """ stdio[say,...]: print all args.  If the last option is
        /stay, don't print a new line (i.e., stay on the same line). """

    #assert len(invocation)>1    # should be guaranteed by the type system
    # but if we don't get any args we just don't print anything :)

    strs=[]
    eol='\n'
    building=''

    for elem in invocation[1:]:
        if isinstance(elem, FAFlag):
            if elem.pvFlagname=='stay':
                eol = ''    # don't go to next line
            elif elem.pvFlagname=='left':
                building += '\r'
            elif elem.pvFlagname=='home':
                building += '\033[1;1H'
            else:
                if elem.pvFlagname[0:2]=='on': 
                    # /on<name> -- background
                    building += myaixterm.aix_bg(elem.pvFlagname[2:])
                elif elem.pvFlagname=='fg' and elem.arg is not None:
                    # /fg/<color index>
                    building += myaixterm.aix_fg(int(elem.arg.eval(env)))
                elif elem.pvFlagname=='bg' and elem.arg is not None:
                    # /bg/<color index>
                    building += myaixterm.aix_bg(int(elem.arg.eval(env)))
                else:
                    # /<name> --- foreground
                    building += myaixterm.aix_fg(elem.pvFlagname)
            #endif /stay else
        else:   # a variable
            val = elem.eval(env)
            if val is not None:
                strs += [building+str(val)]
            building = ''
        #endif flag else
    #foreach elem

    if building != '':
        strs += [building]

    if len(strs)>0:
        strs[-1] += myaixterm.aix_normal()
                        # always leave it sane
        print(' '.join(strs), end=eol)
    #endif there was something to print

# end _stdio_say

def _stdio_say_help(me, invocation, env, retty):
    print("""Possible colors are:
black(0) """,end='')
    for k in sorted(myaixterm.aix_colors.keys()):
        print('%s%s(%d)'%(myaixterm.aix_fg(k), k, myaixterm.aix_colors[k]),
                end=' ')
    print(myaixterm.aix_normal())
    print(""" Options for gem[say,...] are:
    /<colorname>: set foreground to <colorname>
    /fg/<colornumber>: set foreground to <colornumber>
    /on<colorname>: set background to <colorname>
    /bg/<colornumber>: set background to <colornumber>"""
    )

# end _stdio_say_help

def _stdio_ask(me, invocation, env, retty):
    #pdb.set_trace()

    ask_def_prompt = True   # whether to use the default prompt
    nodes=[]    # nodes to evaluate as part of the prompt

    for arg in invocation[1:]:     # skip the function name
        if arg.ty is fty.FTString:
            ask_def_prompt = False
            nodes += [arg]
        if isinstance(arg,FAReadVar):   # typespec or prompt
            decltype = env.decltype(arg.pvName)
            if decltype is None:
                ask_def_prompt = False
                # It wasn't a type, so assume it's part of a prompt.
                nodes += [arg]
    # next arg

    # Evaluate the prompt if any nodes, or use the default
    if len(nodes)>0:
        operands=[str(n.eval(env)) for n in nodes]
        prompt = ' '.join(operands) + ' '
            # always a trailing space for the convenience of the person
            # begin prompted
    else:
        prompt ='What say you?> '
    #endif

    # Get the input.  TODO figure out history.
    instr=get_input(prompt)

    # Convert the input to retty if possible.
    return retty.val_from_str(instr)
# end _stdio_ask

def _stdio_save(me, invocation, env, retty):
    theenv = env.find(fty.PVN_SAVE)
    if theenv is None:
        print("I'm sorry, but I can't save this session.")
    else:
        saver = theenv[fty.PVN_SAVE].pyval
        saver.save()
    #end if
# end _stdio_save

def _stdio_load(me, invocation, env, retty):
    theenv = env.find(fty.PVN_LOAD)
    if theenv is None:
        print("I'm sorry, but I can't load this session.")
    else:
        loader = theenv[fty.PVN_LOAD].pyval
        loader.load()
    #end if
# end _stdio_save


if _pvnKStdio not in fty.Lang:
    # Register KStdio
    (KStdio, _) = MakeKindInEnv(_pvnKStdio, fty.Lang)

    KStdio.declare_ctor(None, _stdio_ctor)

    # TODO tighten up the matching pattern below.
    KStdio.declare_named_method('say',
            FKind.AtLeastOneStr(FKind.EitherOr(FKind.WildcardType(),
                                FKind.WildcardFlag(True))),
        _stdio_say, fty.FTNone)     # True: allow flag args

    # Help.  TODO figure out how to do gem[say,/help]
    KStdio.declare_named_method('help',None,
                                    _stdio_say_help, fty.FTNone)

    KStdio.declare_named_method('ask',
        FKind.AnyArgs, _stdio_ask, fty.FTString,
        FKMethFlags.OptionalFlexReturn)

    KStdio.declare_named_method('save', None, _stdio_save, fty.FTNone)
    KStdio.declare_named_method('load', None, _stdio_load, fty.FTNone)

#endif

# Default environment ====================================================

# The default environment a frpl program can expect.  Not added to Lang
# since it belongs in the next environment in.
DefaultEnvProgram = """s nm text
t nm text
sbox nm boxtext
n nm number
o nm number
b nm boolean
q nm real
r nm real
nv nm numbervector
nv = numbervector[rez]
random nm randomgen
random = randomgen[rez]
here nm timezone
here = timezone[rez,'local']
UTC nm timezone
UTC = timezone[rez]
gem nm console
gem = console[rez]
shapes nm shapetools
shapes = shapetools[rez]
anim nm animstudio
anim = animstudio[rez]
"""
    # TODO FIXME: random is a mutable instance.  Figure out how to handle that.

# Last two changes:
#   2015/10/03  cxw     Renamed DefaultEnv to DefaultEnvProgram
#   2015/11/14  cxw     Added colornumbers to _stdio_say_help

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

