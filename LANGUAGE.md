# About the frpl language #
frpl language design principles:
  1. All blocks are self-delimiting - no compound statements.
      (e.g., for ... next rather than for begin...ta)
  2. No whitespace sensitivity (except for comments to eol and
      separating tokens)
  3. No special cases in the syntax - you can write user subroutines
      that have the same calling flexibility as built-in subroutines.
  4. The shift key is used for advanced features but not for
      basic features.
      4b. The shift key shall NEVER be required to use any feature.
  5. The language should be usable for the programs that control an
      entire robot soccer team.
  6. As much as possible, avoid typos that will succeed silently.
  6b. Never double characters to change meaning (e.g., C's = vs ==).
  7. No implicit conversions between any type and bool.  Conditional
      statements only accept bools.
  8. No implicit conversions period!  Get used to banging the bits;
      the skill will serve you well.

IDE notes: no programmer-speak in error messages; show expected and
provided side-by-side so programmer can figure out what to do.
Also, always provide an easy way to get back to a known state.

NOTES:
  - Reserve @ and ! for pointers: prefix @ is address-of; postfix ! is
      deference.  Don't use ! for not.

