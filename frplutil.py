# frplutil: Utility routines

import pyp

def flatten(l, ltypes=(list, tuple)):
    """ Flatten a list - see copyright notice at end of file """
    ltype = type(l)
    l = list(l)
    i = 0
    while i < len(l):
        while isinstance(l[i], ltypes):
            if not l[i]:
                l.pop(i)
                i -= 1
                break
            else:
                l[i:i + 1] = l[i]
        i += 1
    return ltype(l)
# flatten()

#===============================================================
# Parsing helpers

FB = pyp.FollowedBy
CK = pyp.CaselessKeyword

def CKOneOf(x):
    return pyp.oneOf(x, True, asKeyword=True)   # caseless keywords

def SCK(x):
    return pyp.Suppress(pyp.CaselessKeyword(x))

_ONEOF_CASELESS = True  # friendly names for flags
#_ONEOF_KEYWORD = True
def CaselessOneOf(x):
    return pyp.oneOf(x, _ONEOF_CASELESS)

#===============================================================
# Copyright messages

# flatten by MonkeeSage copied from
# http://rightfootin.blogspot.com/2006/09/more-on-python-flatten.html
# and modified from BasicTypes.
# BasicTypes required message:
#BasicProperty and BasicTypes
#   Copyright (c) 2002-2003, Michael C. Fletcher
#   All rights reserved.
#
#THIS SOFTWARE IS NOT FAULT TOLERANT AND SHOULD NOT BE USED IN ANY
#SITUATION ENDANGERING HUMAN LIFE OR PROPERTY.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions
#are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials
#    provided with the distribution.
#
#    The name of Michael C. Fletcher may not be used to endorse or 
#    promote products derived from this software without specific 
#    prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#COPYRIGHT HOLDERS AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
#STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#OF THE POSSIBILITY OF SUCH DAMAGE.

#---------------------------------------------------------------------
# Last two changes:
#   2014/01/26  cxw     Added flatten
#   2015/10/08  cxw     Moved parsing helpers from frplparse


# vi: set ts=4 sts=4 sw=4 expandtab ai: #

