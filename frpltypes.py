#!/bin/env python
# frpltypes.py: Basic structure and type system for frpl.

#   TODO: decide whether to support floating point.
#   TODO change vectors to general arrays?
#   TODO add a read-only flag to Entry.

import abc  # abstract base classes
import shortuuid
# NOTE: does not import any other frpl modules to avoid circular dependencies.

import pdb #DEBUG

#-------------------------------------------------------------------------
# Type names:
# - Type names are in [A-Za-z@].  Identifiers that cannot occur in frpl
#   source (and only those) have an @ in the name.
# - In a "gives" pattern, a type is represented as `#foo#'.
# - In a "wants" pattern, a list of possible matching types are represented
#   as `#foo#bar#bat#baz#' (since # isn't a regex metachar)
# - In a pattern, a flag is represented as <foo/> or <foo/bar>
#   for name foo and type bar.  Bar can have multiple #-delimited options.
#   TODO permit #-delimited options in foo?
# - In a pattern, individual parms are separated by commas.
# - Patterns have no whitespace.

##########################################################################
# frpl-specific exceptions

class FrplError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
# class FrplError

##########################################################################
# Specialized identifiers in environments

#PyVal Names, i.e., strings and not FAConsts
PVN_SAVE = '@save'      # provided by runtime system to save environment
PVN_LOAD = '@load'      # provided by runtime system to save environment
    # Note: '@' is used as the special flag because it's not a regex
    # operator.

##########################################################################
# Environments

class Entry(object):
    # TODO add default values (initializers) and RO flag
    """An entry in an environment.  """
    def __init__(self,ty, pyval=None, s=None, l=None, decltype=None):
        """Create entry with new type ty and new python value pyval (if any).
            Inputs:
                ty          the FTBase of pyval
                pyval       the Python value stored in the instance
                s           The source string
                l           The source location
                decltype    The FTBase for new instances when this is used
                            in a declaration. (default None)

            Entries that represent Kinds have non-None decltypes.
            For these entries, ty and pyval are the agency.
            decltype is FKind.worker_ty.
            In this way, a single Entry can represent an FTBase and an
            ast.FWorker instance.  This is very useful for Kinds, for which
            the FTBase is the Kind's worker_ty, and the pyval is the Agency
            FWorker instance.
        """
        self.ty,self.pyval,self.decltype=ty,pyval,decltype
        self.source_string,self.source_loc=s,l
    # end __init__

    def carboncopy(self):
        """ Return a new Entry referring to the same pyval"""
        retv = Entry(self.ty, self.pyval,
                        self.source_string, self.source_loc, self.decltype)
        return retv

    def __str__(self):
        if self.pyval is None:
            s=''
        else:
            s='pyval %s%s'%(str(self.pyval),
                                ', ' if self.decltype is not None else '')
        #endif

        if self.decltype is not None:
            s += 'decltype %s'%str(self.decltype)

        #return "(%s->pyval %s)"%(str(self.ty),str(self.pyval))
        return "(%s->%s)"%(str(self.ty),s)
    # end __str__

    def __repr__(self):
        return str(self)
# end class Entry

class Env(dict):
    # from lis.py, (c) Peter Norvig, 2010; See http://norvig.com/lispy.html
    # Believed to be available for reuse based on references seen by Norvig
    # to derivative works.
    # Modified:
    #   2014/02/14  cxw     Permit _outer_ to be None.
    """An environment: a dict of {'var':val} pairs, with an outer Env.
        Holds Entry() instances except in FKind.methods.
    """
    def __init__(self, parms=(), args=(), outer=None):
        super().__init__()
        self.uu = shortuuid.uuid()[:5]  # DEBUG
        self.update(zip(parms,args))
        self.outer = outer
        self.sn=0   # used as a flag to invalidate memoized exprs when
                    # an entry in the environment is updated.
        #print('$$$ created env %s->%s'%(self.uu,   # DEBUG
        #        'None' if self.outer is None else self.outer.uu))
    # __init__()

    def __str__(self):
        return '|[%s->%s: %s]|'%(self.uu,
            'None' if self.outer is None else self.outer.uu,
            super().__str__())

    def newframe(self,otr=None):
        """ Make a new Env with the same keys and outer but a new sn and
            no values. """
        retv = Env(outer=otr if otr is not None else self.outer)
        for (k,v) in self.items():
            retv[k] = v.carboncopy()
            # Add all the same names in, but in fresh Entry()s.
        # next k,v
        return retv
    # newframe()

    def find(self, pvName:str):
        "Find the innermost Env where pvName appears."
        return (self if pvName in self else
                    (None if self.outer is None else self.outer.find(pvName) ))

    def decltype(self, pvName:str):
        """ Helper function to return the decltype of _pvName_, or None."""
        env = self.find(pvName)
        if env is None:
            return None
        else:
            ent = env[pvName]
            return ent.decltype
    # end decltype()

    def add(self, pvName:str, pyval):
        """ Always used to add entries to the Env.  Checks for
            duplicates.
        """
        if pvName in self:
            raise FrplError(
                'Attempt to create entry %s, which already exists'%pvName)

        self[pvName] = pyval
    #end add()

#end class Env

# Outermost environment - basic language definitions #####################
Lang = Env()

# Type registry ##########################################################
_Typereg = {}

def Name2Type(aname):
    """ DEPRECATED in favor of putting FTBases in Envs."""
    return _Typereg[aname]
    # TODO decide whether to return None rather than throwing

def IsType(aname):
    """ DEPRECATED in favor of putting FTBases in Envs."""
    return (aname in _Typereg)

# Utility classes and functions ##########################################
def _nop(s=None):
    pass

class FieldBundle(object):
    """A class I can add fields to by assigning them.  With plain 'object',
        that doesn't work."""
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name
    def __repr__(self):
        return str(self)
# end class FieldBundle

# Types ##################################################################

class FTBase: #(metaclass=abc.ABCMeta):
    """Ancestor for all classes representing frpl types.
        vfs: function that returns a python value of this type
        from 's', or throws."""
    def __init__(self, aname, vfs, s=None, l=None, handle=None):
        # TODO implement vfs as a member function of the appropriate Kind
        """ Create a frpl type.  Inputs:
            aname (reqd)    The name of the type
            vfs (reqd)      A function to parse a string into instances of
                            this type, if such a thing is possible.
            s, l            Source code string and location of the declaration
            handle          Optional opaque data held but not processed
                            by FTBase.
        """
        self.name=aname             # name should be human-readable
                                    # and human-comprehensible.
        self.val_from_str=vfs   # doesn't take _self_.
            # Used by 'ask' to take keyboard input of this type.

        self.handle = handle

        _Typereg[aname]=self    # register with the map

        self._implements=[]
            # List of interfaces or capabilities this type implements.
            # Capabilities themselves can be any object, since all
            # self._implements does is stash them.

        self.pvkBox = None
            # a python instance of frplast:FKind() that represents
            # the autoboxing class (if any) for this type.
    # end __init__

    def __str__(self):
        s="`"+self.name
        if self.handle is not None:
            s += '[%s]'%str(self.handle).replace("'","-")
                # replace() because type names can't include
                # apostrophes or else resolving won't work
        s += "'"
        return s

    def ipattern(self, wants=False):
        """ for duck typing with frplast.FTNode.
            If wants=False, a _gives_ pattern representing this specific
            type.  If wants=True, a _wants_ pattern representing anything
            that can be this type. """
        if wants:
            return "`[^']*#%s#[^']*'"%self.name
        else:
            return "`#%s#'"%self.name
        #return str(self)

    def __repr__(self):
        return str(self)

    # restricted propget, propput for self._implements (basically)
    def this_implements(self, x):
        self._implements.append(x)

    def does_implement(self,x):
        return x in self._implements

    # autoboxing
    def set_box(self,pvkBox):
        self.pvkBox=pvkBox
    def get_box(self):
        return self.pvkBox

# end class FTBase

# Representations of FTBases in Envs.
FTType = FTBase('type', _nop)

def _register(ty):
    """ Register a type as being a fundamental frpl type (in Lang) """
    Lang.add(ty.name, Entry(FTType, ty, decltype=ty, s='', l=0))
        # Decltype points to the type.
        # This will make other code cleaner since it unifies
        # kinds and primitives in decltype.
#end _register

# NOTE: I am not registering FTType itself, since frpl code (at least as
# the language is presently defined) cannot manipulate types as, well,
# first-class types.

#-----------------------
# Helpers for making new types

def _vfs_maker(cond, val, name):
    def inner(s):
        if cond(s):
            return val(s)
        else:
            raise ValueError("Can't make `%s' from '%s'"%(name,s))
    #end inner
    return inner
#end vfs_maker

def _succeeds(fn):
    def inner(arg):
        try:
            fn(arg)
        except:
            return False
        return True
    return inner
# end _succeeds

def _vfs_passthrough(nm):
    return  _vfs_maker(lambda s:True, lambda s:s, nm)

def _vfs_none(nm):
    return _vfs_maker(lambda s:False, lambda s:None, nm)

# Function usable from outside to create frpltypes.

def NewFType(name, handle=None):
    """ Create a new FType.  The caller is responsible for registering that
        type in any appropriate Env.
        Inputs:
            name (required)                 The name of the type
            handle (optional, default None) User data stored in rv.handle
    """
    return FTBase(name, _vfs_none(name), handle=handle)

#-----------------------
# Capabilities

FCCallable = FieldBundle('callable!')
    # If a type implements FCCallable, its pyvals are classes having
    # a .invoke() method matching the signature of frplast.FWorker.invoke()

#-----------------------
# Primitive types

FTNone = FTBase("none",        # TODO what to call this???
    _vfs_maker(lambda s:s.lower()=="none",
                lambda s:None,
                'none'))
# Not registered - no instances of Nones.

FTBoolean = FTBase("boolean", # TODO call this "true/false" or some such?
    _vfs_maker(lambda s:s.lower() in ["true", "false"],
                lambda s:s.lower=='true',
                'boolean'))
_register(FTBoolean)

FTInteger = FTBase("number",   # TODO call this "whole number" or some such?  int?
    _vfs_maker(_succeeds(int),
                int,
                'number'))
_register(FTInteger)

FTReal = FTBase("real", # TODO call this "fractional number" or some such?
    _vfs_maker(_succeeds(float),
                float,
                'real'))
    # may need to support other formats later
# TODO maybe make it all fixed-point?
_register(FTReal)

FTString = FTBase("text", # TODO call this "words" or some such? text?
    _vfs_passthrough('text'))
_register(FTString)

FTFlag = FTBase("flag", _vfs_none('flag'))    # options in envelopes
_register(FTFlag)

# Internal-use types

FTOperator = FTBase("@operator",    # overloadable operators
    _vfs_passthrough('operator'))

FTIdentifier = FTBase("@identifier",    # TODO call this "name"? "object"?
    _vfs_passthrough('identifier'))

FTFunction = FTBase("@function",    # or "callable"?
    _vfs_passthrough('function'))

FTGets = FTBase("@reassignment", _vfs_passthrough('reassignment'))
    # ast.FAReassignment

FTNativeFunction = FTBase("@native_function", _vfs_none('native_function'))
    # a Python function

#---------------------------------------------------------------------------
# Last two changes:
#   2015/10/03  cxw     Fixed FTBase.__str__() so the handle is inside `'.
#                       This way the resolver can work on types with handles.
#   2015/10/16  cxw     Added Env.newframe()

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

