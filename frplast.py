# frplast.py: frpl abstract syntax tree # Modified from imp_ast.py by Jay Conrod; see license note at end of file.
# Note that this code uses Python 3 function annotations to specify
# argument types.  However, it does not as yet check those types.
#
# DESIGN RULE: No pyp.ParseResults objects are allowed in the AST.
# CONVENTION: python values include "pv" in the identifier; frpl values
#               (FANodes) include "fv" in the identifier.
#

# TODO implement field access in Workers.  Currently only methods are usable.
# TODO: initialize vars to defaults in FADecl.eval.
# TODO test FTVariantReturn material, including get_first_type_parm and
#      updating _resolve_by_name, if more testing is needed.

# Overview:
#   In frpl, "Kinds" are classes, "Agencies" are class factories,
#   and "Workers" are instances.
#   Kinds are inaccessible to frpl programs; only their corresponding
#   Agencies are accessible.  For example, Kind KVehicle has an KAgency
#   instance named VehicleAgency registered to frpl.  A frpl program can
#   say         Vehicle[rez]        to get a new Vehicle worker.

import re

import frpltypes as fty
from frpltypes import Env, Entry, FrplError
import abc  # abstract base classes
from pyp import ParseFatalException, ParseResults, col, lineno
import pdb
import copy

#from overrides import overrides #TODO figure out why this doesn't work

##########################################################################
# Abstract syntax tree (parser output) ###################################
# This is part 1, which is what we need for Kind/Worker to be defined.
# More AST types are after the Kind/Worker definitions.

_DEF_SSTR = ''
_DEF_SLOC = -1

_NO_MEMO_ENV = object()
    # unique marker for when there is no memoized eval result

class FANode(metaclass=abc.ABCMeta):
    """ abstract base class for frpl AST """
    def __init__(self, ty=fty.FTNone, s=_DEF_SSTR, l=_DEF_SLOC,
                    needsframe=False):
        self.ty = ty
        self.source_string = s  # for debugging
        self.source_loc = l
        self.needsframe = needsframe
            # True if the FABlock most closely enclosing this node
            # should create a new stack frame.

        self.pvMemo=None                    # memoize the result of an eval
        self.memo_env = _NO_MEMO_ENV        # the env for the memoized eval
        self.memo_env_sn = -1

        if l != _DEF_SLOC:  # def is used for items not directly in the source
            self.source_line = lineno(l,s)
            self.source_col = col(l,s)
        else:
            self.source_line = 0
            self.source_col = 0
    #end __init__

    def __repr__(self): return str(self)    # for convenience

    def _loc(self):
        return "line %d, char %d"%(lineno(self.source_loc, self.source_string),
                                   col(self.source_loc, self.source_string))
    # _loc()

#    def ty(self):   # useful default, non-abstract implementation.
#        """ Return type of this node.  Used at parse time. """
#        return self.ty

    def eval(self,env):
        if (env is not self.memo_env) or (env.sn != self.memo_env_sn):
            # always true on the first call to eval()
            self.memo_env = env
            self.memo_env_sn = env.sn
            #pdb.set_trace() #DEBUG
            self.pvMemo = self._eval(env)
        #endif
        return self.pvMemo

    def ipattern(self, wants=False) -> str:
        """ Returns a string pattern that can be matched by FWorker.invoke
            and related functions. """
        return self.ty.ipattern(wants)
    #end ipattern()

    @abc.abstractmethod
    def _eval(self,env):
        """ Return the current value of this node, if any.
            For operations with side-effects, updates _env_
            according to the side effects.  Used at compile time
            with env==None, and at runtime with env != None. """
        pass
# end FANode

class FANop(FANode):
    """ The lowly nop, here so that all statements will return FANodes.
        *** NOTE: FANops are discardable - any FANop may be pruned from ***
        *** the AST at any time.                                        ***
    """
    def __init__(self, ty=fty.FTNone, s=_DEF_SSTR, l=_DEF_SLOC):
        """ The _ty_ arg is to permit FANops to stand in for parameters in
            FKind.inv2pat().
        """
        super().__init__(ty, s,l)
    def __str__(self):
        return "(nop)"
    def _eval(self,env):
        return None     # nop!
#end class FANop

class FAConst(FANode):
    """ constants """
    def __init__(self, ty, pyval, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(ty, s,l)
        self.pyval = pyval
    def _eval(self,env): return self.pyval
    def __str__(self):
        return ('(%s|const =%s=)' %(str(self.ty), str(self.pyval)))
# end FAConst

# Helper function for making constants
def K(x):
    if isinstance(x,bool):
        return FAConst(fty.FTBoolean, x)
    elif isinstance(x,int):
        return FAConst(fty.FTInteger, x)
    elif isinstance(x,float):
        return FAConst(fty.FTReal, x)
    elif isinstance(x,str):
        return FAConst(fty.FTString, x)
    else:
        raise ValueError(
                "Cannot make an FAConst from %s %s"%(str(type(x)), str(x)))
# end MakeConst

# Constants we will use later
FATrue = K(True)
FAFalse = K(False)
FAPlusOne = K(1)
FAMinusOne = K(-1)

# Flags
class FAFlag(FANode):
    """ Flags (/foo).  Flags can be bare or can have a single
        argument.  Flags are always lower-case.  """
    def __init__(self, pvFlagname:str, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(fty.FTFlag, s,l)
        self.pvFlagname = str(pvFlagname).lower()
        self.arg = None   # FANode: a flag's argument.
        # flags can only hold one arg.
    # __init__()

    def bind(self,arg):
        """ Binds an argument to a flag. """
        self.arg = arg

    def _eval(self,env): return self.pvFlagname

    def __str__(self):
        if self.arg is None:
            return "/%s"%self.pvFlagname
        else:
            return "/%s(%s)"%(self.pvFlagname,self.arg)
    #__str__

    @staticmethod
    def ipat(nm,ty, wants=False):
        """ ipattern accessible from outside.  If _wants_, returns
            a wants pattern, otherwise returns a gives pattern."""
        if ty is None:
            return '<%s/>'%nm
        else:
            return '<%s/%s>'%(nm,ty.ipattern(wants))
    #ipat

    #@overrides
    def ipattern(self, wants=False) -> str:
        return FAFlag.ipat(self.pvFlagname,
                self.arg.ty if self.arg is not None else None,
                wants)
    #ipattern()

# end FAFlag

#def Flg(pvFlagname:str):
#    """ helper functions for making options """
#    return FAFlag(pvFlagname)
## end Flg

##########################################################################
# Kinds and Workers (classes and instances) ##############################

# Notation: python instances are "pi"; frpl instances are "fi"

# Magic names
# constructor/destructor.  These are only called by the runtime system,
# never from frpl code.  Therefore, they have non-frpl identifiers
# ('@' is not a valid FAIdent char).
_PVN_CTOR = '@ctor'
_FVN_CTOR = FAConst(fty.FTString, _PVN_CTOR)
_PVN_DTOR = '@dtor'
_FVN_DTOR = FAConst(fty.FTString, _PVN_DTOR)

_PVN_NONAME='@noname' # flag for FKind().methods.

# Special names
_PVN_REZ = 'rez'    # available to frpl programs
_PVN_MUTATOR = '@mutate'    # change-in-place - for optimization
    # TODO make sure this works once I introduce subroutines and
    # Workers with state

# Exported special names
PVN_CURRENT_ME = '@me'
    # The instance to which "me" should resolve.  "Current" because there
    # will be more than one of these stacked up if you have nested classes.

# === Resolver ===

# TODO refactor all this to make more sense!

# Internal constants
_FVARGS_ACCEPT_ANY = '.*'     #pattern to accept any args (>=0) to a method

# Flags for methods
class FKMethFlags(object):
    FixedReturn=0x00        # default
    DefaultFlags=FixedReturn
    FlexReturn=0x01         # return value specified in the invocation
    HasDefaultRetty=0x02    # has a default return value.  Ignored if not
                            # FlexReturn
    OptionalFlexReturn = FlexReturn | HasDefaultRetty
#end class FKMethFlags

class _FKIPattern(object):
    """ Class for special-purpose ipattern()s.  Holds patterns the caller
        builds manually.  Doesn't verify those patterns in any way. """
    def __init__(self, givespat:str, wantspat:str=""):
        self.givespat = givespat
        self.wantspat = wantspat

    def ipattern(self, wants=False) -> str:
        """ Returns a string pattern that can be matched by FWorker.invoke
            and related functions. """
        return self.wantspat if wants else self.givespat
    #end ipattern()

# -------------------------------------------------------------------
# --- Ultimate ancestor kind ---
# The FKind class is defined below and needs an FKind as a parent.
# To bootstrap, we manually make something that looks like an FKind to
# serve as the ultimate ancestor Kind.


_PIKRoot = fty.FieldBundle('Root')
_PIKRoot.worker_ty = fty.FTNone     # no instances, so no type
_PIKRoot.parentkind = None          # Fill in fields so it looks like an
_PIKRoot.fields = Env(outer=None)   # FKind instance.
_PIKRoot.fields.add('magic', Entry(fty.FTInteger, 42))
    # DEBUG - so there's something to look up

# Predefined methods accessible from frpl
_PIKRoot.methods = Env(outer=None)

# Default constructor/dtor - does nothing
def _RK_nop(me, fvInvocation, env, retty):
    pass

_PIKRoot.methods.add(_PVN_CTOR, [(re.compile(''), _RK_nop, fty.FTNone,
                                    FKMethFlags.FixedReturn)])
_PIKRoot.methods.add(_PVN_DTOR, [(re.compile(''), _RK_nop, fty.FTNone,
                                    FKMethFlags.FixedReturn)])

def RootKind():
    """ Returns the FKind instance corresponding to the ultimate ancestor
        of all kinds. """
    return _PIKRoot

# -------------------------------------------------------------------
# --- Kind: frpl's equivalent of classes ---

# Note: No circular dependencies.  Kinds don't create instances.
# Kind just holds the methods.
# Worker.__init__ takes an FKind instance.  Each FKind pvinstance represents
# a frpl Kind.
# _PIAgency is an FKind instance (frpl Kind) that has one Worker per
# other Kind created.
# There is a helper function, declared below Kind, Agency, and Worker,
# that makes an FKind instance and its corresponding FWorker(KAgency) instance.
# The FKind pointer is stored in a python data member in the FW(FA) instance
# and not in fields[] so that frpl code can't access it.

class FKind(object):
    """ Each python instance represents a Kind (i.e., a class)
        defined in a frpl program.
        A Kind is a wrapper around .methods, the named vtbl (nvtbl)
        for the frpl-accessible methods.  All methods in a frpl Kind
        are virtual.
    """

    def __init__(self, name:str, parentkind=_PIKRoot):
        """ Create a python instance of FKind, i.e., a frpl Kind.
            name: the name of the new Kind.  Must be a valid frpl identifier.
            parentkind: The pi of the parent of the new Kind.
                        Defaults to _PIKRoot.
        """
        #parentkind is usually an FKind, but might be _PIKRoot, so it is
        #not type-annotated.

        # Create the frpltype of frpl instances of this Kind
        self.worker_ty = fty.NewFType(name, handle=self)
            # handle: the type holds a reference to this FKind pyinstance,
            # i.e., to the frpl Kind.
        self.worker_ty.this_implements(fty.FCCallable)
            # workers can have methods called on them

        # Initialize fields
        self.name = name
        self.parentkind = parentkind
            # NOTE: single-inheritance only.
        self.fields = Env(outer=parentkind.fields)
            # map from string->Entry.  Holds default values of fields.

        # The core: the named virtual-method table (nvtable)
        self.methods = Env(outer=parentkind.methods)
            # map from string->list.  Keys are method names, or
            # _PVN_NONAME for anonymous.  Values are lists of
            # (pattern, callable, retval_type, flags) tuples to be tried in
            # order starting from 0.  _retval_type_ is the return type,
            # or FTNone for a procedure with no retval.  flags are FKMethFlags.
            # A flags of FlexReturn means that the retval is
            # the same as the first type parameter in the call
            # (flex retvals).  TODO IMPLEMENT THIS.
            # A flags of OptionalFlexReturn means that the retval is the
            # first type parm in the call, if any, otherwise retval_type.
            # _pattern_ is a pattern string [TODO -> compiled regexp].
            # The arguments to the callable are me, the given parameters,
            # including the key, in frpl form,
            # and the environment of execution.
            # So, e.g., a[pl,2] maps to a function:
            # def doAdd(me, frpl-val invocation, env)
            # where me is a, invocation[0]==K('pl'), and invocation[1]==K(2).
            # This would be looked up in self.methods.find('pl')['pl'],
            # which would be, e.g.,
            # [ ("`number'", doAdd, fty.FTNumber)]
            # In the pattern, the types are str(FTBase), hence ` and '.
            # Elements are separated by commas.  E.g., foo[bar,1,1.5] is
            # "`number',`real'" in foo.methods.find('bar').
            #
            # To resolve a method, loop over the list of (p,c,r) pairs for
            # the appropriate method name, or _PVN_NONAME.  Invoke c for the
            # first pair for which the pattern matches expression p.
            # p can be a regex, so, e.g., r"(`integer',?)+" matches any
            # number of integers.  p is wrapped in ^ and $ before matching.
            # If no pattern matches, or if the first arg is not a string,
            # look in methods.find[_PVN_NONAME].
            # E.g., for an array, a[5] is a noname call since   5   is not
            # a string.  methods.find(_PVN_NONAME) thus includes
            # [ ..., ("`number'",doGetIndex,retty), ...].

        self.trampoline_targets=[]
            # each item is (name, index in methods[name]).
            # Used by implement_method().
    # end __init__

    def __str__(self):
        return 'K%s/%d'%(self.name, id(self))
    def __repr__(self):
        return str(self)

    # - - - - - - - - -
    # Field declarations

    def declare_field(self, name:str, ty:fty.FTBase, pvDefault=None):
        self.fields.add(name, Entry(ty, pvDefault))

    # - - - - - - - - -
    # Method creation helpers

    # A special flag to indicate functions that can take any arguments
    # TODO replace this with something cleaner.
    AnyArgs=fty.FieldBundle('anyargs')

    ### Helpers for creating arg lists for declare_*_method.  These
    ### must be kept in sync with the implementations of fty.FTBase.__str__
    ### and FANode.ipattern().

    # TODO make all these return simple string-holder objects that have
    # an ipattern() method so everything is unified.

    @staticmethod
    def WildcardType():
        """ Hack, but hey... This function returns a pattern that will match
            a type.  You can use the retval
            as the argument to AtLeastOne() or other functions.
            Its name is a regex that matches the name of any type.
            As a result, this function is HIGHLY COUPLED to the implementation
            of FTBase.ipattern().
        """
        return _FKIPattern("`[^']*'")
    # end WildcardType

    @staticmethod
    def WildcardFlag(options_permitted=False):
        """ Hack similar to WildcardType().
            Highly coupled to FAFlag.ipattern(). """
        return _FKIPattern(
            r'<[^/]+/' +
            r'([^/]+)?' if options_permitted else '' +
            r'>')

    @staticmethod
    def AtLeastOne(tyOrFlag, wants=False) -> str:
        """ Returns a pattern a la inv2pat for any number of _ty_ parms. """
        pat = tyOrFlag.ipattern(wants)
        return r"%s(,%s)*"%(pat,pat)

    @staticmethod
    def AtLeastOneStr(s:str) -> str:
        """ Returns a pattern a la inv2pat for any number of _ty_ parms. """
        return r"(%s)(,%s)*"%(s,s)

    @staticmethod
    def EitherOr(tf1, tf2, wants=False) -> str:
        pat1 = tf1.ipattern(wants)
        pat2 = tf2.ipattern(wants)
        return r'((%s)|(%s))'%(pat1,pat2)

    @staticmethod
    def OptionalArg(tyOrFlag, wants=False) -> str:
        return r"(%s)?"%tyOrFlag.ipattern(wants)

    @staticmethod
    def SpecificNumberOf(tyOrFlag, amin:int, amax:int, wants=False) -> str:
        assert amin>=1
        assert amax>=amin
        pat = tyOrFlag.ipattern(wants)
        return "%s(,%s){%d,%d}"%(pat, pat, amin-1, amax-1)

    @staticmethod
    def Flag(pvFlagname:str, wants=False):
        return FAFlag.ipat(pvFlagname, None, wants)

    @staticmethod
    def FlagWithArg(pvFlagname:str, argty, wants=False):
        return FAFlag.ipat(pvFlagname, argty, wants)

    # Helpers to manipulate the pattern strings returned by the above helpers

    @staticmethod
    def Optional(pat:str) -> str:
        return r"(%s)?"%pat

    @staticmethod
    def PatThenOptional(base:str, opt:str) -> str:
        return r"%s(,%s)?"%(base,opt)

    @staticmethod
    def Sequence(pats:[str]) -> str:
        return ','.join(pats)

    # Support routines for recursion ------------------------------

    @staticmethod
    def ForwardFunction(me, invocation, env, retty):
        """ The body of a function declared but not yet defined,
            such as a function within its own body. """
        raise RuntimeError('Attempt to run function body not yet parsed!')

    def _maketrampoline(self, handle):
        def inner(me:FWorker, invocation, env, retty):
            return self.trampoline_targets[handle](me, invocation, env, retty)
        return inner
    # _maketrampoline

    # Method declarations ------------------------------------------

    def _declare_method_internal(self, name, param_tys, fn, retty,
                                 flags=FKMethFlags.DefaultFlags):
        """ Private helper to declare new methods.
            Inputs: name:   string or _PVN_NONAME
                    param_tys: FTBase or list of FTBase - types of the parms.
                               None - no parms
                               AnyArgs - any parms
                               a string - a pattern per inv2pat
                        NOTE: param_tys does NOT include the method name,
                                for named methods.
                    fn:     the callable.  Parms (me, invocation, env, retty).
                            If this is a forward declaration, so the callable
                            is not yet available, pass ForwardFunction and
                            stash the retval.
                    retty:  The FTBase of the return value, or None as a
                            shortcut for fty.FTNone.
                    flags:  optional FKMethFlags values (default FixedReturn)
            Returns: If fn is ForwardFunction, a handle usable in
                        calls to implement_method().
                     Otherwise, None.
        """
        if isinstance(param_tys, str):
            pat = param_tys
        elif param_tys is FKind.AnyArgs:
            pat = _FVARGS_ACCEPT_ANY
            # This is correct whether the method is named or not.
            # If the method is named, the name is excluded from the pattern,
            # so ANY matches any args after the name.
        elif param_tys is None:
            pat=''
        else:
            if not isinstance(param_tys, (list,tuple)):
                # regularize single parameter.
                # TODO is there a better way to check for an iterable?
                param_tys = [param_tys]

            # Synthesize an invocation with the given types.
            # We only do this once, so it's worth adapting this to use the
            # code for fvInvocations rather than adapting the often-run inv2pat.
            param_nodes=[FANop(ty=x) for x in param_tys]
            pat = FKind.inv2pat(param_nodes, hasname=False, wants=True)
                # hasname=False because the method name is never in param_tys.
                # wants=True because anything that can match the specified
                # invocation is acceptable.
        #endif AnyArgs else

        if fn is FKind.ForwardFunction:
            # Forward declarations: the callable is a trampoline.
            self.trampoline_targets.append(FKind.ForwardFunction)
            # For now, the handle is the row offset in trampoline_targets.  Since we can
            # never undeclare methods with the current code, this works.
            retv = len(self.trampoline_targets)-1
            newfn = self._maketrampoline(retv)
        else:   # Normal case: the provided callable is ready to go
            retv = None
            newfn = fn
        #endif forward else

        r = re.compile('^'+pat+'$')
        newrow = (r, newfn, retty or fty.FTNone, flags)   # Make the new row

        # Save the new row
        if name in self.methods:
            self.methods[name].append(newrow)   # add a definition
        else:
            self.methods.add(name, [newrow])    # create first definition
        #endif

        return retv
    # _declare_method_internal()

    def implement_method(self, handle, new_fn):
        """ Supply the (or a new) implementation function for method _name_.
            This is primarily for use in member-function definitions.
            _handle_ is the return value from _declare_method_internal()
            (or any of the declare_*() functions).
            _new_fn_ is a callable just like for _declare_method_internal.
        """
        #pdb.set_trace()
        assert handle >= 0
        assert handle < len(self.trampoline_targets)
        assert new_fn is not None

        self.trampoline_targets[handle] = new_fn
    # implement_method()

    def declare_named_method(self, name:str, param_tys:list, fn,
                             retty=fty.FTNone, flags=FKMethFlags.DefaultFlags):
        """ Declare a named method.
            Inputs: name:   The method name
                    param_tys: per _declare_method_internal
                    fn:     A callable with parms (me, fvInvocation, env, retty)
                    retty:  an FTBase of the return type
        """
        assert name is not _PVN_NONAME      # sanity check
        return self._declare_method_internal(name, param_tys, fn, retty, flags)

    def declare_ctor(self, param_tys:list, fn):
        """ Special-purpose constructor declarator.  Ctors have no retval.
            An Agency's [rez] has a retval, namely the new Worker, but the
            ctor is only called internally so doesn't return anything.
            This is similar to C++.
        """
        return self._declare_method_internal(_PVN_CTOR,
                                                param_tys, fn, fty.FTNone)

    def declare_dtor(self, param_tys:list, fn):
        """ Declare a destructor.  Args as declare_ctor."""
        return self._declare_method_internal(_PVN_DTOR,
                                                param_tys, fn, fty.FTNone)

    def declare_mutator(self, param_tys:list, fn):
        """ Declare a mutate-in-place routine.  Args as declare_ctor."""
        return self._declare_method_internal(_PVN_MUTATOR,
                                                param_tys, fn, fty.FTNone)

    def declare_noname_method(self, param_tys, fn, retty=fty.FTNone,
                                flags=FKMethFlags.DefaultFlags):
        """ Declare a no-name method.
            Inputs: param_tys: per _declare_method_internal
                    fn:     A callable with parms (me, fvInvocation, env, retty)
                    retty:  an FTBase of the return type
        """
        return self._declare_method_internal(_PVN_NONAME,
                                                param_tys, fn, retty, flags)

    # - - - - - - - - -
    # Method invocation

    # Helpers

    @staticmethod
    def inv2pat(fvInvocation:list, hasname:bool, wants=False):
        """ Return the string pattern for an invocation.
            Inputs: fvInvocation: a list of all the FANodes in the invocation
                    hasname:      True if the invocation includes a method name
                    wants:        True if wants patterns should be used.
            Output: a string pattern of comma-separated elements, each
                    either a type (`foo') or an option ('<bar/>').
                    NOTE: The method name is not included in the pattern.
        """
        firstarg = 1 if hasname else 0      # skip the method name
            # for noname calls, the first arg is part of the pattern.
        descr = ','.join([arg.ipattern(wants)
                            for arg in fvInvocation[firstarg:]])
        return descr
    #end inv2pat()

    @staticmethod
    def get_first_type_parm(fvInvocation:list, hasname:bool):
        """ If fvInvocation includes at least one type parameter, returns
            the first.  Otherwise, returns None.
            Used for flex return types.
        """
        #pdb.set_trace()
        firstarg = 1 if hasname else 0      # skip the method name
            # for noname calls, the first arg is part of the pattern.
        for arg in fvInvocation[firstarg:]:
            if arg.ty is fty.FTType:
                return arg  # e.g., an FAReadVar.
        # next arg
        return None
    # end get_first_type_parm()

    def resolve_type_reference(typeref, env):
        """ Returns an fty.FTBase instance referred to in typeref,
            or None on failure.  Looks in _env_ if necessary. """

        retval = None

        # Check the things we know how to resolve.
        if isinstance(typeref, fty.FTBase):
            retval = typeref
        if isinstance(typeref, FAReadVar):
            # find the referenced type
            rtenv = env.find(typeref.pvName)
            if rtenv is not None:
                retval = rtenv[typeref.pvName].pyval
        elif isinstance(typeref, FAConst):
            retval = typeref.pyval
        # endif a case we can handle

        return retval
    # end resolve_type_reference

    # Function-call resolver

    def _resolve_by_name(self, methname, fvInvocation, env):
        """ Find a match (if any) in self's methods for method _methname_
            with invocation fvInvocation, including the method name if present.
            Returns the match (fn, ret_ty) if found, else None.
            Fills in flex return types if necessary.
        """

        #pdb.set_trace()
        # Make sure we have some environment to start with
        themethod_env = self.methods.find(methname)
        if themethod_env is None:
            return None
        #pdb.set_trace()
        candidates = themethod_env[methname]

        # build the descriptor
        hasname = methname is not _PVN_NONAME
            # for any named method, the method name is in the invocation
        descr = FKind.inv2pat(fvInvocation, hasname, wants=False)
            # the fvInvocation is _gives_ --- it is what we actually have.

        # Find a matching method
        for c in candidates:
            #pdb.set_trace()
            r,fn,ret_ty,flags = c
            if r.match(descr) is not None:      #found a match
                if (flags & FKMethFlags.FlexReturn)==0:
                    return (fn,ret_ty)  # We have everything we need
                else:
                    # Resolve the flex return type
                    #pdb.set_trace()
                    typeparm = FKind.get_first_type_parm(
                                                fvInvocation, hasname)
                    new_ret_ty = FKind.resolve_type_reference(
                                    typeparm, env)

                    if new_ret_ty is None:  # check for defaults
                        if (flags & FKMethFlags.HasDefaultRetty) != 0:
                            new_ret_ty = ret_ty # use the default
                        else:
                            raise FrplError('No type parameter in '+
                                'call %s on instance %s'%(str(fvInvocation),
                                                            str(self)))
                        #endif has default retty else

                    #endif no retty specified else
                    return (fn,new_ret_ty)
                # endif flex return else

            #endif found a match

        # If we got here, we did not find a match.
        return None
    # end _resolve_by_name

    def resolve_invocation(self, fvInvocation:list, env):
        """ Find the method of _self_ for _fvInvocation_, if any.
            Use at parse time or run time.
            Inputs: fvInvocation: a list of FANodes, including any method name.
                    The first parameter, if a string evaluable at compile time,
                    will be tested to see if it is a method name.
                    NOTE: This requires that invocations be non-empty.
                    In frpl, you cannot say foo[].  You have to say
                    foo[default] or something instead.
                    env: the environment.
            Returns (fn, ret_ty) of the function to be invoked, or throws.
        """

        # See if the first parameter is a string, in which case it might
        # be a method name.
        #pdb.set_trace()
        if fvInvocation[0].ty is not fty.FTString:
            pv_methname = _PVN_NONAME   # only strings can be method names
        else:   # string - might be a method name.
            try:    # get the method name.  Will throw if any FANode tries to
                pv_methname = fvInvocation[0].eval(Env())      # access vars.
                    # NOTE: Use an empty Env() for now, rather than None.
                    # This should result in more sensible error messages.
                    # Plus, FANode.eval() requires an environment be passed
                    # rather than None.
                    # In the future, use an Env for a scope of constants,
                    # where the global-var scope has .outer=env_of_constants.
                    # TODO implement this.
                pv_methname = pv_methname.strip().lower()   # just in case
            except: # we couldn't evaluate it as a string at compile time,
                pv_methname = _PVN_NONAME    # so assume it's a parameter.
            # end except
        # endif non-string arg0 else

        # Look for the method
        for phaseidx in (1,2):  # 1: try pv_methname; 2: try NONAME
            methname = pv_methname if phaseidx==1 else _PVN_NONAME

            matchrec = self._resolve_by_name(methname, fvInvocation, env)

            if matchrec is not None:    # found a match
                return matchrec  # EXIT POINT FOR SUCCESS

            if methname is _PVN_NONAME:
                break   # nothing left to try
            # otherwise loop around
        # next phaseidx

        # If we got here, we could not find any appropriate method impl.
        raise FrplError('Could not find function for '+
            'call %s on instance %s'%(str(fvInvocation),str(self)))
    #end resolve_invocation

#end class FKind

# -------------------------------------------------------------------
# --- Worker: frpl's equivalent of instances ---

class FWorker(object):
    def __init__(self, acls:FKind):
        assert acls is not _PIKRoot # design rule: no instances of _PIKRoot

        self.fkind = acls
        self.name = acls.name
        self.ty = acls.worker_ty

        # init instance vars by walking the inheritance chain, grabbing
        # the default values for each FKind as we go.
        self.fvVars = {}    # string->Entry.  NOTE: Not an Env();
        c = acls            # each Worker holds a full set of instance data.
        parents=[]      # Stack up the parents in the inheritance tree.
        while c is not None:
            parents.append(c)   # root at end
            c = c.parentkind
        # Assign starting from parent and working down so that child classes
        # can override assignments in parent classes.
        for c in reversed(parents):   # root down to leaf of inheritance graph.
            for (k,v) in c.fields.items():
                self.fvVars[k]=v
        # end for c in parents
    #end FWorker.__init__

    def __str__(self):
        return '*%s(%s)'%(str(self.ty),self.fvVars)
            # "*" was an arbitrary choice of a visible flag.
    def __repr__(self):
        return str(self)

    def new_entry(self):
        """ Convenience method to create an Entry suitable for adding
            to an ast.Env."""
        return Entry(self.ty, self)

    def invoke(self, fvInvocation:list, env:Env):
        """ Resolve and invoke a function.  A helper for invoke_resolved. """
        fn,ty = self.fkind.resolve_invocation(fvInvocation, env)   #(fn,ret_ty)
        return fn(self, fvInvocation, env, ty)
        #return self.invoke_resolved(ivk, fvInvocation, env)

    def getvar(self, pvName:str) -> Entry:
        try:
            return self.fvVars[pvName]
        except KeyError as e:
            raise FrplError('Attempt to access nonexistent field '+
                                pvName) from e
    #end getvar

    def setvar(self, pvName:str, value:Entry):
        """ Mutate an instance.  This is only used in constructors since
            constructed instances in frpl are immutable. """
        if pvName not in self.fvVars:
            raise KeyError('Attempt to assign to nonexistent field '+pvName)
        self.fvVars[pvName]=value
    #end setvar

    def destroy(self, env):
        """ Clean up an object.  DO NOT call invoke() after this runs. """
        self.invoke([_FVN_DTOR],env)  # frpl dtor
        del self.fvVars               # free references to other objects
        self.fkind = None          # in hopes of helping the GC.
    # end destroy

#end class FWorker

# Helper function to create a worker.
# TODO?  Make this a member function of KAgency?  It is currently
# used in KAgency and in frplstdlib:_vec_but().  Adding a map
# from Kinds to Agencies or changing _vec_but to look up its own
# kind in the environment would permit moving this inside KAgency
# without introducing a circular dependency between FKind instances
# and KAgency instances.
def MakeWorkerOfKind(kind:FKind, fvArgs=[], env=Env()):
    """ Makes a new FWorker of this FKind.  Only invoke at runtime.
        Called by KAgency[rez].
        Inputs: kind: an FKind instance that is the kind of the new worker
                fvArgs: optional list of frpl values for args to the
                        constructor call.
                        E.g., in Vehicle[rez,42], fvArgs is [42].
                env:    optional environment in which to run the constructor
    """
    # Build the Python object
    inst = FWorker(kind) # kind is the FKind instance.
        #inst is the new FWorker instance.

    # Now call the frpl constructor with any provided args.
    #pdb.set_trace()
    inst.invoke((_FVN_CTOR,)+tuple(fvArgs), env)

    return inst
# end MakeWorkerOfKind

# -------------------------------------------------------------------
# --- Agency: frpl's version of a class factory ---
# In frpl, each Kind has an Agency that creates new Workers of that Kind.
# Each frpl Kind is a python instance of FKind, holding a reference to an
#   appropriate agency python instance.
# Each agency is an KAgency(->FKind) python instance with a
# strongly-typed rez method that returns the appropriate type of worker.
# Each frpl Worker is a python instance of FWorker, holding a reference
#   to the appropriate python FKind instance.
#   Each FWorker is callable and can be exposed to the frpl program.
# Each frpl Agency is a python instance of FWorker, holding a reference to
#   the corresponding KAgency python instance.
#   When [rez] is invoked on the worker, it delegates to the KAgency instance
#   to create the approprate FWorker instance for the Kind.

class KAgency(FKind):
    """ The Kind of the class factory for the Kind passed to the
        constructor.  An FWorker instance referencing a KAgency
        instance is a frpl class factory.
    """
    def __init__(self,kind):
        super().__init__(kind.name+'Agency')   # RootKind->KAgency
        self.agency_for_kind = kind

        def _KAgency_rez(me, fvInvocation, env, retty):
            """ Kind-specific creation function"""
            # This function is loaded into the methods table under _PVN_REZ.
            # Therefore,
            assert fvInvocation[0].eval(env).lower() == _PVN_REZ  # sanity check
            # As a result, the only required invocation parsing is to
            # chop the 'rez'.  All other parameters go straight through
            # to the constructor of agency_for_kind
            return MakeWorkerOfKind(kind, fvInvocation[1:], env)
                # [1:] works regardless of the length of the list,
                # and _invocation_ is always a list at this point per
                # FKind.invoke().
        # end inner _KAgency_rez

        self.declare_named_method(_PVN_REZ, FKind.AnyArgs, _KAgency_rez,
                                               kind.worker_ty)
                        # Specific retval type ^^^^^^^^^^^^^^
                        # This is why we need a different KAgency instance
                        # for each Kind.  Otherwise, the return value for
                        # any rez would be a generic RootKind, and
                        # typechecking wouldn't be available.
    # end __init__
# end class KAgency

# -------------------------------------------------------------------
# --- Instantiation code --- tie it all together ---

def MakeKindAndAgency(name:str, parentkind=_PIKRoot):
    """ Makes a new FKind Python instance and a corresponding KAgency
        python instance.
        Returns (pviKind, pviAgency, entry),
        where _entry_ is an entry for the Agency to be inserted in
        a frpl environment.
    """
    #pdb.set_trace()
    pviKind = FKind(name, parentkind)       # Declare the class
        # Note: the frpl code will never see pviKind directly
    pviAgency = KAgency(pviKind)            # Declare the class factory
    pviAgencyWorker = MakeWorkerOfKind(pviAgency)
        # Instantiate the class factory.  No args or environment required.

    # Declare the class factory instance to frpl code
    newentry = Entry(pviAgency.worker_ty, pviAgencyWorker,
        decltype=pviKind.worker_ty)

    return (pviKind, pviAgency, newentry)
# end MakeKindAndAgency

# Helper
def MakeKindInEnv(name:str, env:Env, parentkind=_PIKRoot):
    """ Convenience function to make a Kind and load its Agency into _env_.
        Returns (FKind instance, FAgency instance) so you can
        add fields or methods.
    """
    (kind, agency_kind, agency_entry) = MakeKindAndAgency(name, parentkind)
    env.add(name, agency_entry)
    return (kind, agency_entry.pyval)
# end MakeKindInEnv

##########################################################################
# Back to the AST

#====================================================================
# Expressions

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Calls

class FAApply(FANode):
    """ Message send/array index/function application all in one"""
    def __init__(self, fvTarget, fvInvocation:tuple, env:Env,
                    s=_DEF_SSTR, l=_DEF_SLOC):
        """ Create an Apply node.
            Inputs: fvTarget: the FANode that should handle the invocation
                    fvInvocation: the frpl arguments provided
                    env: an environment in which fvTarget makes sense.
        """
        super().__init__(fty.FTNone, s,l)   # .ty filled in below
        self.fvTarget, self.fvInvocation = fvTarget, fvInvocation

        # Is the target callable?
        if not fvTarget.ty.does_implement(fty.FCCallable):
            raise ParseFatalException(s,l,
                'Attempt to invoke %s on non-callable %s'%(
                str(fvInvocation), str(fvTarget.ty)))
        #endif

        # Get the kind
        #pdb.set_trace()
        pvKind = fvTarget.ty.handle
        if not isinstance(pvKind, FKind):
            raise ParseFatalException(s,l,
                'Attempt to invoke %s on %s, which is not a Kind'%(
                str(fvInvocation), str(pvKind)))
        #endif

        try:
            self.fn, self.ty = pvKind.resolve_invocation(fvInvocation, env)
            # save the retval type of the invocation
        except FrplError as e:  # resolve_invocation may throw FrplError.
            # resolve doesn't throw ParseFatalException since it doesn't
            # have s or l.
            raise ParseFatalException(s,l,
                'Could not find function for call %s on %s'%(
                    str(fvInvocation), str(fvTarget))) from e
        # end try

    # end __init__

    def __str__(self):
        return "(%s|call %s with %s)" % (str(self.ty), str(self.fvTarget),
                                            str(self.fvInvocation))
    def _eval(self,env):
        #pdb.set_trace()
        pvTarget = self.fvTarget.eval(env)  # get the instance

        if pvTarget is None:
            #pdb.set_trace()
            # TODO should this be a RuntimeError?  Maybe a new subclass of
            # RuntimeError that reports s and l.
            raise ParseFatalException(self.source_string,self.source_loc,
                'Attempt to access uninitialized instance %s'%
                    str(self.fvTarget))

        #return pvTarget.invoke_resolved(self.ivk, self.fvInvocation, env)
        return self.fn(pvTarget, self.fvInvocation, env, self.ty)
            # Notes:
            # 1. Entries in the method table take the python value of the
            # instance as the _me_ argument.  Since instances are immutable,
            # methods don't need a way to reach back to the Entry from
            # which the pyval was pulled.
            # 2. self.ty is passed for flex-return functions.
            # 3. TODO? add a pytype field to _FTBase so you can get the pytype
            # (native Python type) of a frpl type.
#end class FAApply

class FAList(FANode):
    """ List of items.  Used to hold arguments that will be unwrapped
        into an FAApply.
    """

    def __init__(self,env, msg:list, s=_DEF_SSTR, l=_DEF_SLOC):
        """ Initialize.  Inputs are:
            env     The environment at the point of definition
            msg     The message
            s, l    Location of the call
        """
        super().__init__(fty.FTNone, s,l)

        if len(msg)<1:
            raise ParseFatalException(s,l,"Can't parse zero-element list!")
            # This shouldn't happen since FAList instances only come
            # from the comma operator.

        self.whole_msg = msg
            # for use by frplparse._ParseApply in converting these
            # to FAApply nodes.

    # end __init__

    def __str__(self):
        return "(|list %s)" % str(self.whole_msg)

    def _eval(self,env):
        raise FrplError(
            ("Attempt to eval comma list %s (at %s).  >>>>> Did you forget "+
             'to put the name of the recipient before the "["?')%(
                                    str(self), self._loc()))

    # end _eval()

#end class FAList

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Names and variables

class FAIdent(FANode):
    """ References to identifiers. """
    def __init__(self, pvName:str, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(fty.FTIdentifier, s,l)
        #pdb.set_trace()
        self.pvName = pvName
    def __str__(self): return '(?|id %s)'%(str(self.pvName))

    def _eval(self,env):
        # TODO figure out if this is right
        theenv = env.find(self.pvName)
        return None if theenv is None else theenv[self.pvName]
# end class FAIdent

class FAReadVar(FANode):
    """ Reads of variables. """

    NO_ENV = object()   # a special flag for indentifiers that should
                        # not be checked in an environment

    def __init__(self, ty, pvName:str, env:Env, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(ty, s,l)
        self.pvName = str(pvName)

        if env is not FAReadVar.NO_ENV:
            theenv = env.find(self.pvName)
            if theenv is None:
                # The identifier was not found
                raise ParseFatalException(s,l,
                    ("I don't recognize '%s'.  "
                    "Did you declare it with 'names' or 'nm'?")%
                    self.pvName
                )
            #endif not found
        #endif not NO_ENV

    def __str__(self):
        return ( '(%s|read %s)'%(str(self.ty), self.pvName) )

    def _eval(self,env):
        theenv = env.find(self.pvName)
        if theenv is None:
            # The identifier was not found - this shouldn't happen,
            # since calling code should not eval an FAReadVar
            # if env was NO_ENV in the ctor call.
            raise FrplError(
                "### Couldn't find environment for '%s' (at %s).  "%
                (self.pvName, self._loc())
            )
        #endif

        #try:    #DEBUG
        retv = None if theenv is None else theenv[self.pvName].pyval
        #except Exception as e:
        #    pdb.set_trace()
        #    raise
        ##end try

            #TODO? Throw if nonexistent?
            # return the python value from the entry.
        return retv
# end class FAReadVar

class FAWriteVar(FANode):
    """ Write references to variables.  Note that actual assignment
        is handled by the = (remembers) operator, not by this class."""
    def __init__(self, ty, pvName, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(ty, s,l)
        self.pvName = pvName
    def __str__(self):
        return '(%s|write %s)'%(str(self.ty), str(self.pvName))
    def _eval(self, env):
        theenv = env.find(self.pvName)
        return None if theenv is None else theenv[pvName]
# end class FAWriteVar

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Operators

class FAArithExp(FANode):

    # Operations and functions to compute them
    _optable = {'+':lambda l, r:l+r,
                'pl':lambda l, r:l+r,
                '-':lambda l,r: l-r,
                'mi':lambda l,r: l-r,
                '*':lambda l,r: l*r,
                'ti':lambda l,r: l*r,
                '/':lambda l,r: l/r,
                'di':lambda l,r: l/r,
                'mod':lambda l,r: l%r,
                'mo':lambda l,r: l%r,
                'tothe': lambda l,r: l**r
               }

    """Left-associative arithmetic operators"""
    def __init__(self,ty,elems:tuple, s=_DEF_SSTR, l=_DEF_SLOC):
        """elems should be a non-empty source-code order tuple of operand,
            operator, operand, ..., in which all the operators have
            equal precedence."""
        super().__init__(ty, s,l)
        self.elems = elems
    def __str__(self):
        return '(%s|arith %s)' % (str(self.ty), str(self.elems))

    def _eval(self,env):
        # TODO change this to invoke methods on the values.
        #pdb.set_trace()
        theval = self.elems[0].eval(env)
        for opidx in range(1,len(self.elems),2):
            the_operator = self.elems[opidx]
            the_operand = self.elems[opidx+1].eval(env)
            #pdb.set_trace()
            theval = FAArithExp._optable[the_operator](theval, the_operand)
        return theval
# end class FAArithExp

# TODO? Refactor to put common code between Rel and Logical one place?
class FARelExp(FANode):
    _allowable_ops = "< > <= >= <> lt gt eq le ge ne".split()
    _optable = {'<':lambda a,b: a<b,
                '>':lambda a,b: a>b,
                '<=':lambda a,b: a<=b,
                '>=':lambda a,b: a>=b,
                '<>':lambda a,b: a!=b,
                'lt':lambda a,b: a<b,
                'gt':lambda a,b: a>b,
                'eq':lambda a,b: a==b,
                'le':lambda a,b: a<=b,
                'ge':lambda a,b: a>=b,
                'ne':lambda a,b: a!=b}

    def __init__(self,ty, elems:tuple, s=_DEF_SSTR, l=_DEF_SLOC):
        """_ty_ is ignored.  _elems_ should be a three-element, source-code
            order tuple of operand, operator, operand."""
        super().__init__(fty.FTBoolean, s,l)
        #pdb.set_trace()
        if len(elems)!=3:
            #pdb.set_trace()
            raise ParseFatalException(s,l,
                'Relational operator must have exactly two args')

        if elems[1] not in FARelExp._allowable_ops:
            raise ParseFatalException(s,l,
                "I don't understand relop "+str(elems[1]))

        self.op = elems[1]
        self.params=elems[::2]

    def __str__(self):
        return '(%s|rel %s %s %s)' % (str(self.ty), str(self.params[0]),
                                        str(self.op),str(self.params[1]))

    def _eval(self,env):
        vals=[]
        for idx in range(2):
            vals.append(self.params[idx].eval(env))
        #try: #DEBUG
        retv= FARelExp._optable[self.op](vals[0],vals[1])
        #except Exception as e:
        #    pdb.set_trace()
        #    raise
        return retv
# end class FARelExp

class FALogicalExp(FANode):
    _allowable_ops = "and or xor nand nor xnor not".split()
        # Treat "not" as if it were binary for the sake of uniformity
    _optable = {'and':lambda a,b: a and b,
                'or':lambda a,b: a or b,
                'xor':lambda a,b: (a and (not b)) or ((not a) and b),
                'nand':lambda a,b: not (a and b),
                'nor':lambda a,b: not (a or b),
                'xnor':lambda a,b: not( (a and (not b)) or ((not a) and b) ),
                'not':lambda a,b: not b     # ignores A
               }
    def __init__(self,ty,elems:tuple, s=_DEF_SSTR, l=_DEF_SLOC):
        """elems should be a source-code order string of operand, operator,
            operand, ..., in which all the operators have equal precedence.
            _ty_ is ignored."""
        super().__init__(fty.FTBoolean, s,l)
        if len(elems)!=3:   # TODO relax this requirement
            raise ParseFatalException(s,l,
                'Logical operator must have exactly two args')
        if elems[1] not in FALogicalExp._allowable_ops:
            raise ParseFatalException(s,l,
                "I don't understand logical operator "+str(elems[1]))
        self.op = elems[1]
        self.params=elems[::2]

    def __str__(self):
        return '(%s|logical %s%s%s)' % (str(self.ty), str(self.params[0]),
                                        str(self.op),str(self.params[1]))

    def _eval(self,env):
        vals=[]
        for idx in range(2):
            vals.append(self.params[idx].eval(env))
        return FALogicalExp._optable[self.op](vals[0],vals[1])
# end class FALogicalExp

# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Miscellaneous

class FARange(FANode):
    """ Ranges a to b [by c].  TODO figure out of the type of this should be
        something other than the base type."""
    def __init__(self,ty, fromexpr:FANode, toexpr:FANode,
                    isup:bool, strideexpr:FANode,
                    s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(ty, s,l)
        #self.elems = elems
        self.first = fromexpr       # public data
        self.last = toexpr
        self.up = isup
        self.stride_amount = strideexpr

    def __str__(self):
        return '(%s|range %s %s %s by %s)' % (str(self.ty),
            str(self.first), 'to' if self.up else 'downto',
            str(self.last), str(self.stride_amount))

    def _eval(self,env):
        raise FrplError('Attempted to eval range %s at %s'%(
            str(self), self._loc() ) )

# end class FARange

class FAReassignment(FANode):
    """ elem X gets new value Y.  TODO figure out of the type of this should be
        something other than the base type."""
    def __init__(self, destidx, newval, s=_DEF_SSTR, l=_DEF_SLOC):
        """elems should be a source-code order string of operand, operator,
            operand, ..., in which all the operators have equal precedence."""
        super().__init__(fty.FTGets, s,l)
        #pdb.set_trace()
        #self.elems = elems
        self.destidx, self.newval = destidx,newval

    def __str__(self):
        return '(%s|[%s] gets %s)' % (str(self.ty),
            str(self.destidx),  str(self.newval))

    def _eval(self,env):
        raise FrplError('Attempted to eval reassignment %s at %s'%(
            str(self), self._loc()) )
# end class FAReassignment

class FABut(FANode):
    """ Build a new array (or whatever) based on an old one, with
        reassignments applied."""
        #TODO remove this and replace it with an Apply of the function 'but'
    def __init__(self, base:FAReadVar, mods:tuple,
                        s=_DEF_SSTR, l=_DEF_SLOC):
        """elems should be a source-code order string of operand, operator,
            operand, ..., in which all the operators have equal precedence."""
        super().__init__(fty.FTNone, s,l)
        #pdb.set_trace()
        self.base, self.mods = base,mods

    def __str__(self):
        return '(.|%s but (%s))' % (
            str(self.base),  str(self.mods))

    def _eval(self,env):
        #pdb.set_trace()

        orig = self.base.eval(env)
        if orig is None:    # HACK - TODO remove this once vars are initialized
            orig=[]         # to sensible values and not None.

        newarr = orig[:]  # deep copy - don't trash the original
        for mod in self.mods:
            idx = mod.destidx.eval(env)
            if idx is None:
                raise ParseFatalException(self.source_string, self.source_loc,
                    'Cannot access index %s - is it initialized?'%
                    str(mod.destidx))
            if idx>=len(newarr):    # zero pad so it fits
                newarr=newarr+[0]*(idx-len(newarr)+1)
            newarr[idx] = mod.newval.eval(env)
        return newarr
# end class FABut

class FAExpr(FANode):
    """Represents a complete expression"""
    def __init__(self,ty,expr, s=_DEF_SSTR, l=_DEF_SLOC):
        """expr is a postfix list of operations to be performed.
            This saves on recursion depth, although it does make
            common-subexpression elimination more difficult."""
        super().__init__(ty, s,l)
        self.expr = expr[0] if isinstance(expr,ParseResults) else expr
            # TODO FIXME ^^^ The above is hacky - fix once the
            # parse actions in _FExpr are filled in properly.

    def __str__(self):
        return '(%s|expr %s)' % (str(self.ty), str(self.expr))

    def _eval(self,env):
        return self.expr.eval(env)
            # Pass to the encapsulated expression.
# end class FAExpr

class FATypecast(FANode):
    """Typecasts"""
    def __init__(self,totype, expr,converter, s=_DEF_SSTR, l=_DEF_SLOC):
        """expr is an AST node; converter is a one-arg lambda
            that does the conversion."""
        super().__init__(totype, s,l)
        self.expr,self.converter=expr,converter

    def __str__(self):
        return '(%s|promote %s from %s)' % (
            str(self.ty), str(self.expr), str(self.expr.ty))

    def _eval(self,env):
        theval = self.expr.eval(env)
        return self.converter(theval)
# end class FATypecast

#====================================================================
# Statements

class FABlock(FANode):
    """ Represents a nested scope """
    def __init__(self,newenv, child, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(fty.FTNone, s,l, needsframe=child.needsframe)
        self.newenv,self.child = newenv,child

    def __str__(self): return "(.|block %s|in %s)"%(
                                str(self.child),str(self.newenv))

    def _eval(self,env):
        #self.newenv.outer = env    # TODO Do we need this?
        if self.needsframe:
            frame = self.newenv.newframe(env)
                # Make a new dynamic activation record for this invocation
                # of the block.  Permits recursion.
            return self.child.eval(frame)

        else:   # No frame, but there may be dynamic records on the
                # stack above here.  Link the new environment into the
                # stack, but don't copy it.
            #pdb.set_trace()
            frame = self.newenv
            oldouter = frame.outer
            frame.outer = env   # Link it in
            try:
                retv = self.child.eval(frame)
            finally:
                frame.outer = oldouter  # Restore the linkage
            #end try
            return retv
        #endif

#end class FABlock

class FALoopClause(FANode):
    """ Represents a loop clause (condition). """
    def __init__(self, init, increment, pretest=FATrue, posttest=FATrue,
                decl=None, s=_DEF_SSTR, l=_DEF_SLOC):
        """ Initializes.  Each of init, increment, pretest, posttest
            is an AST node.  init and increment are statements.
            Pretest and posttest are expressions returning boolean.
            decl, if not None, is a declaration to hook into the AST
            before the loop for this clause.  This is used when the
            loop statement creates the index variable.
            The loop is:
                init
                A: pretest
                    body
                posttest
                increment
                goto A.
            If either pretest or posttest is not specified, it is true (i.e., the
            loop will not terminate unless a specified pre/posttest returns False.)
        """

        super().__init__(fty.FTNone, s,l)
        self.incr_statement,self.pretest, self.posttest=(
            increment,pretest, posttest)

        # If there was a declaration, stick it before the loop
        if decl is None:
            self.init = init
        else:
            self.init = FACompoundStmt([decl,init],s,l)

        self.needsframe = ( increment.needsframe or
            pretest.needsframe or posttest.needsframe or init.needsframe)
    # end __init__

    def __str__(self): return (
        "(.|clause: init %s|incr %s|pretest %s|posttest %s)"%
        (str(self.init),str(self.incr_statement), str(self.pretest),
            str(self.posttest))
        )

    def _eval(self,env):
        """ eval is not used in loop clauses. Instead, FALoop directly
            evals self.{init,incr,pretest,posttest} as appropriate. """
        # TODO maybe use this to catch 1 downto 2 situations and the like?
        # That check should go somewhere.
        raise FrplError('Attempt to eval loop clause %s at %s'%(
            str(self), self._loc()))

#end class FALoopClause

class FALoop(FANode):
    """ Represents a loop.
            The loop is:
                init
                A: pretest
                    body
                posttest
                increment
                goto A.
    """

    def __init__(self, clause:FALoopClause, body:FANode,
                    s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(fty.FTNone, s,l)
        #pdb.set_trace()
        if not isinstance(clause, FALoopClause):
            raise ParseFatalException(s, l,
                'Attempt to initialize loop with non-clause %s'%clause)

        self.clause,self.body = clause,body
        self.needsframe = ( clause.needsframe or body.needsframe )
        #print(self) #DEBUG

    def __str__(self): return "(.|for %s|do %s)"%(
                                str(self.clause),str(self.body))

    def _eval(self,env):
        """ Represents a loop.
                The loop is:
                    init
                    A: pretest
                        body
                    posttest
                    increment
                    goto A.
        """
        #pdb.set_trace()
        self.clause.init.eval(env)
        while True: # The loop
            # TODO figure out if the "is not FATrue" statements are
            # premature optimization and should be removed
            if ( (self.clause.pretest is not FATrue) and
               (not self.clause.pretest.eval(env)) ):
                break

            try:    # TODO can I do continue with a try outside the While
                self.body.eval(env)     # so we don't have to set up each time?
            except:
                # TODO define and handle exceptions for break and continue
                raise

            if ( (self.clause.posttest is not FATrue) and
               (not self.clause.posttest.eval(env)) ):
                break
            self.clause.incr_statement.eval(env)
        #end loop

        return None     # TODO see if we should return something different
#end class FALoop

class FADecl(FANode):
    """ Represents a declaration """
    def __init__(self, ty, fvIdent:FAIdent, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(ty, s,l, needsframe=True)
        self.fvIdent = fvIdent
        #print('%s is now a %s'%(fvIdent,ty))   # DEBUG
        # TODO add this name+ty to the appropriate environment so it can
        # be used for type checking

    def __str__(self): return "(%s|decl %s)"%(self.ty,self.fvIdent)

    def _eval(self,env):
        # The work was already done in frplparse.py:_ParseDeclaration -
        # nothing more to do.
        # TODO?  Assign default values here?
        return None
#end class FADecl

#class FAAssignmentOld(FANode):
#    def __init__(self,ty,
#                dest_name: str,
#                dest_entry: Entry,
#                exp: FAExpr,
#                s=_DEF_SSTR, l=_DEF_SLOC):
#
#        #pdb.set_trace()
#        # The caller is required to look up the entry in the right
#        # environment, which must exist at parse time.
#        super().__init__(ty, s,l)
#
#        # Sanity check.  TODO replace this with the ensure library.
#        if not isinstance(dest_entry, Entry):
#            raise ParseFatalException(s, l,
#                'Attempt to assign to non-identifier or unknown identifier %s'
#                    % str(dest))
#
#        self.dest_name,self.dest_entry,self.exp = dest_name,dest_entry,exp
#
#        # Check valid types for array assignments.  HACK - TODO generalize.
#        if dest_entry.ty is fty.FTVector:
#            if isinstance(exp, FAArithExp):
#                pass    # OK - will be single-element vector
#            elif isinstance(exp, FAConst):
#                pass    # OK - will be single-element vector
#            elif isinstance(exp, FAList):
#                pass    # OK - will be a modification.
#                # TODO check the types of the elements in the call.
#                # Should not have any reassignments.
#            elif isinstance(exp, FABut):
#                pass    # OK - will be a modification
#            else:
#                raise ParseFatalException(s, l,
#                    ('Attempt to assign non-scalar, non-replacement %s to'+
#                    ' array %s')%(str(exp), str(dest_entry)))
#            #endif checks else
#
#    def __str__(self):
#        return "(%s|assign %s of %s := %s)" % (str(self.ty), self.dest_name,
#                                    self.dest_entry.ty, str(self.exp))
#            # TODO use type of var
#    def _eval(self,env):
#        if self.dest_entry.ty is not fty.FTVector:
#            # scalars
#            self.dest_entry.pyval = self.exp.eval(env)
#        else:
#            # TODO HACK - make more general
#            if (isinstance(self.exp, FAArithExp) or
#                isinstance(self.exp, FAConst)):     #single-element
#                self.dest_entry.pyval = [self.exp.eval(env)]
#            elif isinstance(self.exp, FAList):
#                self.dest_entry.pyval = [self.exp.fname.eval(env)]
#                for v in self.exp.params:
#                    self.dest_entry.pyval += [v.eval(env)]
#            elif isinstance(self.exp, FABut):
#                # FABut.eval returns a new array
#                #pdb.set_trace()
#                self.dest_entry.pyval = self.exp.eval(env)
#        #endif not vector else
#        env.sn = (env.sn+1) % (2**31-1)
#            # Mark _env_ dirty so that memoized exprs will be reevaluated
#        #print("After assignment: "+str(env)) #DEBUG
##end class FAAssignmentOld

class FAAssignment(FANode):
    def __init__(self,dest_ty: fty.FTBase,
                dest_name: str,
                exp: FAExpr,
                s=_DEF_SSTR, l=_DEF_SLOC):

        #pdb.set_trace()
        super().__init__(fty.FTNone, s,l, needsframe=exp.needsframe)

        # TODO? type-check assignment?
        self.dest_ty,self.dest_name,self.exp = dest_ty,dest_name,exp

        # Check whether this is a straight mutation
        self.is_mutator = False     # assume not

# TODO - uncomment this code to try automatically rewriting reassignments
# to mutations.  Has only been tested once.  It appeared to work that one
# time, but YMMV.  Also, needs to be reworked when subroutines and
# other escaping references are added.  Maybe just disable the mutator if
# an escaping-reference flag is set?
#        if (isinstance(exp,FAApply) and             # Check for a straight-
#            exp.fvTarget.pvName == dest_name and    # mutation tree pattern
#            exp.fvInvocation is not None and        # in the AST.
#            len(exp.fvInvocation)>0 and
#            isinstance(exp.fvInvocation[0],FAConst) and
#            exp.fvInvocation[0].ty is fty.FTString and
#            exp.fvInvocation[0].pyval=='but'):
#
#            #pdb.set_trace()
#
#            # See if the Kind has a mutator
#            fvInv = exp.fvInvocation
#            fvInv[0].pyval=_PVN_MUTATOR     # rewrite 'but' to mutate-in-place
#
#            akind = exp.fvTarget.ty.handle
#            try:
#                res = akind.resolve_invocation(fvInv)
#                self.is_mutator, self.mutation_inv = True, fvInv
#                    # TODO? cache res for use in _eval()?
#            except:
#                pass    # not a mutator
#            #end try
#
#        #endif

    #end __init__()

    def __str__(self):
        return "(.|assign %s of %s := %s%s)" % (self.dest_name,
                                    self.dest_ty, str(self.exp),
                                    " (MUTATOR)" if self.is_mutator else "")
            # TODO use type of var
    #end __str__

    def _eval(self,env):
        # TODO uncomment if you want to try the mutator
        #if not self.is_mutator:
        #    self.dest_entry.pyval = self.exp.eval(env)  # normal case
        #else:
        #    #pdb.set_trace()
        #    self.dest_entry.pyval.invoke(self.mutation_inv, env)
        #        # mutate in place.  TODO make sure this still works once
        #        # we have subroutines or Workers holding references
        ##endif

        #pdb.set_trace()
        theenv = env.find(self.dest_name)   # may not be _env_
        if theenv is None:  # sanity check
            raise ParseFatalException(self.source_string,self.source_loc,
                "Couldn't find an environment holding " + self.dest_name)
        #endif

        newpyval = self.exp.eval(env)  # normal case.  Comment out for mutator.
        theenv[self.dest_name].pyval = newpyval

        # Mark _theenv_ dirty so that memoized exprs will be reevaluated.
        # Also mark any subsidiary environments dirty to be on the safe
        # side, since expressions may change based on values in any
        # environments they reference.
        # TODO seriously replace this with something cleaner.
        e = env
        while e is not None:
            e.sn = (e.sn+1) % (2**31-1)
            if e==theenv: break     # We've done all we need to
            e = e.outer
        # next

        #print("After assignment: "+str(env)) #DEBUG
    #end _eval()
#end class FAAssignment

class FAConditional(FANode):
    def __init__(self,
                #condition: FAExpr,
                #true_statements,
                #false_statements,   # must be filled in
                conditions, #list of FAExpr
                actions,    # list of FANode.
                s=_DEF_SSTR, l=_DEF_SLOC):
        """ Init with lists of conditions and corresponding actions.
            The caller must guarantee that at least one of the conditions
            will evaluate to True.  The easiest way to do this is to
            stick FATrue, FANop() at the ends of the lists. """
        super().__init__(fty.FTNone, s,l)

        # Sanity check
        assert len(conditions)==len(actions), \
            (" Internal error: number of conditions different from number" +
             " of actions.\nConds %s\nActions %s"%(conditions,actions))
        # Type check
        for cond in conditions:
            assert cond.ty is fty.FTBoolean, \
                "'if' statements have to test a boolean condition."

        # TODO? type-check actions?

        #self.condition,self.when_true,self.when_false = \
        #    condition,true_statements,false_statements
        self.condacts=list(zip(conditions,actions))

        # TODO set needsframe

    #end __init__

    def __str__(self):
        #return "(.|if %s then %s%s)" % (self.condition,
        #        self.when_true,
        #        (' else %s'%self.when_false
        #            if not isinstance(self.when_false, FANop)
        #            else '')
        #    )
        retval="(.|if:\n"
        for condact in self.condacts:
            retval += "    %s => %s\n"%condact
        retval += ")"
        return retval

    # end __str__

    def _eval(self,env):
        for (cond,act) in self.condacts:
            istrue = cond.eval(env)     # returns a boolean
            if istrue:
                return act.eval(env)
        # next (cond,act)

        # Should never get here, because at least the last condition
        # should be FATrue.
        raise FrplError(
            "Bad conditional at %s - nothing matched:\n%s\n"%(
            self._loc(), self.condacts))
    # end _eval
#end class FAConditional

class FACompoundStmt(FANode):
    def __init__(self,statements, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(fty.FTNone, s,l)
        self.statements = []
        self.needsframe = False

        # Drop NOPs.  TODO adjust the parser so the NOPs don't get created
        # in the first place.
        # NOTE: This can result in self.statements==[] if everything in the
        # chunk is a NOP.
        # While looping, fill in needsframe as well.
        for e in statements:
            if not isinstance(e, FANop):
                self.statements += [e]
                self.needsframe = self.needsframe or e.needsframe
            #endif
        #next e

    def __str__(self):
        return "(.|compound/%d: %s)" % (len(self.statements),
                                    self.statements)
    def _eval(self,env):
        for stmt in self.statements:
            stmt.eval(env)
        return None     # retvals are done explicitly via "ans"
# end class FACompoundStmt

#--------------------------------------------------------------------
# Kinds

class FAFnHdr(FANode):
    """ Declaration and definition of the header of a
        member function of a Kind. """
    def __init__(self, kind, handle, param_names:list, fname=None,
                s=_DEF_SSTR, l=_DEF_SLOC):
        """ Inputs:
                kind: a python FKind instance
                handle: the handle of the function body, returned by
                        FKind.declare_*()
                param_names: a list of the names of the parameters, in
                                order in the invocation.
                fname: if not None, the function body needs to rez a
                        kind.worker_ty instance called fname.
        """
        super().__init__(fty.FTNone, s,l)
        self.kind,self.handle,self.param_names,self.fname= \
            kind,handle,param_names, fname
    # __init__

    def __str__(self):
        #return "(hdr|[%s] nm %s %d)"%(self.argpattern,
        #         str(self.retty), self.flags)
        return "(hdr|in %s noname #%d takes %s (fname %s))"%(
            str(self.kind),self.handle,str(self.param_names), str(self.fname))
    # __str__

    def _eval(self, env):
        #print(str(self))    # DEBUG
        #return              # DEBUG
        raise FrplError(
            'Attempt to eval function header %s (at %s).'%(
            str(self), self._loc()))
    # _eval
# FAFnHdr

class FAMemberFn(FANode):
    """ Declaration and definition of a member function of a Kind.
    """
    def __init__(self, caller_env:Env, hdr:FAFnHdr,
                 body:FANode, s=_DEF_SSTR, l=_DEF_SLOC):
        super().__init__(fty.FTNone, s,l)
        self.caller_env, self.hdr, self.body = caller_env, hdr, body

        # Initialize the singleton instance at the point of declaration,
        # if necessary.
        if self.hdr.fname is None:
            self.define = FANop
        else:
            # Make an assignment tree that will init the singleton
            # worker Kind at the point of declaration.
            #pdb.set_trace()

            # Get info about the agency
            agency_env = caller_env.find(self.hdr.kind.name)
            assert agency_env is not None
            agency_entry = agency_env[self.hdr.kind.name]

            # Access the agency at runtime
            fvagency = FAReadVar(agency_entry.ty, self.hdr.kind.name,
                                 caller_env)
            # Ask the agency to rez the instance
            rez = FAApply(fvagency, [K(_PVN_REZ)], caller_env)
            # Stash the instance
            assgn = FAAssignment(self.hdr.kind.worker_ty, self.hdr.fname, rez)
            self.define = assgn
        #endif defining
    # __init__

    def __str__(self):
        return "(fn|when %s\n  with %s\n  => %s)"%(
                self.hdr, self.newenv, self.body)
    # __str__

    def _eval(self, env):
        return self.define.eval(env)    # just define if necessary.
    # _eval

# FAMemberFn

#====================================================================
# Program
class Program(object):  # NOT an ast node!
    def __init__(self,tree,initialenv):
        self.tree,self.initialenv=tree,initialenv
    # init(tree, initialenv)

    def run(self): # NO ARGS
        self.tree.eval(self.initialenv)

    def __str__(self):
        return "(|program\n\t%s\nin\n\t%s)" % (str(self.tree),
                                                str(self.initialenv))

# end class Program

# END OF CODE

###########################################################################
# Originally created from imp_ast.py.
#
# Copyright notice for imp_ast.py from
# http://www.jayconrod.com/posts/39/
#                           a-simple-interpreter-from-scratch-in-python-part-3
# ---
# Copyright (c) 2011, Jay Conrod.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Jay Conrod nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL JAY CONROD BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#---------------------------------------------------------------------------
# Most recent two changes:
#   2015/11/03  cxw     FACompoundStmt.eval expressly returns None.
#   2015/11/14  cxw     FAReadVar: moved typechecking to parse time.
#                       FAApply: improved error message for unresolved call

# vi: set ts=4 sts=4 sw=4 expandtab ai: #

